% -----------------------------------------------------------------------------------
% last changes: HS, 20.10.2017
% FLIIMP Version 1.0
% -----------------------------------------------------------------------------------
function varargout = FLIIMP_form_calib(varargin)
% FLIIMP_FORM_CALIB MATLAB code for FLIIMP_form_calib.fig
%      FLIIMP_FORM_CALIB, by itself, creates a new FLIIMP_FORM_CALIB or raises the existing
%      singleton*.
%
%      H = FLIIMP_FORM_CALIB returns the handle to a new FLIIMP_FORM_CALIB or the handle to
%      the existing singleton*.
%
%      FLIIMP_FORM_CALIB('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FLIIMP_FORM_CALIB.M with the given input arguments.
%B
%      FLIIMP_FORM_CALIB('Property','Value',...) creates a new FLIIMP_FORM_CALIB or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before FLIIMP_form_calib_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to FLIIMP_form_calib_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help FLIIMP_form_calib

% Last Modified by GUIDE v2.5 07-Apr-2023 12:37:32

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @FLIIMP_form_calib_OpeningFcn, ...
                   'gui_OutputFcn',  @FLIIMP_form_calib_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})i
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before FLIIMP_form_calib is made visible.
function FLIIMP_form_calib_OpeningFcn(hObject, ~, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to FLIIMP_form_calib (see VARARGIN)

% load configuration
config=FLIIMP_config;

% apply position information
if isfield(varargin{1},'position')
  if length(varargin{1}.position)>4
    set(hObject,'position',varargin{1}.position{5});
  end
  handles.position=varargin{1}.position;
end

% Choose default command line output for FLIIMP_form_calib
handles.output = hObject;
handles.logfigure = varargin{1}.logfigure;
handles.logtext = varargin{1}.logtext;

set(hObject,'Name','FLIIMP - Step 5');

% add calibrations to popup menu
[~,cruns]=FLIIMP_standards(-1);
calnames={};
for cnr=1:length(cruns)
    ccor=FLIIMP_standards(cruns(cnr));
    if ~isempty(ccor)
      calnames{cnr}=sprintf('Cal-%03d: %s',cruns(cnr),ccor.description);
    else
      calnames{cnr}=sprintf('Cal-%03d: N/A',cruns(cnr));
    end
end
set(handles.calnrPopup,'string',calnames);
% select the maximum of the below-100 calibration (i.e. non-special)
set(handles.calnrPopup,'Value',find(cruns<100,1,'last'));

if length(varargin)>0
  % copy settings if applicable
  if isfield(varargin{1},'settings')
    handles.settings=varargin{1}.settings;
    handles.inputPath=varargin{1}.inputPath;
    handles.outputPath=varargin{1}.outputPath; 
    projectTypes={'internal';'collaborator';'external'};
    settings=handles.settings;
    if isfield(settings,'projectType')
      idx=find(strcmp(settings.projectType,projectTypes));
      set(handles.projectEdit,'String',settings.project);
      set(handles.runEdit,'String',settings.runID);
      set(handles.reportEdit,'String',datestr(settings.reportDate,'yyyymmdd'));
      set(handles.customerEdit,'String',settings.customer);
      set(handles.customeremailEdit,'String',settings.contact);
      set(handles.operatorEdit,'String',settings.operator);
      set(handles.operatoremailEdit,'String',settings.operatorContact);
      set(handles.reportPopup,'Value',settings.writeReport);
      set(handles.calibrationEdit,'String',sprintf('%s ',settings.standardNames{:}));
      set(handles.driftEdit,'String',sprintf('%s ',settings.standardsDrift{:}));
      % removed in FLIIMP V1.8
      %if isfield(settings,'calibrationType')
      %  set(handles.caltypePopup,'Value',settings.calibrationType);
      %end
      if isfield(settings,'calibrationRun')
        idx=find(cruns==settings.calibrationRun);
        if isempty(idx)
          idx=length(cruns);
        end
        set(handles.calnrPopup,'Value',idx);
      end
      set(handles.injectionsEdit,'String',num2str(settings.averageInjections));
      if isfield(settings,'averageFirstInjections')
        set(handles.firstInjectionsEdit,'String',num2str(settings.averageFirstInjections));
      end
      set(handles.speciesPopup,'Value',min(settings.includeE17,1)+1);
      if isfield(settings,'useHumidityCorrection')
        set(handles.humCheckbox,'Value',settings.useHumidityCorrection);
      end
      if isfield(settings,'useDriftCorrection')
        set(handles.driftCheckbox,'Value',settings.useDriftCorrection);
      end
      if isfield(settings,'useMemoryCorrection')
        set(handles.memCheckbox,'Value',settings.useMemoryCorrection);
      end
      if isfield(settings,'comment')
        set(handles.commentEdit,'String',settings.comment);
      end
    end
  end
end

% set lab name in form
set(handles.projectLabel,'String',sprintf('%s Project (*):',config.labname));

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes FLIIMP_form_calib wait for user response (see UIRESUME)
% uiwait(handles.FLIIMP_form_calib);


% --- Outputs from this function are returned to the command line.
function varargout = FLIIMP_form_calib_OutputFcn(hObject, ~, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in caltypePopup.
function caltypePopup_Callback(hObject, ~, handles)
% hObject    handle to caltypePopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns caltypePopup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from caltypePopup


% --- Executes during object creation, after setting all properties.
function caltypePopup_CreateFcn(hObject, ~, handles)
% hObject    handle to caltypePopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function runEdit_Callback(hObject, ~, handles)
% hObject    handle to runEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of runEdit as text
%        str2double(get(hObject,'String')) returns contents of runEdit as a double


% --- Executes during object creation, after setting all properties.
function runEdit_CreateFcn(hObject, ~, handles)
% hObject    handle to runEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function customerEdit_Callback(hObject, ~, handles)
% hObject    handle to customerEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of customerEdit as text
%        str2double(get(hObject,'String')) returns contents of customerEdit as a double


% --- Executes during object creation, after setting all properties.
function customerEdit_CreateFcn(hObject, ~, handles)
% hObject    handle to customerEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in backButton.
function backButton_Callback(hObject, ~, handles)
% hObject    handle to backButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
getSettings(hObject,handles);
handles=guidata(hObject);
handles.position{5}=get(handles.output,'Position');
%set(gcf,'Visible','Off');
close(gcf);
FLIIMP_form_rename(handles);


% --- Executes on button press in calibrateButton.
function calibrateButton_Callback(hObject, ~, handles)
% hObject    handle to calibrateButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
getSettings(hObject,handles);
handles=guidata(hObject);
%handles.settings.timeInterpolation=0;

% create intermediate structure with paths
lsettings=handles.settings;

% if standard names are missing, error message has already been thrown
if isempty(lsettings.standardNames) || isempty(lsettings.standardsDrift)
  return
end
% check if other required parameters are specified
if isempty(lsettings.project)
  logf(handles,'ERROR: need to specify project name');
  return
end
if isempty(lsettings.runID)
  logf(handles,'ERROR: need to specify run ID');
  return
end

lsettings.inputPath=handles.inputPath;
lsettings.outputPath=handles.outputPath;
lsettings.previewDrift=0;
lsettings.previewCal=0;
reportType=lsettings.writeReport;
if reportType==1 || reportType==3
    lsettings.writeReport=1;
  [cdata mdata]=FLIIMP_liquid_calibration(handles,lsettings);
  if isfield(cdata,'name1')
    %logf(handles,'%s',cdata.name1{:});
    logf(handles,'Calibration completed for %d samples. See report file for details',length(cdata.name1));
  else
    logf(handles,'No samples calibrated. Please check your settings.');
  end
  logf(handles,'---------------------------------------------------------------------');

  % move log file to output directory
  %fclose(lid);
  rep='_internal';
  rdir=sprintf('%s/%s-%s%s_%s',lsettings.outputPath,lsettings.project,lsettings.runID,rep,datestr(lsettings.reportDate,'yyyymmdd'));
  copyfile(lsettings.logfile,sprintf('%s/calibration_%s.log',rdir,datestr(lsettings.reportDate,'yyyymmdd')),'f');
  %lsettings.lid=fopen(lsettings.logfile,'a');
  %lid=lsettings.lid;
end
if reportType==2 || reportType==3
  lsettings.writeReport=2;
  [cdata mdata]=FLIIMP_liquid_calibration(handles,lsettings);
  if isfield(cdata,'name1')
    %logf(handles,'%s',cdata.name1{:});
    logf(handles,'Calibration completed for %d samples. See report file for details',length(cdata.name1));
  else
    logf(handles,'No samples calibrated. Please check your settings.');
  end
  logf(handles,'---------------------------------------------------------------------');

  % move log file to output directory
  %fclose(lid);
  rep='_reporting';
  rdir=sprintf('%s/%s-%s%s_%s',lsettings.outputPath,lsettings.project,lsettings.runID,rep,datestr(lsettings.reportDate,'yyyymmdd'));
  copyfile(lsettings.logfile,sprintf('%s/calibration_%s.log',rdir,datestr(lsettings.reportDate,'yyyymmdd')),'f');
  %lsettings.lid=fopen(lsettings.logfile,'a');
  %lid=lsettings.lid;
end
handles.settings=lsettings;
guidata(hObject,handles);

% save current settings to global variable settings
function getSettings(hObject,handles)

if isfield(handles,'settings')
  settings=handles.settings;
end

projectTypes={'internal';'collaborator';'external'};
settings.project=get(handles.projectEdit,'String');
%settings.projectType=projectTypes{get(handles.reportPopup,'Value')};
settings.projectType='internal'; % from 1.9.0 on, only project type
settings.runID=get(handles.runEdit,'String');
settings.reportDate=datenum(get(handles.reportEdit,'String'),'yyyymmdd');
settings.contact=get(handles.customeremailEdit,'String');
settings.customer=get(handles.customerEdit,'String');
settings.operator=get(handles.operatorEdit,'String');
settings.operatorContact=get(handles.operatoremailEdit,'String');
settings.writeReport=get(handles.reportPopup,'Value');
try
  settings.standardNames = textscan(get(handles.calibrationEdit,'String'),'%s');
  % fix a bug
  settings.standardNames=settings.standardNames{:};
catch
  logf(handles,'ERROR: need to specify calibration standards, separated by space characters');
  settings.standardNames={};
  handles.settings=settings;
  guidata(hObject,handles);
  return
end
try
  settings.standardsDrift = textscan(get(handles.driftEdit,'String'),'%s');
  % fix a bug
  settings.standardsDrift=settings.standardsDrift{:};
catch
  logf(handles,'ERROR: need to specify drift standards, separated by space characters');
  settings.standardsDrift={};
  handles.settings=settings;
  guidata(hObject,handles);
  return
end

% selection removed in FLIIMP V1.8
%settings.calibrationType=get(handles.caltypePopup,'Value');
settings.calibrationType=1;
[~,cruns]=FLIIMP_standards(-1);
calnr=get(handles.calnrPopup,'Value');
settings.calibrationRun=cruns(calnr);
settings.averageInjections=eval(get(handles.injectionsEdit,'String'));
settings.averageFirstInjections=eval(get(handles.firstInjectionsEdit,'String'));
settings.includeE17=min(2,get(handles.speciesPopup,'Value'))-1;
settings.useHumidityCorrection=get(handles.humCheckbox,'Value');
settings.useDriftCorrection=get(handles.driftCheckbox,'Value');
settings.useMemoryCorrection=get(handles.memCheckbox,'Value');
settings.comment=get(handles.commentEdit,'String');
handles.settings=settings;
guidata(hObject,handles);

% --- Executes on button press in saveButton.
function saveButton_Callback(hObject, ~, handles)
% hObject    handle to saveButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
getSettings(hObject,handles);
handles=guidata(hObject);
settings=handles.settings;
[newfile newpath]=uiputfile({'*.mat','MATLAB data file'},'Save run settings to...',[handles.outputPath,'/',settings.filename]);
if ~isequal(newpath,0) && ~isequal(newfile,0)
  save(fullfile(newpath,newfile),'settings');
  handles.settings.filename=newfile;
  guidata(hObject,handles);
end


% --- Executes on button press in loadButton.
function loadButton_Callback(hObject, eventdata, handles)
% hObject    handle to loadButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[newfile newpath]=uigetfile({'*.mat','MATLAB data file'},'Load run settings from...',handles.outputPath);
% save log file during loading
logfile=handles.settings.logfile;
if ~isequal(newpath,0) && ~isequal(newfile,0)
  load(fullfile(newpath,newfile));
  settings.lid=handles.settings.lid;
  settings.logfile=logfile;
  settings.filename=newfile;
else 
  return
end
%projectTypes={'internal';'collaborator';'external'};
%idx=find(strcmp(settings.projectType,projectTypes));
idx=1;
set(handles.projectEdit,'String',settings.project);
set(handles.runEdit,'String',settings.runID);
set(handles.reportEdit,'String',datestr(settings.reportDate,'yyyymmdd'));
set(handles.customerEdit,'String',settings.customer);
set(handles.customeremailEdit,'String',settings.contact);
set(handles.operatorEdit,'String',settings.operator);
set(handles.operatoremailEdit,'String',settings.operatorContact);
set(handles.reportPopup,'Value',settings.writeReport);
set(handles.calibrationEdit,'String',sprintf('%s ',settings.standardNames{:}));
set(handles.driftEdit,'String',sprintf('%s ',settings.standardsDrift{:}));
if ~isfield(settings,'comment')
  settings.comment='';
end
set(handles.commentEdit,'String',settings.comment);
% removed in FLIIMP V1.8
%if isfield(settings,'calibrationType'),
%  set(handles.caltypePopup,'Value',settings.calibrationType);
%end
if isfield(settings,'calibrationRun')
  set(handles.calnrPopup,'Value',settings.calibrationRun);  
end
set(handles.injectionsEdit,'String',num2str(settings.averageInjections));
if isfield(settings,'averageFirstInjections')
  set(handles.firstInjectionsEdit,'String',num2str(settings.averageFirstInjections));
end
set(handles.speciesPopup,'Value',min(settings.includeE17,1)+1);
handles.settings=settings;
guidata(hObject,handles);


function customeremailEdit_Callback(hObject, eventdata, handles)
% hObject    handle to customeremailEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of customeremailEdit as text
%        str2double(get(hObject,'String')) returns contents of customeremailEdit as a double


% --- Executes during object creation, after setting all properties.
function customeremailEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to customeremailEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function operatoremailEdit_Callback(hObject, eventdata, handles)
% hObject    handle to operatoremailEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of operatoremailEdit as text
%        str2double(get(hObject,'String')) returns contents of operatoremailEdit as a double


% --- Executes during object creation, after setting all properties.
function operatoremailEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to operatoremailEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in reportPopup.
function reportPopup_Callback(hObject, eventdata, handles)
% hObject    handle to reportPopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns reportPopup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from reportPopup


% --- Executes during object creation, after setting all properties.
function reportPopup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to reportPopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function operatorEdit_Callback(hObject, eventdata, handles)
% hObject    handle to operatorEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of operatorEdit as text
%        str2double(get(hObject,'String')) returns contents of operatorEdit as a double


% --- Executes during object creation, after setting all properties.
function operatorEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to operatorEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function reportEdit_Callback(hObject, eventdata, handles)
% hObject    handle to reportEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of reportEdit as text
%        str2double(get(hObject,'String')) returns contents of reportEdit as a double


% --- Executes during object creation, after setting all properties.
function reportEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to reportEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function projectEdit_Callback(hObject, eventdata, handles)
% hObject    handle to projectEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of projectEdit as text
%        str2double(get(hObject,'String')) returns contents of projectEdit as a double


% --- Executes during object creation, after setting all properties.
function projectEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to projectEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function standardsEdit_Callback(hObject, eventdata, handles)
% hObject    handle to standardsEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of standardsEdit as text
%        str2double(get(hObject,'String')) returns contents of standardsEdit as a double


% --- Executes during object creation, after setting all properties.
function standardsEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to standardsEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function calibrationEdit_Callback(hObject, eventdata, handles)
% hObject    handle to calibrationEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of calibrationEdit as text
%        str2double(get(hObject,'String')) returns contents of calibrationEdit as a double


% --- Executes during object creation, after setting all properties.
function calibrationEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to calibrationEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function driftEdit_Callback(hObject, eventdata, handles)
% hObject    handle to driftEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of driftEdit as text
%        str2double(get(hObject,'String')) returns contents of driftEdit as a double


% --- Executes during object creation, after setting all properties.
function driftEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to driftEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function firstInjectionsEdit_Callback(hObject, eventdata, handles)
% hObject    handle to firstInjectionsEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of firstInjectionsEdit as text
%        str2double(get(hObject,'String')) returns contents of firstInjectionsEdit as a double


% --- Executes during object creation, after setting all properties.
function firstInjectionsEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to firstInjectionsEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in calnrPopup.
function calnrPopup_Callback(hObject, eventdata, handles)
% hObject    handle to calnrPopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns calnrPopup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from calnrPopup


% --- Executes during object creation, after setting all properties.
function calnrPopup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to calnrPopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function injectionsEdit_Callback(hObject, eventdata, handles)
% hObject    handle to injectionsEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of injectionsEdit as text
%        str2double(get(hObject,'String')) returns contents of injectionsEdit as a double


% --- Executes during object creation, after setting all properties.
function injectionsEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to injectionsEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in driftCheckbox.
function driftCheckbox_Callback(hObject, eventdata, handles)
% hObject    handle to driftCheckbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of driftCheckbox


% --- Executes on button press in memCheckbox.
function memCheckbox_Callback(hObject, eventdata, handles)
% hObject    handle to memCheckbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of memCheckbox


% --- Executes on button press in humCheckbox.
function humCheckbox_Callback(hObject, eventdata, handles)
% hObject    handle to humCheckbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of humCheckbox


% --- Executes on selection change in speciesPopup.
function speciesPopup_Callback(hObject, eventdata, handles)
% hObject    handle to speciesPopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns speciesPopup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from speciesPopup


% --- Executes during object creation, after setting all properties.
function speciesPopup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to speciesPopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton7.
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
getSettings(hObject,handles);
handles=guidata(hObject);
if isempty(handles.settings.standardNames)
  return
end
if isempty(handles.settings.standardsDrift)
  return
end
lsettings=handles.settings;
lsettings.inputPath=handles.inputPath;
lsettings.outputPath=handles.outputPath;
lsettings.previewDrift=1;
lsettings.previewCal=0;
lsettings.writeReport=0;
% call with settings that do not create log output
dryhandles.nolog=1;
FLIIMP_liquid_calibration(dryhandles,lsettings);


% --- Executes on button press in pushbutton8.
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
getSettings(hObject,handles);
handles=guidata(hObject);
if isempty(handles.settings.standardNames)
  return
end
if isempty(handles.settings.standardsDrift)
  return
end
lsettings=handles.settings;
lsettings.inputPath=handles.inputPath;
lsettings.outputPath=handles.outputPath;
lsettings.previewCal=1;
lsettings.previewDrift=0;
lsettings.writeReport=0;
% call with settings that do not create log output
dryhandles.nolog=1;
FLIIMP_liquid_calibration(handles,lsettings);


% --------------------------------------------------------------------
function FileMenu_Callback(hObject, eventdata, handles)
% hObject    handle to FileMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function SaveMenu_Callback(hObject, eventdata, handles)
% hObject    handle to SaveMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

saveButton_Callback(hObject, eventdata, handles);

% --------------------------------------------------------------------
function LoadMenu_Callback(hObject, eventdata, handles)
% hObject    handle to LoadMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

loadButton_Callback(hObject, eventdata, handles);

% --------------------------------------------------------------------
function ReportMenu_Callback(hObject, eventdata, handles)
% hObject    handle to ReportMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

calibrateButton_Callback(hObject, eventdata, handles)

% --------------------------------------------------------------------
function QuitMenu_Callback(hObject, eventdata, handles)
% hObject    handle to QuitMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global config;

% confirm quit
%selection = questdlg('OK to quit FLIIMP?','Quit FLIIMP','Quit','Cancel','Cancel');
%if strcmp(selection,'Quit')==0
%    return;
%end

% store current position
handles.position{5}=get(handles.output,'Position');

% save the file path options
clear paths
paths.inputPath=handles.inputPath;
paths.outputPath=handles.outputPath;
paths.positions=handles.position;
logf(handles,['Saving path settings to ',config.homeDirectory,'/FLIIMP_settings.mat']);
save(sprintf('%s/FLIIMP_settings.mat',config.homeDirectory),'paths');
logf(handles,'FLIIMP session ended on %s',datestr(now));

%f=get(0,'Children');
%for i=1:length(f)
%  if ~strcmp(f(i).Name,'FLIIMP - Step 5')
%    delete(f(i).Number);
%  end
%end
%delete(handles.logfigure);
delete(gcf);


% --------------------------------------------------------------------
function BackMenu_Callback(hObject, eventdata, handles)
% hObject    handle to BackMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

backButton_Callback(hObject, eventdata, handles)


% --------------------------------------------------------------------
function Tools_Callback(hObject, eventdata, handles)
% hObject    handle to Tools (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function memdrift_Callback(hObject, eventdata, handles)
% hObject    handle to memdrift (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
FLIIMP_form_memdrift(handles);

% --------------------------------------------------------------------
function LTP_Callback(hObject, eventdata, handles)
% hObject    handle to LTP (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
FLIIMP_form_LTP(handles);



function edit14_Callback(hObject, eventdata, handles)
% hObject    handle to commentEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of commentEdit as text
%        str2double(get(hObject,'String')) returns contents of commentEdit as a double


% --- Executes during object creation, after setting all properties.
function edit14_CreateFcn(hObject, eventdata, handles)
% hObject    handle to commentEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu5.
function popupmenu5_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu5 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu5


% --- Executes during object creation, after setting all properties.
function popupmenu5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit15_Callback(hObject, eventdata, handles)
% hObject    handle to edit15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit15 as text
%        str2double(get(hObject,'String')) returns contents of edit15 as a double


% --- Executes during object creation, after setting all properties.
function edit15_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit16_Callback(hObject, eventdata, handles)
% hObject    handle to edit16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit16 as text
%        str2double(get(hObject,'String')) returns contents of edit16 as a double


% --- Executes during object creation, after setting all properties.
function edit16_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit17_Callback(hObject, eventdata, handles)
% hObject    handle to edit17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit17 as text
%        str2double(get(hObject,'String')) returns contents of edit17 as a double


% --- Executes during object creation, after setting all properties.
function edit17_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit18_Callback(hObject, eventdata, handles)
% hObject    handle to edit18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit18 as text
%        str2double(get(hObject,'String')) returns contents of edit18 as a double


% --- Executes during object creation, after setting all properties.
function edit18_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit19_Callback(hObject, eventdata, handles)
% hObject    handle to edit19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit19 as text
%        str2double(get(hObject,'String')) returns contents of edit19 as a double


% --- Executes during object creation, after setting all properties.
function edit19_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit20_Callback(hObject, eventdata, handles)
% hObject    handle to edit20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit20 as text
%        str2double(get(hObject,'String')) returns contents of edit20 as a double


% --- Executes during object creation, after setting all properties.
function edit20_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu6.
function popupmenu6_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu6 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu6


% --- Executes during object creation, after setting all properties.
function popupmenu6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit21_Callback(hObject, eventdata, handles)
% hObject    handle to edit21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit21 as text
%        str2double(get(hObject,'String')) returns contents of edit21 as a double


% --- Executes during object creation, after setting all properties.
function edit21_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit22_Callback(hObject, eventdata, handles)
% hObject    handle to edit22 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit22 as text
%        str2double(get(hObject,'String')) returns contents of edit22 as a double


% --- Executes during object creation, after setting all properties.
function edit22_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit22 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit23_Callback(hObject, eventdata, handles)
% hObject    handle to edit23 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit23 as text
%        str2double(get(hObject,'String')) returns contents of edit23 as a double


% --- Executes during object creation, after setting all properties.
function edit23_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit23 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton9.
function pushbutton9_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton10.
function pushbutton10_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit24_Callback(hObject, eventdata, handles)
% hObject    handle to edit24 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit24 as text
%        str2double(get(hObject,'String')) returns contents of edit24 as a double


% --- Executes during object creation, after setting all properties.
function edit24_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit24 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in checkbox6.
function checkbox6_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox6


% --- Executes on button press in checkbox7.
function checkbox7_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox7


% --- Executes on button press in checkbox8.
function checkbox8_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox8


% --- Executes on selection change in popupmenu7.
function popupmenu7_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu7 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu7


% --- Executes during object creation, after setting all properties.
function popupmenu7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton11.
function pushbutton11_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton12.
function pushbutton12_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function commentEdit_Callback(hObject, eventdata, handles)
% hObject    handle to commentEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of commentEdit as text
%        str2double(get(hObject,'String')) returns contents of commentEdit as a double


% --- Executes during object creation, after setting all properties.
function commentEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to commentEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --------------------------------------------------------------------
function Log_Callback(hObject, eventdata, handles)
% hObject    handle to Log (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.logfigure,'Visible','on');


% --- Executes on button press in todayButton.
function todayButton_Callback(hObject, eventdata, handles)
% hObject    handle to todayButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.reportEdit,'String',datestr(now,'yyyymmdd'));


% --- Executes when user attempts to close FLIIMP_form_C.
function FLIIMP_form_C_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to FLIIMP_form_C (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
QuitMenu_Callback(hObject, eventdata, handles);

% fin
