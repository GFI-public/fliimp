% FARLAB_process_liquid.m
%--------------------------------
% Description
% prepare settings for liquid sample
% processing using interactive forms
%--------------------------------
% HS, Sat Aug 19 16:18:46 CEST 2017 
%--------------------------------

% 1. Enter date range or fields to process
%-----------------------------------------

% instrument and file settings
settings.instrument='HKDS2038';
%settings.startDate='20170807';
%settings.endDate='20170807';
settings.startDate='20170728';
settings.endDate='20170729';
settings.inputFiles={};

% 2. Inspect for samples to ignore
%-----------------------------------------

settings.ignore=[];

% 3. Define settings to process run
%-----------------------------------------

settings.project='FARLAB';
settings.runID='O17_test_01';
settings.projectType='internal';
settings.contact='harald.sodemann@uib.no';
settings.customer='Harald Sodemann';
settings.operator='Harald Sodemann';
settings.operatorContact='harald.sodemann@uib.no';
settings.writeReport=1;
settings.standardNames = {'VSMOW2','SLAP2'};
settings.standardsUsed = {'VSMOW2','SLAP2'};
settings.calibrationPoints = 2;
settings.averageInjections = 3; % choose number of injections (ends+1) to calculate mean 
settings.ignoreVials = [1];  % choose vials to ignore
%settings.ignoreVials = [];  % choose vials to ignore
settings.timeInterpolation = 0;   % do not time interpolation
settings.reportDate = now; % reporting date

% 4. Process run
%-----------------------------------------

[cdata3 mdata3] = FARLAB_liquid_calibration(settings);

% fin
