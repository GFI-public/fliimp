% batch_FLIIMP_example.m
% ---------------------------
% batch process files for a FARLAB project (here 2019-25-HS)

% location of FLIIMP source (for FARLAB image file source);
global installdir;
installdir='/Users/hso039/Dropbox/FARLAB/matlab/FLIIMP/src/';
addpath(installdir);

% change directory
cd('/Volumes/farlab/Projects/2019/2019-25-HS');

% re-run 3 saved calibration runs
FLIIMP_batch('2019-25-HS_run02_settings.mat');
FLIIMP_batch('2019-25-HS_run04-05_settings.mat');
FLIIMP_batch('2019-25-HS_run10_settings.mat');

% fin