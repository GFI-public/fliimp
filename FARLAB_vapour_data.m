%
% FARLAB_vapour_data returns CRDS data period from the archived files.
%
%   data=FARLAB_vapour_data(device,startdate,enddate)
%   device can be one of the following:
%     HIDS2254 (L2130i), HKDS2039 (L2140i Harald), HKDS2038 (L2140i Ule)
%   startdate and enddate are in matlab date number format
%--------------------------------
% HS, Do 13 Sep 2012 11:21:04 CEST
%--------------------------------
% Issue: variable naming are slightly different for different measuring modes
% Solved the issue by replacing the variables with the same name string
% Script modification in the section of this for loop: 'for id=1:length(ids) ...'
% YW, 03 Apr 2019 22:47:14 CEST 
%--------------------------------
% TODO: calculate quality flag
%--------------------------------

function outdata=FARLAB_vapour_data(device,sdatum,edatum,varargin)

debug=0;

% fill NaNs with large data gaps
fillgaps=0;

% check for valid device id
devices={'HIDS2254';'HKDS2039';'HKDS2038';'HIDS2380'};
if ~ismember(device,devices)
    disp('ERROR: device must be one of the following:');
    disp('HIDS2254 (L2130i), HKDS2039 (L2140i Precipitation), HKDS2038 (L2140i Seawater), HIDS2380 (L2130i ERC)');
    data=[];
    return
end

% path to data repository
if nargin==4,
  datapath=sprintf(varargin{1},device);
  SDMfiles=0;
elseif nargin==5,
  SDMfiles=varargin{2};
  datapath=sprintf(varargin{1},device);
else
    %datapath=sprintf('/Data/gfi/met/shared/Picarro/%s/DataLog_User',device);
    %datapath=sprintf('/Volumes/HS_TRANSFER/%s',device);
    %datapath=sprintf('/Volumes/metdata/FARLAB/%s/DataLog_User',device);
    %datapath=sprintf('/Data/gfi/scratch/metdata/FARLAB/%s/DataLog_User',device);
    %datapath=sprintf('/Data/gfi/scratch/metdata/FARLAB/%s/raw',device);
    %datapath=sprintf('/Volumes/metdata/FARLAB/%s/raw',device);
    datapath=sprintf('/Data/gfi/projects/farlab/Instruments/%s/raw_nc',device);
    SDMfiles=0;
end

data=[];
outdata=[];

% different variable naming on different Picarro modes
varstr1={'AlarmStatus','InstStatus','MPVPos','d18O','dD','N2Flag','SlopeShift','BaselineShift','Residuals'};
varstr2={'ALARM_STATUS','INST_STATUS','MPVPosition','Delta_18_16','Delta_D_H','n2_flag','slope_shift','baseline_shift','residuals'};
idata=[];
    
% debugging test
% device = 'HIDS2254';
% datapath = '/Data/gfi/scratch/metdata/FARLAB/HIDS2254/raw';
% % sdatum = datenum('2019-03-04');
% % edatum = datenum('2019-03-05');
% sdatum = datenum('2017-03-04');
% edatum = datenum('2017-03-04');


% loop through all dates
for day=sdatum:1:edatum,
    svec=datevec(day);
    % handle periods transgressing midnight
    filepath=sprintf('%s/%04d/%02d/',datapath,svec(1),svec(2));
    if SDMfiles==0,
      fprintf('%s%s-%s-DataLog_User.nc\n',filepath,device,datestr(day,'yyyymmdd'))
      files=dir(sprintf('%s%s-%s-DataLog_User.nc',filepath,device,datestr(day,'yyyymmdd')));
    else
      fprintf('%s%s-%s-WaterVapor.nc\n',filepath,device,datestr(day,'yyyymmdd'))
      files=dir(sprintf('%s%s-%s-WaterVapor.nc',filepath,device,datestr(day,'yyyymmdd')));
    end
    for f=1:length(files),
        filename=[filepath,files(f).name];
        ncid=netcdf.open(filename,'noclobber');
        ids=netcdf.inqVarIDs(ncid);
        for id=1:length(ids),
            name=netcdf.inqVar(ncid,ids(id));
            idata.(name)=double(netcdf.getVar(ncid,ids(id)));
            if isfield(data,name)
                data.(name)=[data.(name); idata.(name)];
            elseif ~isfield(data,name) && ~isempty(intersect(varstr1,name)) 
                % if variabe name is one of varstr1, change it to the corresponding variable name in varstr2
                idx = contains(varstr1,name); 
                name2 = char(varstr2(idx));
                idata.(name2)=idata.(name);
                if isfield(data,name2)
                    data.(name2)=[data.(name2); idata.(name2)];
                else
                    data.(name2)=idata.(name2);
                end
            else % data=[], i.e., this is the first loading file
                data.(name)=idata.(name);
            end
            idata=[];
        end
        netcdf.close(ncid);
    end
end

% sort the data with time
if ~isempty(data) && isfield(data,'time') % skip empty nc files that has no [time] variable
    [data.time sidx]=sort(data.time);
    fields=fieldnames(data);
    for f=1:length(fields),
        if strcmp(fields{f},'time')==0,
            sfield=data.(fields{f})(sidx);
            data.(fields{f})=sfield;
        end
    end
    % increase by 1 year to have matlab time
    %data.time=data.time+365;
    % decrease by 1970 years to have matlab time
    data.time=data.time+datenum(1970,1,1);
else
    return
end

% fill in gaps with missing data
if ~isfield(data,'time')
    data=[];
    return
end

% change naming
data.delta_D=data.Delta_D_H;
data.delta_18O=data.Delta_18_16;
data.q=data.H2O;
data=rmfield(data,'Delta_D_H');
data=rmfield(data,'Delta_18_16');
if isfield(data,'Delta_17_16') % original is isfield('Delta_17_16','data'); think it's a typo
    data.delta_17O=data.Delta_17_16;
    data=rmfield(data,'Delta_17_16');
end

% add NaNs at large data gaps if requested
if fillgaps==1,
  gaps=find(diff(data.time)>0.3e-4) % approx 3 seconds
else
  gaps=[];
end

fnames=fieldnames(data);
if isempty(gaps)
    outdata=data;
else    
    gaps=[0; gaps; length(data.time)];
    for g=1:length(gaps)-1,
	% add at next time interval
	if isfield(outdata,name),
	    outdata.time=[outdata.time; data.time((gaps(g)+1):(gaps(g+1))); data.time(gaps(g+1))+0.3e-4];
	else
	    outdata.time=[data.time((gaps(g)+1):(gaps(g+1))); data.time(gaps(g+1))+0.3e-4];
	end
	% set other fields to missing
	for f=1:length(fnames),
	    name=fnames{f};
	    if isfield(outdata,name) && strcmp(name,'time')==0,
		outdata.(name)=[outdata.(name); data.(name)((gaps(g)+1):(gaps(g+1))); NaN];
	    else
		outdata.(name)=[data.(name)((gaps(g)+1):(gaps(g+1))); NaN];
	    end
	end
    end
end

% calculate deuterium excess
outdata.d=outdata.delta_D-8*outdata.delta_18O;

% add missing instrument parameter files
if SDMfiles==1,
  outdata.CavityTemp=ones(size(outdata.d)).*80.0;
  outdata.CavityPressure=ones(size(outdata.d)).*50.0;
  outdata.WarmBoxTemp=ones(size(outdata.d)).*45.0;
  outdata.AmbientPressure=ones(size(outdata.d)).*760.0;
end

% add NaN data in long time gaps (>30s)

tdiff=diff(outdata.time)*24*3600;
tidx=sort(find(tdiff>60.0),'descend');
fnames=fieldnames(outdata);
for i=1:length(tidx),
  %datestr(outdata.time(tidx(i)));
  %datestr(outdata.time(tidx(i)+1));
  for f=1:length(fnames),
    if strcmp(fnames{f},'time')==0,
      outdata.(fnames{f})=[outdata.(fnames{f})(1:tidx(i));NaN;outdata.(fnames{f})(tidx(i)+1:end)];
    else
      outdata.(fnames{f})=[outdata.(fnames{f})(1:tidx(i));mean(outdata.time(tidx(i):tidx(i)+1));outdata.(fnames{f})(tidx(i)+1:end)];
    end
  end
end

% restrict to the requested time period only
stidx=min(find(outdata.time>=sdatum));
if edatum==sdatum,
  edatum=edatum+1;
end
etidx=max(find(outdata.time<=edatum));
fnames=fieldnames(outdata);
for f=1:length(fnames),
  outdata.(fnames{f})=outdata.(fnames{f})(stidx:etidx);
end

end
% fin
