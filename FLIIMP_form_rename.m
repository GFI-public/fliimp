% -----------------------------------------------------------------------------------
% last changes: HS, 20.10.2017
% FLIIMP Version 1.0
% -----------------------------------------------------------------------------------
function varargout = FLIIMP_form_rename(varargin)
% FORM_B MATLAB code for FLIIMP_form_rename.fig
%      FORM_B, by itself, creates a new FORM_B or raises the existing
%      singleton*.
%
%      H = FORM_B returns the handle to a new FORM_B or the handle to
%      the existing singleton*.
%
%      FORM_B('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FORM_B.M with the given input arguments.
%
%      FORM_B('Property','Value',...) creates a new FORM_B or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before FLIIMP_form_rename_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to FLIIMP_form_rename_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help FLIIMP_form_rename

% Last Modified by GUIDE v2.5 07-Apr-2023 12:14:48

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @FLIIMP_form_rename_OpeningFcn, ...
                   'gui_OutputFcn',  @FLIIMP_form_rename_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before FLIIMP_form_rename is made visible.
function FLIIMP_form_rename_OpeningFcn(hObject, ~, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to FLIIMP_form_rename (see VARARGIN)

% apply position information
if isfield(varargin{1},'position')
  if length(varargin{1}.position)>3
    set(hObject,'position',varargin{1}.position{4});    
  end
  handles.position=varargin{1}.position;
end

% Choose default command line output for FLIIMP_form_rename
handles.output = hObject;
handles.logfigure = varargin{1}.logfigure;
handles.logtext = varargin{1}.logtext;

set(hObject,'Name','FLIIMP - Step 4');

if ~isempty(varargin)
  % copy settings if applicable
  if isfield(varargin{1},'settings')
    handles.settings=varargin{1}.settings;
    handles.inputPath=varargin{1}.inputPath;
    handles.outputPath=varargin{1}.outputPath;
  end
end

% Update handles structure
guidata(hObject, handles);

% load the data file
settings=handles.settings;

% create intermediate structure with paths
lsettings=settings;
lsettings.inputPath=handles.inputPath;
lsettings.outputPath=handles.outputPath;

% read in the files which is pointed at
data=FLIIMP_read_liquid_injections(handles,lsettings);

if isempty(data)
  logf(settings.lid,'Please check your settings and restart.\n');
  return
end

% split vials
vials = sum(diff(data.InjNr)<1)+1;
vialstart = [1;find(diff(data.InjNr)<1)+1;length(data.InjNr)+1];
f = fieldnames(data);
for i = 1:vials
  for k = 1:length(f)
    vial(i).(f{k}) = data.(f{k})(vialstart(i):vialstart(i+1)-1);
  end
  vial(i).mH2O=mean(vial(i).H2O,'omitnan');
  vial(i).sH2O=std(vial(i).H2O,'omitnan');
end

% include new settings
if ~isfield(settings,'rename') || length(settings.rename.Identifier1)<vials
  for i=1:vials
    settings.rename.Identifier1{i}=vial(i).Identifier1{1};
    settings.rename.Identifier2{i}=vial(i).Identifier2{1};
    handles.backup.Identifier1{i}=vial(i).Identifier1{1};
    handles.backup.Identifier2{i}=vial(i).Identifier2{1};        
  end
else
% keep old names, and assign saved new names to vials
  for i=1:vials
    for j=1:length(vial(i).Identifier1)
        handles.backup.Identifier1{i}=vial(i).Identifier1{j};
        handles.backup.Identifier2{i}=vial(i).Identifier2{j};
        vial(i).Identifier1{j}=settings.rename.Identifier1{i};
        vial(i).Identifier2{j}=settings.rename.Identifier2{i};
    end
  end
end

% exclude individual injections
if ~isfield(settings,'excludeInj')
  settings.excludeInj=cell(1,vials);
  for i = 1:vials
    settings.excludeInj{i}=[-1];
  end
elseif length(settings.excludeInj)<vials
  for i = 1:vials
    settings.excludeInj{i}=[-1];
  end
end

% build a selection list
if ~isfield(settings,'ignoreInjections')
  settings.ignoreInjections=zeros(vials,1);
end
handles.vial=vial;
handles.tableEntry=cell(vials,1);
% adjust if length of vials has changed
if length(settings.ignoreInjections)>vials
  settings.ignoreInjections=settings.ignoreInjections(1:vials);
elseif length(settings.ignoreInjections)<vials
  settings.ignoreInjections=zeros(vials,1);
end
for i=1:vials
  if settings.ignoreInjections(i)==1
    check='X';
  else 
    check=' ';
  end
  if strcmp(vial(i).Identifier1{1},handles.backup.Identifier1{i})==0 ||  strcmp(vial(i).Identifier2{1},handles.backup.Identifier2{i})==0 
    %settings.rename.Identifier2{i}=vial(i).Identifier2{1};
    col='#CC5500';
  else
    col='#000000';
  end
  handles.tableEntry{i}=sprintf('<HTML><BODY color="%s"><PRE>%c %3d %15s %25s</PRE></BODY></HTML>',col,check,i,vial(i).Identifier1{1},vial(i).Identifier2{1});
end
set(handles.sampleList,'String',handles.tableEntry);
handles.settings=settings;
set(handles.sampleList,'Value',1);
guidata(hObject,handles);
sampleList_Callback(handles.sampleList,0,handles);

% UIWAIT makes FLIIMP_form_rename wait for user response (see UIRESUME)
% uiwait(handles.figureE);

function updateInjectionList(hObject,handles,i)
if handles.settings.ignoreInjections(i)==1
  check='X';
else 
  check=' ';
end
if strcmp(handles.vial(i).Identifier1{1},handles.backup.Identifier1{i})==0 ||  strcmp(handles.vial(i).Identifier2{1},handles.backup.Identifier2{i})==0 
    %settings.rename.Identifier2{i}=vial(i).Identifier2{1};
    col='#CC5500';
else
    col='#000000';
end
handles.tableEntry{i}=sprintf('<HTML><BODY color="%s"><PRE>%c %3d %10s %20s</PRE></BODY></HTML>',col,check,i,handles.vial(i).Identifier1{1},handles.vial(i).Identifier2{1});
set(handles.sampleList,'String',handles.tableEntry);
guidata(hObject,handles);


% --- Outputs from this function are returned to the command line.
function varargout = FLIIMP_form_rename_OutputFcn(hObject, ~, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in sampleList.
function sampleList_Callback(hObject, ~, handles)
% hObject    handle to sampleList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns sampleList contents as cell array
%        contents{get(hObject,'Value')} returns selected item from sampleList
idx=get(hObject,'Value');
set(handles.nameEdit,'String',handles.vial(idx).Identifier1{1});
set(handles.farlabEdit,'String',handles.vial(idx).Identifier2{1});

if isfield(handles.settings,'excludeInj')
  excl=handles.settings.excludeInj{idx};
else
  handles.settings.excludeInj={};
end

% --- Executes during object creation, after setting all properties.
function sampleList_CreateFcn(hObject, ~, handles)
% hObject    handle to sampleList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in backButton.
function backButton_Callback(hObject, ~, handles)
% hObject    handle to backButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.position{4}=get(handles.output,'Position');
for i=1:length(handles.vial)
  handles.settings.rename.Identifier1{i}=handles.vial(i).Identifier1{1};
  handles.settings.rename.Identifier2{i}=handles.vial(i).Identifier2{1};
end
%set(gcf,'Visible','Off');
close(gcf);
FLIIMP_form_preproc(handles);


% --- Executes on button press in nextButton.
function nextButton_Callback(hObject, ~, handles)
% hObject    handle to nextButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.position{4}=get(handles.output,'Position');
for i=1:length(handles.vial)
  handles.settings.rename.Identifier1{i}=handles.vial(i).Identifier1{1};
  handles.settings.rename.Identifier2{i}=handles.vial(i).Identifier2{1};
end
%set(gcf,'Visible','Off');
close(gcf);
FLIIMP_form_calib(handles);


% --- Executes on button press in saveButton.
function saveButton_Callback(hObject, ~, handles)
% hObject    handle to saveButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
for i=1:length(handles.vial)
  handles.settings.rename.Identifier1{i}=handles.vial(i).Identifier1{1};
  handles.settings.rename.Identifier2{i}=handles.vial(i).Identifier2{1};
end
handles=guidata(hObject);
settings=handles.settings;
[newfile newpath]=uiputfile({'*.mat','MATLAB data file'},'Save run settings to...',[handles.outputPath,'/',settings.filename]);
if ~isequal(newpath,0) && ~isequal(newfile,0)
  save(fullfile(newpath,newfile),'settings');
  handles.settings.filename=newfile;
  guidata(hObject,handles);
end


% --- Executes on button press in loadButton.
function loadButton_Callback(hObject, eventdata, handles)
% hObject    handle to loadButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[newfile newpath]=uigetfile({'*.mat','MATLAB data file'},'Load run settings from...',handles.outputPath);
% save log file during loading
lid=handles.settings.lid;
logfile=handles.settings.logfile;
if ~isequal(newpath,0) && ~isequal(newfile,0)
  load(fullfile(newpath,newfile));
  settings.lid=lid;
  settings.logfile=logfile;
  settings.filename=newfile;
else 
  return
end
handles.settings=settings;
guidata(hObject,handles);
sampleList_Callback(handles.sampleList,0,handles);


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over assignButton.
function assignButton_Callback(hObject, eventdata, handles)
% hObject    handle to assignButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% read settings
farlabID=get(handles.projectEdit,'String');
startNum=str2double(get(handles.startEdit,'String'));
standards=strsplit(get(handles.standardsEdit,'String'),' ');

check=' ';
if ~isnan(startNum)
  % create and update names
  for i=1:length(handles.vial)
    if sum(strcmp(handles.vial(i).Identifier1{1},standards))==0
      newID=sprintf('%s-%03d',farlabID,startNum);
      handles.vial(i).Identifier2{1}=newID;
      if strcmp(handles.vial(i).Identifier1{1},handles.backup.Identifier1{i})==0 ||  strcmp(handles.vial(i).Identifier2{1},handles.backup.Identifier2{i})==0 
  %settings.rename.Identifier2{i}=vial(i).Identifier2{1};
        col='#CC5500';
      else
        col='#000000';
      end
      handles.tableEntry{i}=sprintf('<HTML><BODY color="%s"><PRE>%c %3d %10s %20s</PRE></BODY></HTML>',col,check,i,handles.vial(i).Identifier1{1},handles.vial(i).Identifier2{1});
      startNum=startNum+1;
    end
  end
  set(handles.sampleList,'String',handles.tableEntry);
  guidata(hObject, handles);
else
  logf(handles.settings.lid,'Error: bad number format for startNum.');
end


function projectEdit_Callback(hObject, eventdata, handles)
% hObject    handle to projectEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of projectEdit as text
%        str2double(get(hObject,'String')) returns contents of projectEdit as a double


% --- Executes during object creation, after setting all properties.
function projectEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to projectEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function startEdit_Callback(hObject, eventdata, handles)
% hObject    handle to startEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of startEdit as text
%        str2double(get(hObject,'String')) returns contents of startEdit as a double


% --- Executes during object creation, after setting all properties.
function startEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to startEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function standardsEdit_Callback(hObject, eventdata, handles)
% hObject    handle to standardsEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of standardsEdit as text
%        str2double(get(hObject,'String')) returns contents of standardsEdit as a double


% --- Executes during object creation, after setting all properties.
function standardsEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to standardsEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in updateButton.
function updateButton_Callback(hObject, eventdata, handles)
% hObject    handle to updateButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
nameEdit_Callback(handles.nameEdit, eventdata, handles);
farlabEdit_Callback(handles.farlabEdit, eventdata, handles);



function nameEdit_Callback(hObject, eventdata, handles)
% hObject    handle to nameEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of nameEdit as text
%        str2double(get(hObject,'String')) returns contents of nameEdit as a double

idx=get(handles.sampleList,'Value');
handles.vial(idx).Identifier1{1}=get(hObject,'String');
guidata(hObject, handles);
updateInjectionList(hObject,handles,idx);


% --- Executes during object creation, after setting all properties.
function nameEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to nameEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function farlabEdit_Callback(hObject, eventdata, handles)
% hObject    handle to farlabEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of farlabEdit as text
%        str2double(get(hObject,'String')) returns contents of farlabEdit as a double

idx=get(handles.sampleList,'Value');
handles.vial(idx).Identifier2{1}=get(hObject,'String');
guidata(hObject, handles);
updateInjectionList(hObject,handles,idx);


% --- Executes during object creation, after setting all properties.
function farlabEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to farlabEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in restoreIDsButton.
function restoreIDsButton_Callback(hObject, eventdata, handles)
% hObject    handle to restoreIDsButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
check=' ';
for i=1:length(handles.vial)
  handles.vial(i).Identifier2{1}=handles.backup.Identifier2{i};
  if strcmp(handles.vial(i).Identifier1{1},handles.backup.Identifier1{i})==0 ||  strcmp(handles.vial(i).Identifier2{1},handles.backup.Identifier2{i})==0 
  %settings.rename.Identifier2{i}=vial(i).Identifier2{1};
    col='#CC5500';
  else
    col='#000000';
  end
  handles.tableEntry{i}=sprintf('<HTML><BODY color="%s"><PRE>%c %3d %10s %20s</PRE></BODY></HTML>',col,check,i,handles.vial(i).Identifier1{1},handles.vial(i).Identifier2{1});
end
set(handles.sampleList,'String',handles.tableEntry);
guidata(hObject, handles);


% --- Executes on button press in restoreNamesButton.
function restoreNamesButton_Callback(hObject, eventdata, handles)
% hObject    handle to restoreNamesButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
check=' ';
for i=1:length(handles.vial)
  handles.vial(i).Identifier1{1}=handles.backup.Identifier1{i};
  if strcmp(handles.vial(i).Identifier1{1},handles.backup.Identifier1{i})==0 ||  strcmp(handles.vial(i).Identifier2{1},handles.backup.Identifier2{i})==0 
  %settings.rename.Identifier2{i}=vial(i).Identifier2{1};
    col='#CC5500';
  else
    col='#000000';
  end
  handles.tableEntry{i}=sprintf('<HTML><BODY color="%s"><PRE>%c %3d %10s %20s</PRE></BODY></HTML>',col,check,i,handles.vial(i).Identifier1{1},handles.vial(i).Identifier2{1});
end
set(handles.sampleList,'String',handles.tableEntry);
guidata(hObject, handles);


% --- Executes on button press in swapButton.
function swapButton_Callback(hObject, eventdata, handles)
% hObject    handle to swapButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
idx=get(handles.sampleList,'Value');
a=handles.vial(idx).Identifier1{1};
b=handles.vial(idx).Identifier2{1};
handles.vial(idx).Identifier1{1}=b;
handles.vial(idx).Identifier2{1}=a;
set(handles.nameEdit,'String',handles.vial(idx).Identifier1{1});
set(handles.farlabEdit,'String',handles.vial(idx).Identifier2{1});
guidata(hObject, handles);
updateInjectionList(hObject,handles,idx);

% --- Executes on button press in swapAllButton.
function swapAllButton_Callback(hObject, eventdata, handles)
% hObject    handle to swapAllButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
check=' ';
for idx=1:length(handles.vial)
  a=handles.vial(idx).Identifier1{1};
  b=handles.vial(idx).Identifier2{1};
  handles.vial(idx).Identifier1{1}=b;
  handles.vial(idx).Identifier2{1}=a;
  if strcmp(handles.vial(idx).Identifier1{1},handles.backup.Identifier1{idx})==0 ||  strcmp(handles.vial(idx).Identifier2{1},handles.backup.Identifier2{idx})==0 
  %settings.rename.Identifier2{i}=vial(i).Identifier2{1};
    col='#CC5500';
  else
    col='#000000';
  end
  handles.tableEntry{idx}=sprintf('<HTML><BODY color="%s"><PRE>%c %3d %10s %20s</PRE></BODY></HTML>',col,check,idx,handles.vial(idx).Identifier1{1},handles.vial(idx).Identifier2{1});
end
set(handles.sampleList,'String',handles.tableEntry);
guidata(hObject, handles);


% --------------------------------------------------------------------
function FileMenu_Callback(hObject, eventdata, handles)
% hObject    handle to FileMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function LoadMenu_Callback(hObject, eventdata, handles)
% hObject    handle to LoadMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

loadButton_Callback(hObject, eventdata, handles);

% --------------------------------------------------------------------
function SaveMenu_Callback(hObject, eventdata, handles)
% hObject    handle to SaveMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

saveButton_Callback(hObject, eventdata, handles);

% --------------------------------------------------------------------
function BackMenu_Callback(hObject, eventdata, handles)
% hObject    handle to BackMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

backButton_Callback(hObject, eventdata, handles);

% --------------------------------------------------------------------
function NextMenu_Callback(hObject, eventdata, handles)
% hObject    handle to NextMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

nextButton_Callback(hObject, eventdata, handles);

% --------------------------------------------------------------------
function QuitMenu_Callback(hObject, eventdata, handles)
% hObject    handle to QuitMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% confirm quit

global config;

%selection = questdlg('OK to quit FLIIMP?','Quit FLIIMP','Quit','Cancel','Cancel');
%if strcmp(selection,'Quit')==0
%    return;
%end

% store current position
handles.position{4}=get(handles.output,'Position');

% save the file path options
clear paths
paths.inputPath=handles.inputPath;
paths.outputPath=handles.outputPath;
paths.positions=handles.position;
logf(handles,['Saving path settings to ',config.homeDirectory,'/FLIIMP_settings.mat']);
save(sprintf('%s/FLIIMP_settings.mat',config.homeDirectory),'paths');
logf(handles,'FLIIMP session ended on %s',datestr(now));

%f=get(0,'Children');
%for i=1:length(f)
%  if ~strcmp(f(i).Name,'FLIIMP - Step 4')
%    delete(f(i).Number);
%  end
%end
%delete(handles.logfigure);
delete(gcf);


% --------------------------------------------------------------------
function Tools_Callback(hObject, eventdata, handles)
% hObject    handle to Tools (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function memdrift_Callback(hObject, eventdata, handles)
% hObject    handle to memdrift (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
FLIIMP_form_memdrift(handles);

% --------------------------------------------------------------------
function LTP_Callback(hObject, eventdata, handles)
% hObject    handle to LTP (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
FLIIMP_form_LTP(handles);

% --------------------------------------------------------------------
function Log_Callback(hObject, eventdata, handles)
% hObject    handle to Log (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.logfigure,'Visible','on');


% --- Executes when user attempts to close FLIIMP_form_E.
function FLIIMP_form_E_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to FLIIMP_form_E (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
QuitMenu_Callback(hObject, eventdata, handles);


% fin
