% create_FLIIMP_tests.m
%--------------------------------
% Description
% create synthetic IsoData files
% for testing FLIIMP during code
% changes
%--------------------------------
% HS, Wed Nov 11 18:25:17 CET 2020
%--------------------------------

clear

% load current standards
standards=FLIIMP_standards(7);

% define test settings
% offset test, no mem, no drift, no hum var, no missing, no prep, no rinse
tests(1).mem_dD=0;
tests(1).mem_d18O=0;
tests(1).mem_d17O=0;
tests(1).badInj=[];
tests(1).drift_dD=0.0;
tests(1).drift_d18O=0.0;
tests(1).drift_d17O=0.0;
tests(1).offset_dD=-12.0;
tests(1).offset_d18O=2.0;
tests(1).offset_d17O=1.0;
tests(1).H2O=20000;
tests(1).H2Ovar=0.0;
tests(1).prep=0;
tests(1).missingInj=[];
tests(1).rinses=[];
tests(1).calInj=12;
tests(1).smpInj=6;
tests(1).runID='2020-00-HS';
tests(1).standards={'VSMOW2';'SLAP2';'DI2'};
tests(1).driftInterval=3;
tests(1).driftStandard=3;
tests(1).samples={'GLW';'EVAP2';'GSM1';'EVAP';'VATS';'DI';'FIN';'BERM'};
% drift test, no mem, no hum var, no missing, no prep, no rinse
tests(2).mem_dD=0;
tests(2).mem_d18O=0;
tests(2).mem_d17O=0;
tests(2).badInj=[];
tests(2).drift_dD=4.0; % permil/day
tests(2).drift_d18O=0.5; % permil/day
tests(2).drift_d17O=0.25; % permil/day
tests(2).offset_dD=-12.0;
tests(2).offset_d18O=2.0;
tests(2).offset_d17O=1.0;
tests(2).H2O=20000;
tests(2).H2Ovar=0.0;
tests(2).prep=0;
tests(2).missingInj=[];
tests(2).rinses=[];
tests(2).calInj=12;
tests(2).smpInj=6;
tests(2).runID='2020-00-HS';
tests(2).standards={'VSMOW2';'SLAP2';'DI2'};
tests(2).driftInterval=3;
tests(2).driftStandard=3;
tests(2).samples={'GLW';'EVAP2';'GSM1';'EVAP';'VATS';'DI';'FIN';'BERM'};
% memory test, no drift, no hum var, no missing, no prep, no rinse
tests(3).mem_dD=1;
tests(3).mem_d18O=1;
tests(3).mem_d17O=1;
tests(3).badInj=[];
tests(3).drift_dD=0.0;
tests(3).drift_d18O=0.0;
tests(3).drift_d17O=0.0;
tests(3).offset_dD=-12.0;
tests(3).offset_d18O=-2.0;
tests(3).offset_d17O=-1.0;
tests(3).H2O=20000;
tests(3).H2Ovar=0.0;
tests(3).prep=0;
tests(3).missingInj=[];
tests(3).rinses=[];
tests(3).calInj=12;
tests(3).smpInj=6;
tests(3).runID='2020-00-HS';
tests(3).standards={'VSMOW2';'SLAP2';'DI2'};
tests(3).driftInterval=3;
tests(3).driftStandard=3;
tests(3).samples={'GLW';'EVAP2';'GSM1';'EVAP';'VATS';'DI';'FIN';'BERM'};
% hum var test, no drift, no mem, no missing, no prep, no rinse
tests(4).mem_dD=0;
tests(4).mem_d18O=0;
tests(4).mem_d17O=0;
tests(4).badInj=[];
tests(4).drift_dD=0.0;
tests(4).drift_d18O=0.0;
tests(4).drift_d17O=0.0;
tests(4).offset_dD=-12.0;
tests(4).offset_d18O=-2.0;
tests(4).offset_d17O=-1.0;
tests(4).H2O=20000;
tests(4).H2Ovar=3000.0;
tests(4).prep=0;
tests(4).missingInj=[];
tests(4).rinses=[];
tests(4).calInj=12;
tests(4).smpInj=6;
tests(4).runID='2020-00-HS';
tests(4).standards={'VSMOW2';'SLAP2';'DI2'};
tests(4).driftInterval=3;
tests(4).driftStandard=3;
tests(4).samples={'GLW';'EVAP2';'GSM1';'EVAP';'VATS';'DI';'FIN';'BERM'};
% missing test, no mem, no drift, no hum var, no prep, no rinse
tests(5).mem_dD=0;
tests(5).mem_d18O=0;
tests(5).mem_d17O=0;
tests(5).badInj=[13 14 51 52 113 200 201 202];
tests(5).drift_dD=0.0;
tests(5).drift_d18O=0.0;
tests(5).drift_d17O=0.0;
tests(5).offset_dD=-12.0;
tests(5).offset_d18O=2.0;
tests(5).offset_d17O=1.0;
tests(5).H2O=20000;
tests(5).H2Ovar=0.0;
tests(5).prep=0;
tests(5).missingInj=[];
tests(5).rinses=[];
tests(5).calInj=12;
tests(5).smpInj=6;
tests(5).runID='2020-00-HS';
tests(5).standards={'VSMOW2';'SLAP2';'DI2'};
tests(5).driftInterval=3;
tests(5).driftStandard=3;
tests(5).samples={'GLW';'EVAP2';'GSM1';'EVAP';'VATS';'DI';'FIN';'BERM'};
% combined test, offset, mem, drift, hum var, prep, rinse
tests(6).mem_dD=1;
tests(6).mem_d18O=1;
tests(6).mem_d17O=1;
tests(6).badInj=[13 14 51 52 113 200 201 202];
tests(6).drift_dD=4.0; % permil/day
tests(6).drift_d18O=0.5; % permil/day
tests(6).drift_d17O=0.25; % permil/day
tests(6).offset_dD=-12.0;
tests(6).offset_d18O=2.0;
tests(6).offset_d17O=1.0;
tests(6).H2O=20000;
tests(6).H2Ovar=3000.0;
tests(6).prep=1;
tests(6).missingInj=[];
tests(6).rinses=[];
tests(6).calInj=12;
tests(6).smpInj=6;
tests(6).runID='2020-00-HS';
tests(6).standards={'VSMOW2';'SLAP2';'DI2'};
tests(6).driftInterval=3;
tests(6).driftStandard=3;
tests(6).samples={'GLW';'EVAP2';'GSM1';'EVAP';'VATS';'DI';'FIN';'BERM'};


% File format
% Line,  Analysis,             Time Code,           Port,  Inj Nr,
% 1,P-6271,2020/05/30 12:10:30,1-01,1,

% d(18_16)Mean,    d(D_H)Mean,H2O_Mean,  Ignore, Good,
% -40.35,-312.8,20000,-1,1,

% Identifier 1,Identifier 2,   Gas Configuration,Timestamp Mean,
% GLW,Standard,H2O,1590840921.30,

% d(18_16)_SD,     d(D_H)_SD,H2O_SD, d(18_16)_Sl,     d(D_H)_Sl,H2O_Sl,
% 0.112,1.049,137.327,0.002,0.016,

% baseline_shift,   slope_shift,     residuals,baseline_curvature,
% 2.731,55.537,16.104,1.518,65.996,

% interval,ch4_ppm,h16od_adjust,h16od_shift,
% 0.840,-0.606,0.0000,0.0000,

% n2_flag,      DAS Temp,
% 1,42.500,

% Tray,  Sample,     Job,    Method,Error Code
% 1,1,1,FARLAB_Freshwater,0

instrument='HKDS2038';
analysis='P-%04d';
dat=datenum(2020,1,1);
dt=1/24/6; % 10 min per injection
method='FLIIMP_test';

% humidity data point
hdata.H2O_SD=100.0;
hdata.dD_SD=1.0;
hdata.d18O_SD=0.1;
hdata.d17O_SD=0.1;
hdata.dxs_SD=0.5;
hdata.e17_SD=1.0;

% pick test
for t=1:6
  
  output=sprintf('./tests/Instruments/HKDS2038/IsotopeData/HKDS2038_IsoWater_202001%02d_000000.csv',t);
  disp(['creating output file ',output])
  
  fid=fopen(output,'w');
  
  % calculate synthetic data
  fprintf(fid,'  Line,  Analysis,             Time Code,           Port,  Inj Nr, d(18_16)Mean, d(17_16)Mean, d(D_H)Mean, H2O_Mean,  Ignore, Good,Identifier 1,Identifier 2,   Gas Configuration,Timestamp Mean,d(18_16)_SD, d(17_16)_SD,    d(D_H)_SD, E17, E17_SD,H2O_SD,d(18_16)_Sl,     d(D_H)_Sl,H2O_Sl,baseline_shift,   slope_shift,     residuals,baseline_curvature,      interval,ch4_ppm,h16od_adjust,h16od_shift,n2_flag,      DAS Temp,      Tray,  Sample,     Job,    Method,Error Code\n');
  
  % offset variables
  offset_d18O=tests(t).offset_d18O;
  offset_d17O=tests(t).offset_d17O;
  offset_dD=tests(t).offset_dD;
  % keep the time the standard was measured first, for drift
  firstcal=dat;
  % write standard sequence
  lin=0;
  vial=0;
  % humidity variation field
  hfacs=[-1 2 0 -3 1 -2];
  % memory function
  a=1.1;
  b=0.4;
  w=0.83;
  mem=(w*exp(-a*(1:12))+(1-w)*exp(-b*(1:12)))./10;
  % memory initial
  if tests(t).prep==0
    s=find(strcmp(standards.names,tests(t).standards{1})==1);
    last_d18O=standards.d18O(s);
    last_d17O=standards.d17O(s);
    last_dD=standards.dD(s);
  else
    s=find(strcmp(standards.names,tests(t).standards{3})==1);
    last_d18O=standards.d18O(s);
    last_d17O=standards.d17O(s);
    last_dD=standards.dD(s);
  end
  for n=1:(length(tests(t).standards)+tests(t).prep)
    if tests(t).prep==1 && n==1
      s=find(strcmp(standards.names,tests(t).standards{3})==1);
      calInjs=1;
    else
      s=find(strcmp(standards.names,tests(t).standards{n-tests(t).prep})==1);
      calInjs=tests(t).calInj;
    end
    vial=vial+1;
    ignore=-1;
    for m=1:calInjs
      if m==4
        ignore=0;
      end
      lin=lin+1;
      dat=dat+dt;
      drift_d18O=(dat-firstcal)*tests(t).drift_d18O;
      drift_d17O=(dat-firstcal)*tests(t).drift_d17O;
      drift_dD=(dat-firstcal)*tests(t).drift_dD;
      memory_d18O=((mem(m)./2*last_d18O+(1-mem(m)./2)*(standards.d18O(s)+drift_d18O))-(standards.d18O(s)+drift_d18O))*tests(t).mem_d18O;
      memory_d17O=((mem(m)./2*last_d17O+(1-mem(m)./2)*(standards.d17O(s)+drift_d17O))-(standards.d17O(s)+drift_d17O))*tests(t).mem_d17O;
      memory_dD=((mem(m)*last_dD+(1-mem(m))*(standards.dD(s)+drift_dD))-(standards.dD(s)+drift_dD))*tests(t).mem_dD;
      % humidity variation
      hdata.H2O=tests(t).H2O+tests(t).H2Ovar*hfacs(mod(m,6)+1);
      hdata.d18O=standards.d18O(s)+drift_d18O+memory_d18O;
      hdata.d17O=standards.d17O(s)+drift_d17O+memory_d17O;
      hdata.dD=standards.dD(s)+drift_dD+memory_dD;
      hdata.dxs=hdata.dD-8.0*hdata.d18O;
      hdata.e17 = (log(hdata.d17O./1000+1) - 0.528.*log(hdata.d18O./1000+1)).*1e3;
      hdata.e17_SD = 1.0;
      hdata=FARLAB_wconc_corr(hdata,instrument,1);
      corr_d18O=hdata.d18O-hdata.d18O_corr;
      corr_d17O=hdata.d17O-hdata.d17O_corr;
      corr_dD=hdata.dD-hdata.dD_corr;
      if ~isempty(intersect(lin,tests(t).badInj)==1)
        ignore=1;
        d18O_SD=0.4;
        d17O_SD=0.4;
        dD_SD=1.0;
        e17_SD=1.0;
        interval=0.840;
      else
        ignore=-1;
        d18O_SD=0.1;
        d17O_SD=0.1;
        dD_SD=4.0;
        e17_SD=1.0;
        interval=0.100;
      end
      if isempty(intersect(lin,tests(t).missingInj))
        fprintf(fid,'%d,%s,%s,1-%02d,%d,%8.4f,%8.4f,%8.4f,%d,%d,%d,%s,%s,%s,%.1f,%5.4f,%5.4f,%5.3f,%5.4f,%5.4f,%5.1f,%5.3f,%5.3f,%5.1f,%5.2f,%5.2f,%5.3f,%5.2f,%5.3f,%5.2f,%5.3f,%5.3f,%d,%5.2f,%d,%d,%d,%s,%d\n',...
          lin,... % Line
          sprintf(analysis,vial),... % Analysis
          datestr(dat,'yyyy/mm/dd HH:MM:SS'),... % Time Code
          vial,... % Port
          m,... % Inj Nr
          standards.d18O(s)+offset_d18O+drift_d18O+memory_d18O+corr_d18O,... d(18_16)Mean
          standards.d17O(s)+offset_d17O+drift_d17O+memory_d17O+corr_d17O,... d(17_16)Mean
          standards.dD(s)+offset_dD+drift_dD+memory_dD+corr_dD,... d(D_H)Mean
          tests(t).H2O+tests(t).H2Ovar*hfacs(mod(m,6)+1),... % H2O_Mean
          ignore,... % Ignore
          1,... % Good
          standards.names{s},... Identifier 1
          sprintf('%s-%03d',tests(t).runID,vial),... % Identifier 2
          'H2O',... % Gas Configuration
          (dat-datenum(1970,1,1))*24*3600,... % Timestamp Mean
          d18O_SD,... % d(18_16)_SD
          d17O_SD,... % d(17_16)_SD
          dD_SD,... % d(D_H)_SD
          (log(standards.d17O(s)./1000+1) - 0.528.*log(standards.d18O(s)./1000+1)).*1e3,...   % e17
          e17_SD,...   % e17_SD
          100.0,... % H2O_SD
          0.001,... % d(18_16)_Sl
          0.010,... % d(D_H)_Sl
          2.000,... % H2O_Sl
          50.00,... % baseline_shift
          15.00,... % slope_shift
          1.500,... % residuals
          65.00,... % baseline_curvature
          interval,... % interval
          -0.50,... % ch4_ppm
          0.000,... % h16od_adjust
          0.000,... % h16od_shift
          1,...     % n2_flag
          42.50,... % DAS Temp
          1,...     % Tray
          vial,...  % Sample
          1,...     % Job
          method,...% Method
          0);       % Error Code
      end
    end
    % for memory calculation
    last_d18O=standards.d18O(s);
    last_d17O=standards.d17O(s);
    last_dD=standards.dD(s);
  end
  % write samples
  for n=1:length(tests(t).samples)
    s=find(strcmp(standards.names,tests(t).samples{n})==1);
    vial=vial+1;
    ignore=-1;
    for m=1:tests(t).smpInj
      if m==4
        ignore=0;
      end
      lin=lin+1;
      dat=dat+dt;
      drift_d18O=(dat-firstcal)*tests(t).drift_d18O;
      drift_d17O=(dat-firstcal)*tests(t).drift_d17O;
      drift_dD=(dat-firstcal)*tests(t).drift_dD;
      memory_d18O=((mem(m)./2*last_d18O+(1-mem(m)./2)*(standards.d18O(s)+drift_d18O))-(standards.d18O(s)+drift_d18O))*tests(t).mem_d18O;
      memory_d17O=((mem(m)./2*last_d17O+(1-mem(m)./2)*(standards.d17O(s)+drift_d17O))-(standards.d17O(s)+drift_d17O))*tests(t).mem_d17O;
      memory_dD=((mem(m)*last_dD+(1-mem(m))*(standards.dD(s)+drift_dD))-(standards.dD(s)+drift_dD))*tests(t).mem_dD;
      % humidity variation
      hdata.H2O=tests(t).H2O+tests(t).H2Ovar*hfacs(mod(m,6)+1);
      hdata.d18O=standards.d18O(s)+drift_d18O+memory_d18O;
      hdata.d17O=standards.d17O(s)+drift_d17O+memory_d17O;
      hdata.dD=standards.dD(s)+drift_dD+memory_dD;
      hdata.dxs=hdata.dD-8.0*hdata.d18O;
      hdata.e17 = (log(hdata.d17O./1000+1) - 0.528.*log(hdata.d18O./1000+1)).*1e3;
      hdata.e17_SD = 1.0;
      hdata=FARLAB_wconc_corr(hdata,instrument,1);
      corr_d18O=hdata.d18O-hdata.d18O_corr;
      corr_d17O=hdata.d17O-hdata.d17O_corr;
      corr_dD=hdata.dD-hdata.dD_corr;
      if length(intersect(lin,tests(t).badInj))==1
        ignore=1;
        d18O_SD=0.4;
        d17O_SD=0.4;
        dD_SD=1.0;
        e17_SD=1.0;
        interval=0.840;
      else
        ignore=-1;
        d18O_SD=0.1;
        d17O_SD=0.1;
        dD_SD=4.0;
        e17_SD=1.0;
        interval=0.100;
      end
      if isempty(intersect(lin,tests(t).missingInj))
        fprintf(fid,'%d,%s,%s,1-%02d,%d,%8.4f,%8.4f,%8.4f,%d,%d,%d,%s,%s,%s,%.1f,%5.4f,%5.4f,%5.3f,%5.4f,%5.4f,%5.1f,%5.3f,%5.3f,%5.1f,%5.2f,%5.2f,%5.3f,%5.2f,%5.3f,%5.2f,%5.3f,%5.3f,%d,%5.2f,%d,%d,%d,%s,%d\n',...
          lin,... % Line
          sprintf(analysis,vial),... % Analysis
          datestr(dat,'yyyy/mm/dd HH:MM:SS'),... % Time Code
          vial,... % Port
          m,... % Inj Nr
          standards.d18O(s)+offset_d18O+drift_d18O+memory_d18O+corr_d18O,... d(18_16)Mean
          standards.d17O(s)+offset_d17O+drift_d17O+memory_d17O+corr_d17O,... d(17_16)Mean
          standards.dD(s)+offset_dD+drift_dD+memory_dD+corr_dD,... d(D_H)Mean
          tests(t).H2O+tests(t).H2Ovar*hfacs(mod(m,6)+1),... % H2O_Mean
          ignore,... % Ignore
          1,... % Good
          standards.names{s},... Identifier 1
          sprintf('%s-%03d',tests(t).runID,vial),... % Identifier 2
          'H2O',... % Gas Configuration
          (dat-datenum(1970,1,1))*24*3600,... % Timestamp Mean
          d18O_SD,... % d(18_16)_SD
          d17O_SD,... % d(17_16)_SD
          dD_SD,... % d(D_H)_SD
          (log(standards.d17O(s)./1000+1) - 0.528.*log(standards.d18O(s)./1000+1)).*1e3,...   % e17
          e17_SD,...   % e17_SD
          100.0,... % H2O_SD
          0.001,... % d(18_16)_Sl
          0.010,... % d(D_H)_Sl
          2.000,... % H2O_Sl
          50.00,... % baseline_shift
          15.00,... % slope_shift
          1.500,... % residuals
          65.00,... % baseline_curvature
          interval,... % interval
          -0.50,... % ch4_ppm
          0.000,... % h16od_adjust
          0.000,... % h16od_shift
          1,...     % n2_flag
          42.50,... % DAS Temp
          1,...     % Tray
          vial,...  % Sample
          1,...     % Job
          method,...% Method
          0);       % Error Code
      end
    end
    % for memory calculation
    last_d18O=standards.d18O(s);
    last_d17O=standards.d17O(s);
    last_dD=standards.dD(s);
    % add a drift standard
    if n==tests(t).driftInterval
      s=find(strcmp(standards.names,tests(t).standards{tests(t).driftStandard})==1);
      % injection line increases, but not the vial
      vial=vial+1;
      ignore=-1;
      for m=1:tests(t).smpInj
        if m==4
          ignore=0;
        end
        lin=lin+1;
        dat=dat+dt;
        % calculate drift since last measurement
        drift_d18O=(dat-firstcal)*tests(t).drift_d18O;
        drift_d17O=(dat-firstcal)*tests(t).drift_d17O;
        drift_dD=(dat-firstcal)*tests(t).drift_dD;
        memory_d18O=((mem(m)./2*last_d18O+(1-mem(m)./2)*(standards.d18O(s)+drift_d18O))-(standards.d18O(s)+drift_d18O))*tests(t).mem_d18O;
        memory_d17O=((mem(m)./2*last_d17O+(1-mem(m)./2)*(standards.d17O(s)+drift_d17O))-(standards.d17O(s)+drift_d17O))*tests(t).mem_d17O;
        memory_dD=((mem(m)*last_dD+(1-mem(m))*(standards.dD(s)+drift_dD))-(standards.dD(s)+drift_dD))*tests(t).mem_dD;
        % humidity variation
        hdata.H2O=tests(t).H2O+tests(t).H2Ovar*hfacs(mod(m,6)+1);
        hdata.d18O=standards.d18O(s)+drift_d18O+memory_d18O;
        hdata.d17O=standards.d17O(s)+drift_d17O+memory_d17O;
        hdata.dD=standards.dD(s)+drift_dD+memory_dD;
        hdata.dxs=hdata.dD-8.0*hdata.d18O;
        hdata.e17 = (log(hdata.d17O./1000+1) - 0.528.*log(hdata.d18O./1000+1)).*1e3;
        hdata.e17_SD = 1.0;
        hdata=FARLAB_wconc_corr(hdata,instrument,1);
        corr_d18O=hdata.d18O-hdata.d18O_corr;
        corr_d17O=hdata.d17O-hdata.d17O_corr;
        corr_dD=hdata.dD-hdata.dD_corr;
        if length(intersect(lin,tests(t).badInj))==1
          ignore=1;
          d18O_SD=0.4;
          d17O_SD=0.4;
          dD_SD=1.0;
          e17_SD=1.0;
          interval=0.840;
        else
          ignore=-1;
          d18O_SD=0.1;
          d17O_SD=0.1;
          dD_SD=4.0;
          e17_SD=1.0;
          interval=0.100;
        end
        if isempty(intersect(lin,tests(t).missingInj))
          fprintf(fid,'%d,%s,%s,1-%02d,%d,%8.4f,%8.4f,%8.4f,%d,%d,%d,%s,%s,%s,%.1f,%5.4f,%5.4f,%5.3f,%5.4f,%5.4f,%5.1f,%5.3f,%5.3f,%5.1f,%5.2f,%5.2f,%5.3f,%5.2f,%5.3f,%5.2f,%5.3f,%5.3f,%d,%5.2f,%d,%d,%d,%s,%d\n',...
            lin,... % Line
            sprintf(analysis,vial),... % Analysis
            datestr(dat,'yyyy/mm/dd HH:MM:SS'),... % Time Code
            vial,... % Port
            m,... % Inj Nr
            standards.d18O(s)+offset_d18O+drift_d18O+memory_d18O+corr_d18O,... d(18_16)Mean
            standards.d17O(s)+offset_d17O+drift_d17O+memory_d17O+corr_d17O,... d(17_16)Mean
            standards.dD(s)+offset_dD+drift_dD+memory_dD+corr_dD,... d(D_H)Mean
            tests(t).H2O+tests(t).H2Ovar*hfacs(mod(m,6)+1),... % H2O_Mean
            ignore,... % Ignore
            1,... % Good
            standards.names{s},... Identifier 1
            sprintf('%s-%03d',tests(t).runID,vial),... % Identifier 2
            'H2O',... % Gas Configuration
            (dat-datenum(1970,1,1))*24*3600,... % Timestamp Mean
            d18O_SD,... % d(18_16)_SD
            d17O_SD,... % d(17_16)_SD
            dD_SD,... % d(D_H)_SD
            (log(standards.d17O(s)./1000+1) - 0.528.*log(standards.d18O(s)./1000+1)).*1e3,...   % e17
            e17_SD,...   % e17_SD
            100.0,... % H2O_SD
            0.001,... % d(18_16)_Sl
            0.010,... % d(D_H)_Sl
            2.000,... % H2O_Sl
            50.00,... % baseline_shift
            15.00,... % slope_shift
            1.500,... % residuals
            65.00,... % baseline_curvature
            interval,... % interval
            -0.50,... % ch4_ppm
            0.000,... % h16od_adjust
            0.000,... % h16od_shift
            1,...     % n2_flag
            42.50,... % DAS Temp
            1,...     % Tray
            vial,...  % Sample
            1,...     % Job
            method,...% Method
            0);       % Error Code
        end
      end
      % for memory calculation
      last_d18O=standards.d18O(s);
      last_d17O=standards.d17O(s);
      last_dD=standards.dD(s);
    end
  end
  
  % write inverse standard sequence
  for n=length(tests(t).standards):-1:1
    s=find(strcmp(standards.names,tests(t).standards{n})==1);
    % injection line increases, but not the vial
    vial=vial+1;
    ignore=-1;
    for m=1:tests(t).calInj
      if m==4
        ignore=0;
      end
      lin=lin+1;
      dat=dat+dt;
      % calculate drift since last measurement
      drift_d18O=(dat-firstcal)*tests(t).drift_d18O;
      drift_d17O=(dat-firstcal)*tests(t).drift_d17O;
      drift_dD=(dat-firstcal)*tests(t).drift_dD;
      memory_d18O=((mem(m)./2*last_d18O+(1-mem(m)./2)*(standards.d18O(s)+drift_d18O))-(standards.d18O(s)+drift_d18O))*tests(t).mem_d18O;
      memory_d17O=((mem(m)./2*last_d17O+(1-mem(m)./2)*(standards.d17O(s)+drift_d17O))-(standards.d17O(s)+drift_d17O))*tests(t).mem_d17O;
      memory_dD=((mem(m)*last_dD+(1-mem(m))*(standards.dD(s)+drift_dD))-(standards.dD(s)+drift_dD))*tests(t).mem_dD;
      % humidity variation
      hdata.H2O=tests(t).H2O+tests(t).H2Ovar*hfacs(mod(m,6)+1);
      hdata.d18O=standards.d18O(s)+drift_d18O+memory_d18O;
      hdata.d17O=standards.d17O(s)+drift_d17O+memory_d17O;
      hdata.dD=standards.dD(s)+drift_dD+memory_dD;
      hdata.dxs=hdata.dD-8.0*hdata.d18O;
      hdata.e17 = (log(hdata.d17O./1000+1) - 0.528.*log(hdata.d18O./1000+1)).*1e3;
      hdata.e17_SD = 1.0;
      hdata=FARLAB_wconc_corr(hdata,instrument,1);
      corr_d18O=hdata.d18O-hdata.d18O_corr;
      corr_d17O=hdata.d17O-hdata.d17O_corr;
      corr_dD=hdata.dD-hdata.dD_corr;
      if length(intersect(lin,tests(t).badInj))==1
        ignore=1;
        d18O_SD=0.4;
        d17O_SD=0.4;
        dD_SD=1.0;
        e17_SD=1.0;
        interval=0.840;
      else
        ignore=-1;
        d18O_SD=0.1;
        d17O_SD=0.1;
        dD_SD=4.0;
        e17_SD=1.0;
        interval=0.100;
      end
      if isempty(intersect(lin,tests(t).missingInj))
        fprintf(fid,'%d,%s,%s,1-%02d,%d,%8.4f,%8.4f,%8.4f,%d,%d,%d,%s,%s,%s,%.1f,%5.4f,%5.4f,%5.3f,%5.4f,%5.4f,%5.1f,%5.3f,%5.3f,%5.1f,%5.2f,%5.2f,%5.3f,%5.2f,%5.3f,%5.2f,%5.3f,%5.3f,%d,%5.2f,%d,%d,%d,%s,%d\n',...
          lin,... % Line
          sprintf(analysis,vial),... % Analysis
          datestr(dat,'yyyy/mm/dd HH:MM:SS'),... % Time Code
          vial,... % Port
          m,... % Inj Nr
          standards.d18O(s)+offset_d18O+drift_d18O+memory_d18O+corr_d18O,... d(18_16)Mean
          standards.d17O(s)+offset_d17O+drift_d17O+memory_d17O+corr_d17O,... d(17_16)Mean
          standards.dD(s)+offset_dD+drift_dD+memory_dD+corr_dD,... d(D_H)Mean
          tests(t).H2O+tests(t).H2Ovar*hfacs(mod(m,6)+1),... % H2O_Mean
          ignore,... % Ignore
          1,... % Good
          standards.names{s},... Identifier 1
          sprintf('%s-%03d',tests(t).runID,vial),... % Identifier 2
          'H2O',... % Gas Configuration
          (dat-datenum(1970,1,1))*24*3600,... % Timestamp Mean
          d18O_SD,... % d(18_16)_SD
          d17O_SD,... % d(17_16)_SD
          dD_SD,... % d(D_H)_SD
          (log(standards.d17O(s)./1000+1) - 0.528.*log(standards.d18O(s)./1000+1)).*1e3,...   % e17
          e17_SD,...   % e17_SD
          100.0,... % H2O_SD
          0.001,... % d(18_16)_Sl
          0.010,... % d(D_H)_Sl
          2.000,... % H2O_Sl
          50.00,... % baseline_shift
          15.00,... % slope_shift
          1.500,... % residuals
          65.00,... % baseline_curvature
          interval,... % interval
          -0.50,... % ch4_ppm
          0.000,... % h16od_adjust
          0.000,... % h16od_shift
          1,...     % n2_flag
          42.50,... % DAS Temp
          1,...     % Tray
          vial,...  % Sample
          1,...     % Job
          method,...% Method
          0);       % Error Code
      end
    end
    % for memory calculation
    last_d18O=standards.d18O(s);
    last_d17O=standards.d17O(s);
    last_dD=standards.dD(s);
  end
  
  fclose(fid);
  
end

% fin
