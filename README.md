 **Using FLIIMP**
 
 See detailed instructions regarding [Download](https://git.app.uib.no/GFI-public/fliimp/-/wikis/Download-compiled-FLIIMP), [Installation](https://git.app.uib.no/GFI-public/fliimp/-/wikis/Installation) and [Documentation](https://git.app.uib.no/GFI-public/fliimp/-/wikis/Documentation) in the Wiki.

 **Key scientific reference**:

 Sodemann, H., Mørkved, P. T., Wahl, S., 2023: FLIIMP - a community software for the processing, calibration, and reporting of liquid water isotope measurements on cavity-ring down spectrometers, MethodsX, 11, 102297, [https://doi.org/10.1016/j.mex.2023.102297](https://doi.org/10.1016/j.mex.2023.102297)