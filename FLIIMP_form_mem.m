% -----------------------------------------------------------------------------------
% last changes: HS, 02.01.2020
% FLIIMP Version 1.6
% -----------------------------------------------------------------------------------
function varargout = FLIIMP_form_mem(varargin)
% FLIIMP_form_mem MATLAB code for FLIIMP_form_mem.fig
%      FLIIMP_form_mem, by itself, creates a new FLIIMP_form_mem or raises the existing
%      singleton*.
%
%      H = FLIIMP_form_mem returns the handle to a new FLIIMP_form_mem or the handle to
%      the existing singleton*.
%
%      FLIIMP_form_mem('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FLIIMP_form_mem.M with the given input arguments.
%
%      FLIIMP_form_mem('Property','Value',...) creates a new FLIIMP_form_mem or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before FLIIMP_form_mem_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to FLIIMP_form_mem_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help FLIIMP_form_mem

% Last Modified by GUIDE v2.5 07-Apr-2023 12:14:02

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
  'gui_Singleton',  gui_Singleton, ...
  'gui_OpeningFcn', @FLIIMP_form_mem_OpeningFcn, ...
  'gui_OutputFcn',  @FLIIMP_form_mem_OutputFcn, ...
  'gui_LayoutFcn',  [] , ...
  'gui_Callback',   []);
if nargin && ischar(varargin{1})
  gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
  [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
  gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before FLIIMP_form_mem is made visible.
function FLIIMP_form_mem_OpeningFcn(hObject, ~, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to FLIIMP_form_mem (see VARARGIN)

% apply position information
if isfield(varargin{1},'position')
  if length(varargin{1}.position)>2
    set(hObject,'position',varargin{1}.position{3});
  end
  handles.position=varargin{1}.position;
end

% Choose default command line output for FLIIMP_form_mem
handles.output = hObject;
handles.logfigure = varargin{1}.logfigure;
handles.logtext = varargin{1}.logtext;

set(hObject,'Name','FLIIMP - Step 3');

if ~isempty(varargin)
  % copy settings if applicable
  if isfield(varargin{1},'settings')
    handles.settings=varargin{1}.settings;
    handles.inputPath=varargin{1}.inputPath;
    handles.outputPath=varargin{1}.outputPath;
  end
end

% create mem structure if it does not yet exist
if ~isfield(handles.settings,'mem')
  % set the default fitting parameters
  handles.settings.mem.alpha(1:3)=1.1;
  handles.settings.mem.beta(1:3)=0.4;
  handles.settings.mem.b(1:3)=0.83;
  handles.settings.mem.c0(1:3)=4;
  handles.settings.mem.rmse(1:3)=0;
  handles.settings.mem.minInj(1:3)=10;
  handles.settings.mem.avgInj=3;
  handles.settings.mem.enableCorrection(1:3)=0;
  handles.settings.mem.standards='';
else
  if ~isfield(handles.settings.mem,'minInj')
    handles.settings.mem.minInj(1:3)=10;
  end
  if ~isfield(handles.settings.mem,'avgInj')
    handles.settings.mem.avgInj=3;
  end
  if ~isfield(handles.settings.mem,'standards')
    handles.settings.mem.standards='';
  end
end

if length(handles.settings.mem.alpha)<3
  % set the default 17O fitting parameters
  handles.settings.mem.alpha(3)=1.1;
  handles.settings.mem.beta(3)=0.4;
  handles.settings.mem.b(3)=0.83;
  handles.settings.mem.c0(3)=4;
  handles.settings.mem.rmse(3)=0;
  handles.settings.mem.minInj(3)=12;
  handles.settings.mem.avgInj=3;
  handles.settings.mem.enableCorrection(3)=0;
  handles.settings.mem.standards='';
end

% Update handles structure
guidata(hObject, handles);

% load the data file
settings=handles.settings;

% create intermediate structure with paths
lsettings=settings;
lsettings.inputPath=handles.inputPath;
lsettings.outputPath=handles.outputPath;

% read in the files which is pointed at
data=FLIIMP_read_liquid_injections(handles,lsettings);

if isempty(data)
  logf(handles,'No data could be loaded. Please check your settings and restart.');
  return
end

% identify 17O-mode
if isfield(data,'d17O')
  field={'d18O_corr';'dD_corr';'d17O_corr';'dxs_corr';'e17_corr'};
  opts={'d18O (raw)';'dD (raw)';'d17O (raw)'};
else
  field={'d18O_corr';'dD_corr';'dxs_corr'};
  opts={'d18O (raw)';'dD (raw)'};
end
set(handles.parameterPopup,'String',opts);

% apply settings to input fields
idx=get(handles.parameterPopup,'Value');
set(handles.alphaEdit,'String',num2str(handles.settings.mem.alpha(idx)));
set(handles.betaEdit,'String',num2str(handles.settings.mem.beta(idx)));
set(handles.weightEdit,'String',num2str(handles.settings.mem.b(idx)));
set(handles.correctCheckbox,'Value',handles.settings.mem.enableCorrection(idx));
set(handles.minInjEdit,'String',handles.settings.mem.minInj(idx));
set(handles.avgInjEdit,'String',handles.settings.mem.avgInj);
set(handles.standardsEdit,'String',handles.settings.mem.standards);
set(handles.editC0,'String',num2str(handles.settings.mem.c0(idx),'%4.2f'));
set(handles.labelRMSE,'String',num2str(handles.settings.mem.rmse(idx),'%8.4f'));
set(handles.labelMaxmem,'String',num2str(handles.settings.mem.c0(idx),'%8.2f%%'));

% remove bad injections, i.e. where the sampling frequency dropped to below 0.8 Hz
bad=find(data.interval>1);
for f=1:length(field)
  data.(field{f})(bad)=NaN;
end

% set excluded injections to NaN
for e=1:length(settings.excludeInj)
  for v=1:length(settings.excludeInj{e})
    if settings.excludeInj{e}(v)>=0
      idx=find(data.SampleLine==e & data.InjNr==settings.excludeInj{e}(v));
      if ~isempty(idx)
        for f=1:length(field)
          data.(field{f})(idx)=NaN;
        end
      end
    end
  end
end

% use humidity correction here based on settings in previous form
data=FARLAB_wconc_corr(data,settings.instrument,settings.useHumidityCorrection);

% perform memory calculation
mdata=FLIIMP_memory_analysis(data,settings.excludeInj,settings.mem.avgInj);

% remove however many samples are to be ignored from dataset altogether
%field=fieldnames(data);
%mfield=fieldnames(mdata);
%for v=length(settings.ignoreVials):-1:1,
%  idx=(data.SampleLine==settings.ignoreVials(v));
%  for f=1:length(field),
%    data.(field{f})(idx)=[];
%  end
%  idx=(mdata.SampleLine==settings.ignoreVials(v));
%  for f=1:length(mfield),
%    mdata.(mfield{f})(idx)=[];
%  end
%end

% store current position
handles.position{3}=get(handles.output,'Position');

handles.mdata=mdata;
eidx=~ismember(mdata.SampleLine,handles.settings.ignoreVialsCal);
handles.maxInjNr=max(mdata.maxInjNr(eidx));
handles.settings=settings;
handles.settingsBackup=settings; % use for cancel button
guidata(hObject,handles);
updateFit_Callback(handles.parameterPopup,0,handles);

% UIWAIT makes FLIIMP_form_mem wait for user response (see UIRESUME)
% uiwait(handles.FLIIMP_form_mem);


% --- Outputs from this function are returned to the command line.
function varargout = FLIIMP_form_mem_OutputFcn(hObject, ~, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in parameterPopup.
function parameterPopup_Callback(hObject, ~, handles)
% hObject    handle to parameterPopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns parameterPopup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from parameterPopup

% get current settings
idx=get(handles.parameterPopup,'Value');
set(handles.alphaEdit,'String',num2str(handles.settings.mem.alpha(idx)));
set(handles.betaEdit,'String',num2str(handles.settings.mem.beta(idx)));
set(handles.weightEdit,'String',num2str(handles.settings.mem.b(idx)));
set(handles.correctCheckbox,'Value',handles.settings.mem.enableCorrection(idx));
set(handles.minInjEdit,'String',handles.settings.mem.minInj(idx));
set(handles.avgInjEdit,'String',handles.settings.mem.avgInj);
set(handles.editC0,'String',num2str(handles.settings.mem.c0(idx),'%4.2f'));

% refresh plot
updateFit_Callback(hObject,0,handles);


% --- Executes during object creation, after setting all properties.
function parameterPopup_CreateFcn(hObject, ~, handles)
% hObject    handle to parameterPopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in sampleList.
function updateFit_Callback(hObject, ~, handles)
% hObject    handle to sampleList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns sampleList contents as cell array
%        contents{get(hObject,'Value')} returns selected item from sampleList

idx=get(handles.parameterPopup,'Value');
ytitles={'mem d18O / %','mem dD / %','mem d17O / %','mem d-excess / %','mem 17O-excess / %'};
variables={'d18O_mem','dD_mem','d17O_mem','dxs_mem','e17_mem'};

% identify 17O-mode
if isfield(handles.mdata,'d17O')
  field={'d18O_mem';'dD_mem';'d17O_mem';'dxs_mem';'e17_mem'};
else
  field={'d18O_mem';'dD_mem';'dxs_mem'};
end

% set excluded injections to NaN
for e=1:length(handles.settings.excludeInj)
  for v=1:length(handles.settings.excludeInj{e})
    if handles.settings.excludeInj{e}(v)>=0
      idx1=find(handles.mdata.SampleLine==e & handles.mdata.InjNr==handles.settings.excludeInj{e}(v));
      if ~isempty(idx1)
        for f=1:length(field)
          handles.mdata.(field{f})(idx1)=NaN;
        end
      end
    end
  end
end

% select between isotope parameters
clear h;
idx=get(handles.parameterPopup,'Value');
var=variables{get(handles.parameterPopup,'Value')};
cla(handles.isoAxes);
hold(handles.isoAxes,'on');
% exclude vials with less than required injections
midx=(handles.mdata.maxInjNr>=handles.settings.mem.minInj(idx));
% exclude manually excluded vials
eidx=~ismember(handles.mdata.SampleLine,handles.settings.ignoreVialsCal);
% include only standards as chosen
if ~isempty(handles.settings.mem.standards)
  str=strsplit(handles.settings.mem.standards);
  sidx=zeros(size(midx));
  for i=1:length(str)
    sidx=sidx | strcmp(handles.mdata.Identifier1,str{i});
  end
  midx=[midx & eidx] & sidx;
else
  midx=[midx & eidx];
end
mdat=handles.mdata.(var)(midx);
mweight=handles.mdata.mstep(midx,idx);
handles.maxInjNr=max(handles.mdata.maxInjNr(midx));
m=[];
s=[];
for i=1:handles.maxInjNr
  saidx=handles.mdata.InjNr(midx)==i;
  naidx=find(isnan(mdat(saidx))==0);
  mmdat=mdat(saidx);
  mmweight=mweight(saidx);
  m(i)=sum(mmdat(naidx).*(mmweight(naidx)/sum(mmweight(naidx),'omitnan')),'omitnan');
  %m(i)=mean(mdat(saidx),'omitnan');
  s(i)=std(mdat(saidx),'omitnan');
end
if ~isempty(m)
  errorbar(handles.isoAxes,1:handles.maxInjNr,m,s,'kx');
  h(1)=plot(handles.isoAxes,handles.mdata.InjNr(midx),mdat,'x','Color',[0.4 0.4 0.4],'MarkerSize',3);
  set(handles.isoAxes,'XLim',[0 handles.maxInjNr+1]);
  set(handles.isoAxes,'YAxisLocation','right');
  ylabel(handles.isoAxes,ytitles{get(handles.parameterPopup,'Value')});
  xlabel(handles.isoAxes,'Injection nr.');
  box(handles.isoAxes,'on');
  
  b=handles.settings.mem.b(idx);
  beta=handles.settings.mem.beta(idx);
  alpha=handles.settings.mem.alpha(idx);
  
  % determine c0
  %c0=mean(mdat(handles.mdata.InjNr(midx)==1),'omitnan')/(b*exp(-alpha)+(1-b)*exp(-beta));
  %c0=mean(mdat(handles.mdata.InjNr(midx)==min(handles.mdata.InjNr(midx))),'omitnan');
  %c0=handles.settings.mem.c0(idx);
  c0=m(1);
  for i=1:handles.maxInjNr
    c(i)=c0*(b*exp(-alpha*(i-1))+(1-b)*exp(-beta*(i-1)));
    d(i)=c0*(b*exp(-alpha*(i-1)));
    e(i)=c0*((1-b)*exp(-beta*(i-1)));
    % last injection remains uncorrected
    if i>(handles.maxInjNr-1)
      c(i)=0;
      d(i)=0;
      e(i)=0;
    end
  end
  for i=1:(handles.maxInjNr-1)
    c(i)=c(i)-c(handles.maxInjNr-1)*(1-(handles.maxInjNr-i)/(handles.maxInjNr-1));
    d(i)=d(i)-d(handles.maxInjNr-1)*(1-(handles.maxInjNr-i)/(handles.maxInjNr-1));
    e(i)=e(i)-e(handles.maxInjNr-1)*(1-(handles.maxInjNr-i)/(handles.maxInjNr-1));
  end
  h(2)=plot(handles.isoAxes,1:handles.maxInjNr,c,'r*-');
  h(3)=plot(handles.isoAxes,1:handles.maxInjNr,d,'g-');
  h(4)=plot(handles.isoAxes,1:handles.maxInjNr,e,'b-');
  legend(h,{'Injections';'Combined';'Fast memory';'Slow memory'},'Location','NorthEast');
  legend(handles.isoAxes,'boxoff');
  
  % output a RMSE value
  rmse=sqrt(mean(m-c).^2);
  logf(handles,'RMSE of fit for %s: %8.4f',var,rmse);
  set(handles.labelRMSE,'String',num2str(rmse,'%8.4f'));
  set(handles.labelMaxmem,'String',num2str(c(1),'%8.2f%%'));
  
  handles.settings.mem.c0(idx)=c0;
  handles.settings.mem.rmse(idx)=rmse;
  guidata(hObject,handles);
  
else
  logf(handles,'Too few injections to display memory fit curve.');
end

% --- Executes on button press in resetButton.
function resetButton_Callback(hObject, ~, handles)
% hObject    handle to resetButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% store current position
handles.position{3}=get(handles.output,'Position');

idx=get(handles.parameterPopup,'Value');

% set the default fitting parameters
handles.settings.mem.alpha(idx)=handles.settingsBackup.mem.alpha(idx);
handles.settings.mem.beta(idx)=handles.settingsBackup.mem.beta(idx);
handles.settings.mem.b(idx)=handles.settingsBackup.mem.b(idx);
handles.settings.mem.c0(idx)=handles.settingsBackup.mem.c0(idx);
handles.settings.mem.rmse(idx)=0;
set(handles.labelRMSE,'String',num2str(rmse,'%8.4f'));
handles.settings.mem.standards='';

% apply settings to input fields
set(handles.alphaEdit,'String',num2str(handles.settings.mem.alpha(idx)));
set(handles.betaEdit,'String',num2str(handles.settings.mem.beta(idx)));
set(handles.weightEdit,'String',num2str(handles.settings.mem.b(idx)));
set(handles.standardsEdit,'String',num2str(handles.settings.mem.standards));

guidata(hObject,handles);
updateFit_Callback(handles.parameterPopup, 0, handles);


% --- Executes on button press in backButton.
function backButton_Callback(hObject, ~, handles)
% hObject    handle to backButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.settings=handles.settingsBackup;
handles.position{3}=get(handles.output,'Position');
%set(gcf,'Visible','off');
close(gcf);
FLIIMP_form_preproc(handles);


% --- Executes on button press in nextButton.
function nextButton_Callback(hObject, ~, handles)
% hObject    handle to nextButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.position{3}=get(handles.output,'Position');
%set(gcf,'Visible','off');
close(gcf);
FLIIMP_form_preproc(handles);


% --- Executes on button press in saveButton.
function saveButton_Callback(hObject, ~, handles)
% hObject    handle to saveButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles=guidata(hObject);
settings=handles.settings;
[newfile newpath]=uiputfile({'*.mat','MATLAB data file'},'Save run settings to...',[handles.outputPath,'/',settings.filename]);
if ~isequal(newpath,0) && ~isequal(newfile,0)
  save(fullfile(newpath,newfile),'settings');
  handles.settings.filename=newfile;
  guidata(hObject,handles);
end


% --- Executes on button press in loadButton.
function loadButton_Callback(hObject, eventdata, handles)
% hObject    handle to loadButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[newfile newpath]=uigetfile({'*.mat','MATLAB data file'},'Load run settings from...',handles.outputPath);
% save log file during loading
logfile=handles.settings.logfile;
if ~isequal(newpath,0) && ~isequal(newfile,0)
  load(fullfile(newpath,newfile));
  settings.lid=handles.settings.lid;
  settings.logfile=logfile;
  settings.filename=newfile;
else
  return
end
handles.settings=settings;
guidata(hObject,handles);

% --- Executes on button press in correctCheckbox.
function correctCheckbox_Callback(hObject, eventdata, handles)
% hObject    handle to correctCheckbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of correctCheckbox
idx=get(handles.parameterPopup,'Value');
handles.settings.mem.enableCorrection(idx)=get(hObject,'Value');
guidata(hObject,handles);


function alphaEdit_Callback(hObject, eventdata, handles)
% hObject    handle to alphaEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of alphaEdit as text
%        str2double(get(hObject,'String')) returns contents of alphaEdit as a double
idx=get(handles.parameterPopup,'Value');
handles.settings.mem.alpha(idx)=str2double(get(handles.alphaEdit,'String'));
if ~isnan(handles.settings.mem.alpha(idx))
  guidata(hObject,handles);
else
  logf(handles,'Error in fast parameter a, use dot as decimal separator.\n');
  handles.settings.mem
end
updateFit_Callback(handles.parameterPopup, 0, handles);


% --- Executes during object creation, after setting all properties.
function alphaEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to alphaEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over updateButton.
function updateButton_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to updateButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
updateFit_Callback(handles.parameterPopup, 0, handles);

function betaEdit_Callback(hObject, eventdata, handles)
% hObject    handle to betaEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of betaEdit as text
%        str2double(get(hObject,'String')) returns contents of betaEdit as a double
idx=get(handles.parameterPopup,'Value');
handles.settings.mem.beta(idx)=str2double(get(handles.betaEdit,'String'));
if ~isnan(handles.settings.mem.beta(idx))
  guidata(hObject,handles);
else
  logf(handles,'Error in slow parameter b, use dot as decimal separator.\n');
  handles.settings.mem
end
updateFit_Callback(handles.parameterPopup, 0, handles);


% --- Executes during object creation, after setting all properties.
function betaEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to betaEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end


function weightEdit_Callback(hObject, eventdata, handles)
% hObject    handle to weightEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of weightEdit as text
%        str2double(get(hObject,'String')) returns contents of weightEdit as a double
idx=get(handles.parameterPopup,'Value');
handles.settings.mem.b(idx)=str2double(get(handles.weightEdit,'String'));
if ~isnan(handles.settings.mem.b(idx))
  guidata(hObject,handles);
else
  logf(handles,'Error in parameter weight, use dot as decimal separator.\n');
  handles.settings.mem
end
updateFit_Callback(handles.parameterPopup, 0, handles);


% --- Executes during object creation, after setting all properties.
function weightEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to weightEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in autofitButton.
function autofitButton_Callback(hObject, eventdata, handles)
% hObject    handle to autofitButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

idx=get(handles.parameterPopup,'Value');
ytitles={'d18O / permil','dD / permil','d17O / permil','d-excess / permil','17O-excess / permeg'};
variables={'d18O_mem','dD_mem','d17O_mem','dxs_mem','e17_mem'};

% select between isotope parameters
var=variables{get(handles.parameterPopup,'Value')};
% exclude vials with less than required injections
midx=(handles.mdata.maxInjNr>=handles.settings.mem.minInj(idx));
% exclude manually excluded vials
eidx=~ismember(handles.mdata.SampleLine,handles.settings.ignoreVialsCal);
% include only standards as chosen
if ~isempty(handles.settings.mem.standards)
  str=strsplit(handles.settings.mem.standards);
  sidx=zeros(size(midx));
  for i=1:length(str)
    sidx=sidx | strcmp(handles.mdata.Identifier1,str{i});
  end
  midx=[midx & eidx] & sidx;
else
  midx=[midx & eidx];
end
mdat=handles.mdata.(var)(midx);
mweight=handles.mdata.mstep(midx,idx);
handles.maxInjNr=max(handles.mdata.maxInjNr(midx));
for i=1:handles.maxInjNr
  saidx=handles.mdata.InjNr(midx)==i;
  naidx=find(isnan(mdat(saidx))==0);
  mmdat=mdat(saidx);
  mmweight=mweight(saidx);
  m(i)=sum(mmdat(naidx).*(mmweight(naidx)/sum(mmweight(naidx),'omitnan')),'omitnan');
  %m(i)=mean(mdat(saidx),'omitnan');
  s(i)=std(mdat(saidx),'omitnan');
end

% vary parameters within a reasonable range
logf(handles,'Starting automatic fitting started...');
minrmse=1e5;
minalpha=1e5;
minbeta=1e5;
for b=0.1:0.02:0.95
  for alpha=0.5:0.02:3.0
    for beta=0.0:0.02:0.5
      % determine c0
      %c0=mean(mdat(handles.mdata.InjNr(midx)==min(handles.mdata.InjNr(midx))),'omitnan')/(b*exp(-alpha)+(1-b)*exp(-beta));
      %c0=mean(mdat(handles.mdata.InjNr(midx)==min(handles.mdata.InjNr(midx))),'omitnan');
      c0=m(1);
      for i=1:handles.maxInjNr
        c(i)=c0*(b*exp(-alpha*(i-1))+(1-b)*exp(-beta*(i-1)));
        if i>(handles.maxInjNr-1)
          c(i)=0;
        end
      end
      for i=1:(handles.maxInjNr-1)
        c(i)=c(i)-c(handles.maxInjNr-1)*(1-(handles.maxInjNr-i)/(handles.maxInjNr-1));
      end
      rmse=sqrt(mean(abs(m-c).^2,'omitnan'));
      if rmse<minrmse
        minrmse=rmse;
        minb=b;
        minalpha=alpha;
        minbeta=beta;
        minc0=c0;
        %logf(handles,'.');
        set(handles.labelRMSE,'String',num2str(rmse,'%8.4f'));
        set(handles.labelMaxmem,'String',num2str(c(1),'%8.2f%%'));
        set(handles.editC0,'String',num2str(c0,'%4.2f'));
      end
    end
  end
end

% output a RMSE value
logf(handles,'RMSE of best fit for %s: %8.4f',var,minrmse);
logf(handles,'Parameter values alpha: %5.2f, beta: %5.2f, b: %4.2f',minalpha,minbeta,minb);

handles.settings.mem.alpha(idx)=minalpha;
handles.settings.mem.beta(idx)=minbeta;
handles.settings.mem.b(idx)=minb;
handles.settings.mem.c0(idx)=minc0;
set(handles.editC0,'String',num2str(minc0,'%4.2f'));
handles.settings.mem.rmse(idx)=minrmse;
set(handles.labelRMSE,'String',num2str(minrmse,'%8.4f'));
set(handles.labelMaxmem,'String',num2str(c(1),'%8.2f%%'));
guidata(hObject,handles);
set(handles.alphaEdit,'String',num2str(handles.settings.mem.alpha(idx)));
set(handles.betaEdit,'String',num2str(handles.settings.mem.beta(idx)));
set(handles.weightEdit,'String',num2str(handles.settings.mem.b(idx)));
updateFit_Callback(handles.parameterPopup, 0, handles);

function minInjEdit_Callback(hObject, eventdata, handles)
% hObject    handle to minInjEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of minInjEdit as text
%        str2double(get(hObject,'String')) returns contents of minInjEdit as a double
idx=get(handles.parameterPopup,'Value');
handles.settings.mem.minInj(idx)=str2double(get(hObject,'String'));
if isnan(handles.settings.mem.minInj)
  logf(handles,'Error in parameter Min. Injections, use dot as decimal separator.');
  handles.settings.mem
else
  guidata(hObject,handles);
  updateFit_Callback(handles.parameterPopup, 0, handles);
end


% --- Executes during object creation, after setting all properties.
function minInjEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to minInjEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end


function standardsEdit_Callback(hObject, eventdata, handles)
% hObject    handle to standardsEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of standardsEdit as text
%        str2double(get(hObject,'String')) returns contents of standardsEdit as a double
handles.settings.mem.standards=get(hObject,'String');
guidata(hObject,handles);
updateFit_Callback(handles.parameterPopup, 0, handles);


% --- Executes during object creation, after setting all properties.
function standardsEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to standardsEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end



function avgInjEdit_Callback(hObject, eventdata, handles)
% hObject    handle to avgInjEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of avgInjEdit as text
%        str2double(get(hObject,'String')) returns contents of avgInjEdit as a double
idx=get(handles.parameterPopup,'Value');
handles.settings.mem.avgInj=str2double(get(hObject,'String'));
if isnan(handles.settings.mem.avgInj)
  logf(handles,'Error in parameter Averaging Injections, use dot as decimal separator.');
  handles.settings.mem
else
  guidata(hObject,handles);
  updateFit_Callback(handles.parameterPopup, 0, handles);
end


% --- Executes during object creation, after setting all properties.
function avgInjEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to avgInjEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end



function editC0_Callback(hObject, eventdata, handles)
% hObject    handle to editC0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editC0 as text
%        str2double(get(hObject,'String')) returns contents of editC0 as a double
idx=get(handles.parameterPopup,'Value');
handles.settings.mem.c0(idx)=str2double(get(hObject,'String'));
if isnan(handles.settings.mem.c0(idx))
  logf(handles,'Error in parameter c0, use dot as decimal separator.');
  handles.settings.mem
else
  guidata(hObject,handles);
  updateFit_Callback(handles.parameterPopup, 0, handles);
end


% --- Executes during object creation, after setting all properties.
function editC0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editC0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end


% --------------------------------------------------------------------
function FileMenu_Callback(hObject, eventdata, handles)
% hObject    handle to FileMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function ApplyMenu_Callback(hObject, eventdata, handles)
% hObject    handle to ApplyMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

nextButton_Callback(hObject, eventdata, handles);

% --------------------------------------------------------------------
function CancelMenu_Callback(hObject, eventdata, handles)
% hObject    handle to CancelMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

backButton_Callback(hObject, eventdata, handles);


% --------------------------------------------------------------------
function Tools_Callback(hObject, eventdata, handles)
% hObject    handle to Tools (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function memdrift_Callback(hObject, eventdata, handles)
% hObject    handle to memdrift (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function LTP_Callback(hObject, eventdata, handles)
% hObject    handle to LTP (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Log_Callback(hObject, eventdata, handles)
% hObject    handle to Log (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.logfigure,'Visible','on');


% --- Executes when user attempts to close FLIIMP_form_D.
function FLIIMP_form_D_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to FLIIMP_form_D (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
backButton_Callback(hObject, eventdata, handles);

% fin
