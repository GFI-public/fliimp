% handle = dimfigure (handle, width, height)
%--------------------------------
% Description
% creates a new figure with the
% given number and width/height
% parameters
%--------------------------------
% HS, Do 20 Aug 2009 15:03:00 PDT 
%--------------------------------

function h = dimfigure(handle,width,height,varargin)

  try
    set(0,'CurrentFigure',handle);
  catch
    figure(handle)
  end
  clf
  set(handle,'color','w')
  set(handle, 'PaperUnits', 'centimeters');
  set(handle, 'PaperSize', [width height]);
  set(handle, 'PaperPosition', [0.5 0.5 width-0.5 height-0.5]);
  if length(varargin)>0
      set(handle, 'NumberTitle', 'off', 'Name', varargin{1})
  end
  h=handle;

end

% fin
