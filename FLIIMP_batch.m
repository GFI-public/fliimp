% FLIIMP_batch.m
% input arguments: structure with members
% inputPath, outputPath (string)
% to allow replacing default settings
%-----------------
% batch process FLIIMP using saved settings

function FLIIMP_batch(filepattern,varargin)

  global installdir
  installdir='/Users/hso039/Dropbox/FARLAB/matlab/FLIIMP/src';
  
  global config;
  config=FLIIMP_config(installdir);

  if nargin==0
    error('Please provide name or pattern of settings files (*.mat) in current directory as arguments.');
  end

  % Include default input and output paths
  if exist('~/FLIIMP_settings.mat','file')
    load('~/FLIIMP_settings.mat'); % into variable paths
  else
    paths.inputPath='/Volumes/farlab';
    paths.outputPath='/Volumes/farlab/projects';
  end

  % process optional argument
  if nargin==2
    if isfield(varargin{1},'inputPath')
      paths.inputPath=varargin{1}.inputPath;
    end
    if isfield(varargin{1},'outputPath')
      paths.outputPath=varargin{1}.outputPath;
    end
    if isfield(varargin{1},'writeReport')
      paths.writeReport=varargin{1}.writeReport;
    end
    if isfield(varargin{1},'installDir')
      config=FLIIMP_config(varargin{1}.installDir);
    end
  else
    disp('Using default path settings.');
  end

  sfiles=dir(filepattern);
  
  if length(sfiles)<1
      error('no settings files found for pattern "%s".',filepattern);
  end

  for i=1:length(sfiles)
    
    load(sfiles(i).name);

    % open log file
    settings.logfile=tempname;
    settings.lid=fopen(settings.logfile,'w');
    if settings.lid<0
        logf(handles,'WARNING: Could not open log file.');
    end
    settings.timeInterpolation=0;
    settings.inputPath=paths.inputPath;
    settings.outputPath=paths.outputPath;
    if isfield(paths,'writeReport')
      settings.writeReport=paths.writeReport;
    end

    settings
    
    % create handles that do not require gui for logging
    handles.settings=settings;
    
    % create intermediate structure with paths
    [cdata mdata]=FLIIMP_liquid_calibration(handles,settings);
    if isfield(cdata,'name1')
      %logf(handles,'%s\n',cdata.name1{:});
      logf(handles,'Calibration completed for %d samples. See report file for details',length(cdata.name1));
    else
      logf(handles,'No samples calibrated. Please check your settings.');
    end
    logf(handles,'---------------------------------------------------------------------');

    % move log file to output directory
    fclose(handles.settings.lid);
    if settings.writeReport==0
      rep='_reporting';
    else
      rep='_internal';
    end
    rdir=sprintf('%s/%s-%s%s_%s',settings.outputPath,settings.project,settings.runID,rep,datestr(settings.reportDate,'yyyymmdd'));
    try
      movefile(settings.logfile,sprintf('%s/calibration_%s.log',rdir,datestr(settings.reportDate,'yyyymmdd')));
    catch
      logf(handles,'WARNING: Clould not archive calibration log file.');
    end

end
