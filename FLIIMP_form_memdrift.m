function varargout = FLIIMP_form_memdrift(varargin)
% FLIIMP_FORM_MEMDRIFT MATLAB code for FLIIMP_form_memdrift.fig
%      FLIIMP_FORM_MEMDRIFT, by itself, creates a new FLIIMP_FORM_MEMDRIFT or raises the existing
%      singleton*.
%
%      H = FLIIMP_FORM_MEMDRIFT returns the handle to a new FLIIMP_FORM_MEMDRIFT or the handle to
%      the existing singleton*.
%
%      FLIIMP_FORM_MEMDRIFT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FLIIMP_FORM_MEMDRIFT.M with the given input arguments.
%
%      FLIIMP_FORM_MEMDRIFT('Property','Value',...) creates a new FLIIMP_FORM_MEMDRIFT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before FLIIMP_form_memdrift_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to FLIIMP_form_memdrift_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help FLIIMP_form_memdrift

% Last Modified by GUIDE v2.5 03-Jan-2023 15:27:16
% read configuration
global config;
config=FLIIMP_config;

% assign install directory in case FLIIMP is not run from source directory
addpath(config.installdir)

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @FLIIMP_form_memdrift_OpeningFcn, ...
                   'gui_OutputFcn',  @FLIIMP_form_memdrift_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before FLIIMP_form_memdrift is made visible.
function FLIIMP_form_memdrift_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to FLIIMP_form_memdrift (see VARARGIN)

% select the latest calibration set
global calset;
global config;
global driftparms;

% Choose default command line output for FLIIMP_form_memdrift
handles.output = hObject;
handles.settings.lid=varargin{1}.settings.lid;
handles.logtext=varargin{1}.logtext;

% Update handles structure
guidata(hObject, handles);

% This sets up the initial plot - only do when we are invisible
% so window can get raised using FLIIMP_form_memdrift.
if strcmp(get(hObject,'Visible'),'off')
    plot(rand(3),'color','w');
end

axes(handles.axesMem);
cla;

[~,snum]=FLIIMP_standards(1);
calset=FLIIMP_standards(max(snum));

% Populate instrument selector with available instruments
set(handles.popupInstrument,'string',config.deviceNames);

% Populate parameter selector with available parameters
driftparms={'drift_bias_d18O';'drift_bias_dD';'internal_reproducibility_d18O';'internal_reproducibility_dD';'c0_d18O';'alpha_d18O';...
    'beta_d18O';'b_d18O';'RMSE_d18O';'c0_dD';'alpha_dD';'beta_dD';'b_dD';'RMSE_dD';'injections_per_standard';'samples_count';'injections_per_sample';'total_injections'};
set(handles.popupParameter,'string',driftparms);


% UIWAIT makes FLIIMP_form_memdrift wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = FLIIMP_form_memdrift_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --------------------------------------------------------------------
function FileMenu_Callback(hObject, eventdata, handles)
% hObject    handle to FileMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% --------------------------------------------------------------------
function OpenMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to OpenMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
file = uigetfile('*.fig');
if ~isequal(file, 0)
    open(file);
end

% --------------------------------------------------------------------
function PrintMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to PrintMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
printdlg(handles.figure1)

% --------------------------------------------------------------------
function CloseMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to CloseMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
selection = questdlg(['Close ' get(handles.figure1,'Name') '?'],...
                     ['Close ' get(handles.figure1,'Name') '...'],...
                     'Yes','No','Yes');
if strcmp(selection,'No')
    return;
end

delete(handles.figure1)


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
     set(hObject,'BackgroundColor','white');
end

set(hObject, 'String', {'plot(rand(5))', 'plot(sin(1:0.01:25))', 'bar(1:.5:10)', 'plot(membrane)', 'surf(peaks)'});


% --- Executes on selection change in popupInstrument.
function popupInstrument_Callback(hObject, eventdata, handles)
% hObject    handle to popupInstrument (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupInstrument contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupInstrument

global params

syears=get(handles.editStart,'String');
eyears=get(handles.editEnd,'String');
syear=datenum(syears,'yyyymmdd');
if ~isempty(eyears)
  eyear=datenum(eyears,'yyyymmdd');
else
  eyear=now;
  logf(handles.settings.lid,'WARNING: No valid end date specified, using %s.\n',datestr(now));
end
if isfield(params,'start_date')
  plotMem(syear,eyear,handles);
end

% --- Executes during object creation, after setting all properties.
function popupInstrument_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupInstrument (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function editStart_Callback(hObject, eventdata, handles)
% hObject    handle to editStart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editStart as text
%        str2double(get(hObject,'String')) returns contents of editStart as a double

global params

syears=get(handles.editStart,'String');
eyears=get(handles.editEnd,'String');
syear=datenum(syears,'yyyymmdd');
if ~isempty(eyears)
  eyear=datenum(eyears,'yyyymmdd');
else
  eyear=now;
  logf(handles.settings.lid,'WARNING: No valid end date specified, using %s.\n',datestr(now));
end
if isfield(params,'start_date')
  plotMem(syear,eyear,handles);
end


% --- Executes during object creation, after setting all properties.
function editStart_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editStart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function editEnd_Callback(hObject, eventdata, handles)
% hObject    handle to editEnd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editEnd as text
%        str2double(get(hObject,'String')) returns contents of editEnd as a double

global params

syears=get(handles.editStart,'String');
eyears=get(handles.editEnd,'String');
syear=datenum(syears,'yyyymmdd');
if ~isempty(eyears)
  eyear=datenum(eyears,'yyyymmdd');
else
  eyear=now;
  logf(handles.settings.lid,'WARNING: No valid end date specified, using %s.\n',datestr(now));
end
if isfield(params,'start_date')
  plotMem(syear,eyear,handles);
end


% --- Executes during object creation, after setting all properties.
function editEnd_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editEnd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in buttonUpdate.
function buttonUpdate_Callback(hObject, eventdata, handles)
% hObject    handle to buttonUpdate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global config;

fprintf('Scanning available calibration results:\n');

clear params
global params
global calset
runcnt=0;

% get input parameters
syears=get(handles.editStart,'String');
eyears=get(handles.editEnd,'String');
try
  syear=eval(syears(1:4));
catch
  logf(handles.settings.lid,'WARNING: No valid start date specified. Use format YYYYMMDD.\n');
  return
end
if ~isempty(eyears)
  eyear=eval(eyears(1:4));
else
  eyear=eval(datestr(now,'yyyy'));
  logf(handles.settings.lid,'WARNING: No valid end date specified, using %d.\n',eyear);
end

% loop through selected years
params={};
p=1;
for year=syear:eyear
    
    projects=dir(sprintf('%s/Projects/%d/%d-*',config.defaultInputPath,year,year));
    
    for i=1:length(projects)
        fprintf('.');
        runs=dir(sprintf('%s/%s/*',projects(i).folder,projects(i).name));
        for j=1:length(runs)
            parfile=dir(sprintf('%s/%s/*parameters.csv',runs(j).folder,runs(j).name));
            parfile = parfile(arrayfun(@(x) ~strcmp(x.name(1),'.'),parfile));
            if length(parfile)==1
              % parse parameter file into structure
              pid=fopen([parfile(1).folder,'/',parfile(1).name],'r');
              while ~feof(pid) 
                st=split(fgets(pid),','); 
                sa=replace(replace(st{1},'"',''),' ','_');
                sb=eval(st{2});
                if isnumeric(sb)
                  params.(sa)(p)=sb;
                else
                  if contains(sa,'_date')
                    params.(sa)(p)=datenum(sb{:},'yyyy-mm-dd HH:MM');
                  else
                    params.(sa){p}=sb{:};
                  end
                end
              end
              %params
              
              p=p+1;
            end
            accfile=dir(sprintf('%s/%s/*accounting.csv',runs(j).folder,runs(j).name));
            accfile = accfile(arrayfun(@(x) ~strcmp(x.name(1),'.'),accfile));
            allfile=dir(sprintf('%s/%s/*alldata.csv',runs(j).folder,runs(j).name));
            allfile = allfile(arrayfun(@(x) ~strcmp(x.name(1),'.'),allfile));
            % get date and instrument
            if length(accfile)==1000
                %opts=detectImportOptions([accfile(1).folder,'/',accfile(1).name], 'NumHeaderLines', 1, 'FileType','text','NumVariables',7); 
                %opts.VariableNamesLine=1;
                %adata=table2struct(readtable([accfile(1).folder,'/',accfile(1).name],opts));
                [accfile(1).folder,'/',accfile(1).name]
                adata=table2struct(readtable([accfile(1).folder,'/',accfile(1).name]));
                try 
                  if ~isfield(adata,'Lab_Filename')
                    accdata=split(adata(1).FARLABFilename,'_');
                  else
                    accdata=split(adata(1).Lab_Filename,'_');
                  end
                  instrument=find(strcmp(config.devices,accdata{1})==1);
                  dat=datenum([accdata{end-1},accdata{end}(1:6)],'yyyymmddHHMMSS');
                catch
                  logf(handles.settings.lid,'WARNING: error reading accdata file, found wrong number of column names.\n');
                end                  
            end
            % get standards
            if length(allfile)==1000
                runcnt=runcnt+1;
                %fprintf('\n');
                if i==1
                    disp(accfile(1).folder)
                end
                disp(runs(j).name)
                sprintf('%s/%s/*alldata.csv',runs(j).folder,runs(j).name);
                %opts=detectImportOptions([allfile(1).folder,'/',allfile(1).name], 'NumHeaderLines', 1); 
                %opts.VariableNamesLine=1;
                %data=table2struct(readtable([allfile(1).folder,'/',allfile(1).name],opts));
                [allfile(1).folder,'/',allfile(1).name]
                data=table2struct(readtable([allfile(1).folder,'/',allfile(1).name]));
                if size(fieldnames(data))~=34
                    logf(handles.settings.lid,'WARNING: error reading alldata file, found %d columns instead of 34.\n',size(fieldnames(data)));
                else
                    % merge to long file with standards only
                    for l=1:length(data)
                        data(l).Instrument=instrument;
                        data(l).Date=dat+l*1/24/6; % add 10 min per sample
                        data(l).Standard=0;
                        data(l).Project=projects(i).name;
                        data(l).Run=runs(j).name;
                        data(l).RunCnt=runcnt;
                        fnames=fieldnames(data);
                        for s=1:length(calset.names)
                            standard=calset.names{s};
                            if strcmp({data(l).Sample_name},standard)==1
                                data(l).Standard=s;
                                for k=1:length(fnames)
                                    f=data(l).(fnames{k});
                                    if isfield(alldata,fnames{k})
                                        alldata.(fnames{k})=[alldata.(fnames{k}),f];
                                    else
                                        if ischar(f)
                                            alldata.(fnames{k})={f};
                                        else
                                            alldata.(fnames{k})=f;
                                        end
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end
    end
end

fprintf('Scanning complete.\n');

%params

syears=get(handles.editStart,'String');
eyears=get(handles.editEnd,'String');
try
  syear=datenum(syears,'yyyymmdd');
catch
  logf(handles.settings.lid,'WARNING: No valid start date specified. Use format YYYYMMDD.\n');
  return
end
if ~isempty(eyears)
  eyear=datenum(eyears,'yyyymmdd');
else
  eyear=now;
  logf(handles.settings.lid,'WARNING: No valid end date specified, using %s.\n',datestr(now));
end
if isfield(params,'start_date')
  plotMem(syear,eyear,handles);
else
  logf(handles.settings.lid,'WARNING: No run parameter files *parameters.csv found for date range %s to %s in folder %s. Please check time range and path.\n',syears,eyears,config.defaultInputPath);
end


function editStandard_Callback(hObject, eventdata, handles)
% hObject    handle to editStandard (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editStandard as text
%        str2double(get(hObject,'String')) returns contents of editStandard as a double


% --- Executes during object creation, after setting all properties.
function editStandard_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editStandard (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function plotMem(syears,eyears,handles)

global params
global config
global driftparms

cols={'r';'b';'c';'k';'m'};
parmtitles={'drift bias d18O';'drift bias dD';'internal reproducibility d18O';'internal reproducibility dD';'c0 d18O';'alpha d18O';...
    'beta d18O';'b d18O';'RMSE d18O';'c0 dD';'alpha dD';'beta dD';'b dD';'RMSE dD';'injections per standard';'samples count';'injections per sample';'total injections'};
parmunits ={'permil';'permil';'permil';'permil';'%';'1';'1';'1';'permil';'%';'1';'1';'1';'permil';'1';'1';'1';'1'};

% FontSize
fs=9;

%drange=[min(alldata.Date)-5 max(alldata.Date)+5];
drange=[syears,eyears];

s=get(handles.popupParameter,'Value');
i=get(handles.popupInstrument,'Value');

axes(handles.axesMem);
cla;

%params
% return immediately if nothing to display
if ~isfield(params,'start_date')
  return
end

clear h;
idx=[];
k=1;
%idx=find((alldata.Instrument==i)&(alldata.Standard==s)& alldata.Date>drange(1) & alldata.Date<drange(2));
for r=1:length(params.instrument_serial_number)
  if strcmp(params.instrument_serial_number{r},config.devices{i})==1
    idx(k)=r;
    k=k+1;
  end
end
%idx=1:length(params.start_date);

if ~isempty(idx)    
    h=plot(params.start_date(idx),params.(driftparms{s})(idx),'x','Color',cols{1});
    set(gca,'TickDir','out','XLim',drange,'FontSize',fs);
    datetick('x','yyyy-mm','keeplimits');
    ylabel(sprintf('%s / %s',parmtitles{s},parmunits{s}),'FontSize',fs);
    box on
end
if exist('h','var')
    legend(gca,h,config.devices{i});
    legend boxoff
    %  print('-dpng','-r200',sprintf('../fig/FARLAB_picarro_drift_raw_%d_%s.png',year,standard));
end


% --- Executes on selection change in popupParameter.
function popupParameter_Callback(hObject, eventdata, handles)
% hObject    handle to popupParameter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupParameter contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupParameter

global params

% get input parameters
syears=get(handles.editStart,'String');
eyears=get(handles.editEnd,'String');
try
  syear=datenum(syears,'yyyymmdd');
catch
  logf(handles.settings.lid,'WARNING: No valid start date specified. Use format YYYYMMDD.\n');
  return
end

if ~isempty(eyears)
  eyear=datenum(eyears,'yyyymmdd');
else
  eyear=now;
  logf(handles.settings.lid,'WARNING: No valid end date specified, using %s.\n',datestr(now));
end
if isfield(params,'start_date')
  plotMem(syear,eyear,handles);
end

% --- Executes during object creation, after setting all properties.
function popupParameter_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupParameter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupIsotope.
function popupIsotope_Callback(hObject, eventdata, handles)
% hObject    handle to popupIsotope (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupIsotope contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupIsotope

global params

syears=get(handles.editStart,'String');
eyears=get(handles.editEnd,'String');
try
  syear=datenum(syears,'yyyymmdd');
catch
  logf(handles.settings.lid,'WARNING: No valid start date specified. Use format YYYYMMDD.\n');
  return
end
  
if ~isempty(eyears)
  eyear=datenum(eyears,'yyyymmdd');
else
  eyear=now;
  logf(handles.settings.lid,'WARNING: No valid end date specified, using %s.\n',datestr(now));
end
if isfield(params,'start_date')
  plotMem(syear,eyear,handles);
end


% --- Executes during object creation, after setting all properties.
function popupIsotope_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupIsotope (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
