% -----------------------------------------------------------------------------------
% FLIIMP_liquid_calibration(settings)
% -----------------------------------------------------------------------------------
% Returns results of Picarro measurements by liquid injections
% Method for use in FLIIMP
% -----------------------------------------------------------------------------------
% last changes: HS, 20.10.2017
% FLIIMP Version 1.0
% last changes: HS, 14.06.2018
% FLIIMP Version 1.2
% last changes: HS, 16.08.2018
% FLIIMP Version 1.3
% last changes: HS, 05.10.2018
% FLIIMP Version 1.4
% added memory information
% remove injections with low sampling frequency
% last changes: HS, 07.09.2019
% FLIIMP Version 1.5
% Manual injection removal
% FLIIMP Version 1.6
% Memory correction procedure
% Manual renaming
% d

function [cout, mout] = FLIIMP_liquid_calibration(handles,settings)

global config;
if isempty(config)
  config=FLIIMP_config;
end

cout = [];
mout = [];
tf={'Off';'On'}; % text for True/False

% add new parameter with default value, if not present
if ~isfield(settings,'mem')
  settings.useMemoryCorrection=0;
else
  if ~isfield(settings.mem,'avgInj')
    settings.mem.avgInj=4;
  end
end
if ~isfield(settings,'useHumidityCorrection')
  settings.useHumidityCorrection=1;
end
if ~isfield(settings,'useDriftCorrection')
  settings.useDriftCorrection=1;
end
if ~isfield(settings,'useMemoryCorrection')
  settings.useMemoryCorrection=1;
end
if ~isfield(settings,'previewDrift')
  settings.previewDrift=0;
end
if ~isfield(settings,'previewCal')
  settings.previewCal=0;
end
if ~isfield(settings,'ignoreInjectionsCal')
  if isfield(settings,'mem')
    % before V1.9, recalculate c0 to account for removal of injection offset
    % from earlier versions of FLIIMP
    for i=1:3
      alpha=settings.mem.alpha(i);
      beta=settings.mem.beta(i);
      b=settings.mem.b(i);
      c0=settings.mem.c0(i);
      % calculate for large number of injections
      settings.mem.c0(i)=c0*(b*exp(-alpha)+(1-b)*exp(-beta));
    end
  end
  settings.ignoreInjectionsCal=settings.ignoreInjections;
end
settings.ignoreVialsCal=find(settings.ignoreInjectionsCal==1);
if ~isfield(settings,'comment')
  settings.comment='';
end


% default: plot all figures
injfig=1; % injection overview
resfig=1; % residuals and drift
driftfig=1; %  drift overview
rawfig=1; %  raw data plot
stdfig=1; %  standard overview
sumfig=1; %  calibration summary
calsumfig=1; %  overview of calibrated samples
memfig=1; %  memory overview
indfig=1; %  individual figures
preview=0; % default is not for preview
% if drift preview requested, only plot driftfig
if settings.previewDrift==1
  preview=1;
  injfig=0;
  resfig=0;
  driftfig=1;
  rawfig=0;
  stdfig=0;
  sumfig=0;
  calsumfig=0;
  memfig=0;
  indfig=0;
elseif settings.previewCal==1
  preview=1;
  injfig=0;
  driftfig=0;
  resfig=0;
  rawfig=0;
  stdfig=1;
  sumfig=0;
  calsumfig=0;
  memfig=0;
  indfig=0;
end

%addpath('/Users/hso039/Documents/matlab/picarro');

% laboratory and IAEA standards: expected delta value ?? precision
calcor=FLIIMP_standards(settings.calibrationRun);

% pre-definitions for writing codes
iso = {'d18O','dD','d17O','H2O','dxs','e17','DASTemp','baseline_shift','slope_shift','residuals','ch4_ppm','H2O_Sl','date'};
isoSD = {'d18O_SD','dD_SD','d17O_SD','H2O_SD','dxs_SD','e17_SD'};
labels = {'\delta^{18}O / permil','\deltaD / permil','\delta^{17}O / permil',...
  'Water Conc. / ppmv','d-excess / permil','\Delta^{17}e / permeg',...
  'DAS temp / C','baseline shift / ppb cm-1','slope shift /  ppb cm-1',...
  'residuals / ppb cm-1','CH_4 / ppmv','H2O Slope','MATLAB date'};

% font sizes, initialisation
fs = 6;  % normal fontsize
fs2= 8;  % title fontsize
fig = 10;
showfig = 1; % set to zero for output debugging
figres = '-r200';
figwid = '900px';

% transfer settings
ends      = settings.averageInjections-1; % choose number of injections to calculate mean
% zero means average over all
if ends<0
  ends=0;
end
ends1     = settings.averageFirstInjections-1; % choose number of injections to calculate mean
% zero means average over all
if ends1<0
  ends1=0;
end
del_vials = settings.ignoreVials;        % choose vials to ignore
%time_int  = settings.timeInterpolation;  % do not time interpolation

% load data
data = FLIIMP_read_liquid_injections(handles,settings);
if isempty(data)
  logf(handles,'ERROR: no files found for date range at path %s',path);
  return
end

LTP=config.longTermReproducibility;

% remove commas and semicolon if existing from standardNames
settings.standardNames=strrep(settings.standardNames,',','');
settings.standardNames=strrep(settings.standardNames,';','');
settings.standardsDrift=strrep(settings.standardsDrift,',','');
settings.standardsDrift=strrep(settings.standardsDrift,';','');
standards=length(settings.standardNames);

% translate calibration type to points and interpolation
if settings.calibrationType<3
  settings.calibrationPoints=2;
else
  settings.calibrationPoints=3;
end
if settings.calibrationType==1 || settings.calibrationType==3
  settings.calibrationInterpolation=1;
else
  settings.calibrationInterpolation=2;
end

% create different folder name for customer
if settings.writeReport==2
  rep='_reporting';
  logf(handles,'Writing short report for reporting to customer.');
else
  rep='_internal';
  logf(handles,sprintf('Writing long report for internal use at %s.',config.labname));
end

% inform about 17-O mode analysis
if settings.includeE17==1
  logf(handles,'Processing of delta 17-O and 17-O excess included.');
  figwid17 = '1200px';
else
  logf(handles,'No processing for delta 17-O and 17-O excess.');
  figwid17 = '900px';
end

% create directory for output and figures
rdir=sprintf('%s/%s-%s%s_%s',settings.outputPath,settings.project,settings.runID,rep,datestr(settings.reportDate,'yyyymmdd'));
if ~exist(rdir,'dir')
  try
    mkdir(rdir);
  catch
    logf(handles,'ERROR: could not create output directory %s. Please check output path settings.',rdir);
    return
  end
end
fdir=sprintf('%s/img_%s',rdir,settings.runID);
if ~exist(fdir,'dir')
  mkdir(fdir);
end
hfdir=sprintf('./img_%s',settings.runID); % use relative location in HTML file

% copy the logo to the image folder
try
  copyfile([config.installdir,'/',config.logofile],fdir,'f');
catch
  logf(handles,'ERROR: could not copy logo file. Check installdir in FLIIMP_config.csv and target write permissions.');
end

% open report file
fid=fopen(sprintf('%s/index.html',rdir),'w');
fprintf(fid,'<html><head>\n');
fprintf(fid,'<style>body {font: normal 12pt Helvetica,Arial; }</style>\n');
% refresh page continuously
%fprintf(fid,'<meta http-equiv="refresh" content="5;" />');
fprintf(fid,'</head><body>\n');

% calibration and plotting
%---------------------------------------------------------------

% correct isotopic dependency on water concentration
data.dD_uc=data.dD;
data.dD_SD_uc=data.dD_SD;
data.d18O_uc=data.d18O;
data.d18O_SD_uc=data.d18O_SD;
data.dxs_uc=data.dxs;
data.dxs_SD_uc=data.dxs_SD;
% include 17O data for processing
if ~isfield(data,'d17O')
  % create zero data to allow processing at high precision if requested
  data.d17O=zeros(size(data.d18O));
  data.d17O_SD=zeros(size(data.d18O_SD));
  data.e17=zeros(size(data.d18O));
  data.e17_SD=zeros(size(data.d18O));
end
data.d17O_uc=data.d17O;
data.d17O_SD_uc=data.d17O_SD;
data.e17_uc=data.e17;
data.e17_SD_uc=data.e17_SD;

% if requested, corr for wconc corrected, SD does not need further correction
data=FARLAB_wconc_corr(data,settings.instrument,settings.useHumidityCorrection);

% perform memory calculation
mdata=FLIIMP_memory_analysis(data,settings.excludeInj,settings.mem.avgInj);

% remove bad injections from mdata
trsh=mean(data.interval,'omitnan')+min(10*std(data.interval,'omitnan'),0.01);
bad=find(mdata.interval>trsh);
if ~isempty(bad)
  logf(handles,'Found bad injections at:       ');
  logf(handles,'%d ',bad);
  for i=[1 2 3 5 6]
    data.(iso{i})(bad)=NaN;
    mdata.(iso{i})(bad)=NaN;
    data.(isoSD{i})(bad)=NaN;
    mdata.(isoSD{i})(bad)=NaN;
    mdata.([iso{i},'_mem'])(bad)=NaN;
  end
end

% fill data for standards into struct certall
for v = 1:length(calcor.d18O)
  for i = 1:3
    certall(v).name1 = (calcor.names{v});
    certall(v).name2 = '';
    certall(v).(iso{i}) = calcor.(iso{i})(v);
    certall(v).(isoSD{i}) = calcor.(isoSD{i})(v);
  end
end
for v = 1:standards
  s = find(strcmpi(calcor.names,settings.standardNames{v}));
  if (isempty(s))
    logf(handles,'ERROR: Unknown calibration standard name: %s\n',settings.standardNames{v});
    logf(handles,'       Standard names must be one of this list:\n       ');
    logf(handles,'%s ',calcor.names{:});
    return
  end
  certs(v) = certall(s);
end
for v = 1:length(settings.standardsDrift)
  s = find(strcmpi(calcor.names,settings.standardsDrift{v}));
  if (isempty(s))
    logf(handles,'ERROR: Unknown drift standard name: %s\n',settings.standardsDrift{v});
    logf(handles,'       Standard names must be one of this list:\n       ');
    logf(handles,'%s ',calcor.names{:});
    return
  end
  certd(v) = certall(s);
end

% split vials
vials = sum(diff(data.InjNr)<1)+1;
vialstart = [1;find(diff(data.InjNr)<1)+1;length(data.InjNr)+1];
str = fieldnames(data);
for v = 1:vials
  for l = 1:length(str)
    vial(v).(str{l}) = data.(str{l})(vialstart(v):vialstart(v+1)-1);
  end
end

% rename if applicable
if isfield(settings,'rename')
  for i=1:vials
    for j=1:length(vial(i).Identifier1)
      vial(i).Identifier1{j}=settings.rename.Identifier1{i};
      vial(i).Identifier2{j}=settings.rename.Identifier2{i};
    end
  end
end

% find the max number of injections per sequential sample
maxInjNr=0;
for v=1:vials
  if ~ismember(v,settings.ignoreVials)
    vial(v).maxInjNr=max(vial(v).InjNr);
    maxInjNr=max(maxInjNr,vial(v).maxInjNr);
  end
end

% correct for memory effect
clear c;
c=zeros(maxInjNr,3);
for k=1:3
  alpha=settings.mem.alpha(k);
  beta=settings.mem.beta(k);
  b=settings.mem.b(k);
  c0=settings.mem.c0(k);
  % calculate for large number of injections
  for i=1:maxInjNr
    mfc(i,k)=c0*(b*exp(-alpha*(i-1))+(1-b)*exp(-beta*(i-1)));
    mfd(i,k)=c0*(b*exp(-alpha*(i-1)));
    mfe(i,k)=c0*((1-b)*exp(-beta*(i-1)));
    if i>(maxInjNr-1) % last injection will remain uncorrected
      mfc(i,k)=0;
      mfd(i,k)=0;
      mfe(i,k)=0;
    end
  end
  for i=1:(maxInjNr-1)
    mfc(i,k)=mfc(i,k)-mfc(maxInjNr-1,k)*(1-(maxInjNr-i)/(maxInjNr-1));
    mfd(i,k)=mfd(i,k)-mfd(maxInjNr-1,k)*(1-(maxInjNr-i)/(maxInjNr-1));
    mfe(i,k)=mfe(i,k)-mfe(maxInjNr-1,k)*(1-(maxInjNr-i)/(maxInjNr-1));
  end
end

% correct raw data
if settings.includeE17==1
  maxk=3;
else
  maxk=2;
end
for k=1:maxk % for all relevant primary isotopes
  for j=1:vials
    % initialize with raw corr value
    vial(j).(iso{k})=vial(j).([iso{k},'_corr']);
  end
  % only do actual correction if requested
  if settings.mem.enableCorrection(k)==1 && settings.useMemoryCorrection==1
    for j=1:vials
      if j>1
        if ~ismember(j,settings.ignoreVials)
          nmax=length(vial(j-1).InjNr);
          for i=1:length(vial(j).InjNr)
            mf=1-mfc(i,k)/100.0;
            % if not NaN in previous vial last injections, use memory correction
            premem=mean(vial(j-1).([iso{k},'_corr'])(max(1,nmax-settings.mem.avgInj):nmax),'omitnan');
            if ~isnan(premem)
              % use mean of last avgInj injections as memory reference
              vial(j).(iso{k})(i)=(vial(j).([iso{k},'_corr'])(i)-(1-mf)*premem)/mf;
              % copy corrected isotope data to array data
              data.(iso{k})(vial(j).Line(i))=vial(j).(iso{k})(i);
            end
          end
        else % do nothing
          %for i=1:length(vial(j).InjNr),
          %  vial(j).(iso{k})(i)=vial(j).([iso{k},'_corr'])(i);
          %  data.(iso{k})(vial(j).Line(i))=vial(j).(iso{k})(i);
          %end
        end
      end
    end
  end
end
% update d-excess and e17 after corrections
vial(j).dxs=vial(j).dD-8*vial(j).d18O;
% recalculated e17
if settings.includeE17==1
  vial(j).e17=(log(vial(j).d17O./1000+1)-0.528.*log(vial(j).d18O./1000+1)).*1e6;
end

% remove manually excluded injections
if isfield(settings,'excludeInj')
  %settings.excludeInj
  for v = 1:vials
    excl=settings.excludeInj{v};
    if excl(1)>-1
      vial(v).d18O(excl)=NaN;
      vial(v).dD(excl)=NaN;
      vial(v).dxs(excl)=NaN;
      vial(v).d18O_corr(excl)=NaN;
      vial(v).dD_corr(excl)=NaN;
      vial(v).dxs_corr(excl)=NaN;
      if settings.includeE17==1
        vial(v).d17O(excl)=NaN;
        vial(v).e17(excl)=NaN;
        vial(v).d17O_corr(excl)=NaN;
        vial(v).e17_corr(excl)=NaN;
      end
      logf(handles,'Manually excluded %d injections (%s) in vial %d (%s)',length(excl),sprintf('%d ',excl),v,vial(v).Identifier1{1});
    end
  end
end

% remove vials if requested
clear dvial;
if ~isempty(del_vials)
  dvial=vial(del_vials);
  for i=del_vials'
    logf(handles,'Deleting vial %2d',i);
  end
  vial(del_vials)=[];
  vials=length(vial);
  % also remove from exclude injection list
  settings.excludeInj(del_vials)=[];
end

% also remove from memory data
%mfields=fieldnames(mdata);
%for v=length(settings.ignoreVials):-1:1
%  idx=(mdata.SampleLine==settings.ignoreVials(v));
%  for f=1:length(mfields)
%    mdata.(mfields{f})(idx,:)=[];
%  end
%end

% calculate the measured value
% - average of the last (ends+1) injections
% - proper estimation from vial overview: standards last 6 (4-8) injections, samples last 3 injections
for v = 1:vials
  % assign specific ends value to each vial
  if v==1
    if ends1>0
      vial(v).ends=ends1;
    else
      vial(v).ends=length(vial(v).InjNr)-1;
    end
  else
    if ends>0
      vial(v).ends=ends;
    else
      vial(v).ends=length(vial(v).InjNr)-1;
    end
  end
  meas(v).name1 = vial(v).Identifier1{end}; % assign vial name1 from 'Identifier1'
  meas(v).name2 = vial(v).Identifier2{end}; % assign vial name2 from 'Identifier2'
  meas(v).filename = vial(v).Filename{end}; % assign vial filename from 'Filename'
  % correct spelling errors
  if strcmpi(meas(v).name1,'VSMOV2')
    meas(v).name1='VSMOW2';
  elseif strcmpi(meas(v).name1,'DI_DRIFT')
    meas(v).name1='DI';
  elseif strcmpi(meas(v).name1,'SEA II')
    meas(v).name1='SEAII';
  elseif strcmpi(meas(v).name1,'SEA OLD')
    meas(v).name1='SEAOLD';
  elseif strcmpi(meas(v).name1,'SEA')
    meas(v).name1='SEAold';
  end
  for i = 1:length(iso)
    if length(vial(v).(iso{i}))<vial(v).ends
      logf(handles,'ERROR: Not enough injections for included vial %d (%s). Please review exclusion list.',v,meas(v).name1);
      return;
    end
    meas(v).(iso{i}) = mean(vial(v).(iso{i})(end-vial(v).ends:end),'omitnan');
    if i~=4 && i<7
      meas(v).([iso{i},'_uc']) = mean(vial(v).([iso{i},'_uc'])(end-vial(v).ends:end),'omitnan');
    end
    if isnan(meas(v).(iso{i}))
      logf(handles,'WARNING: NaN measurement for included vial %d (%s). Please review exclusion list.',v,meas(v).name1);
    end
  end
  % recalc e17 after averaging (non-linear quantity)
  if settings.includeE17==1
    meas(v).e17=(log(meas(v).d17O./1000+1)-0.528.*log(meas(v).d18O./1000+1)).*1e6;
  end
  for i = 1:length(isoSD)
    len=sum(~isnan(vial(v).(iso{i})(end-vial(v).ends:end)));
    meas(v).(isoSD{i}) = std(vial(v).(iso{i})(end-vial(v).ends:end),'omitnan')/sqrt(len); % standard deviation of the mean
    if i~=4 && i<7
      meas(v).([isoSD{i},'_uc']) = std(vial(v).([iso{i},'_uc'])(end-vial(v).ends:end),'omitnan')/sqrt(len); % standard deviation of the mean for uncorrected data
    end
  end
  meas(v).InjNr=max(vial(v).InjNr);
end

% detect vials for calibration standards
for s = 1:standards
  stdrep{s} = find(strcmpi({meas.name1},settings.standardNames{s}));
end

% detect vials for drift standards
for s = 1:length(settings.standardsDrift)
  stddri{s} = find(strcmpi({meas.name1},settings.standardsDrift{s}));
end

if rawfig==1
  % create overview figure of raw data
  %------------------------------------------------------------------------
  fig = 10;
  dimfigure(fig,20,10);
  set(fig,'Visible','Off');
  clf
  for i = 1:4
    h(fig*10+i) = subplot(2,2,i);
    hold on
    if (i==3 && settings.includeE17==1) || i~=3
      if i<4
        %errorbar(data.([iso{i},'_uc'])-data.(iso{i}),data.(isoSD{i}),'k.');
        plot(data.([iso{i},'_uc'])-data.(iso{i}),'k.');
        plot(data.([iso{i},'_uc'])-data.([iso{i},'_corr']),'b.');
      else
        %errorbar(data.(iso{4})-20000,data.(isoSD{4}),'k.');
        plot(data.(iso{4})-20000,'k.');
      end
      %errorbarwidth(eh,-10);
      ylabel(labels{i},'FontSize',fs);
      xlabel('injection number','FontSize',fs);
      set(gca,'box','on','ygrid','on','xgrid','on','FontSize',fs,'TickDir','out');
      set(gca,'XLim',[0 length(data.Line)]);
      axis tight
    else
      axis off
    end
  end
  if settings.writeReport==1
    print(fig,'-dpng',figres,sprintf('%s/%s_raw_overview.png',fdir,settings.runID));
  end
  figstr{7}=sprintf('<img width="%s" src=%s/%s_raw_overview.png><br><b>Figure 7: Correction of raw measurements.</b> In sequence, the panels show the correction of the raw isotope measurements for the dependency of the spectroscopy of &delta;<sup>2</sup>H, &delta;<sup>18</sup>O and &delta;<sup>17</sup>O on water concentration in the cavity for each injection <span style="color:#0000ff">(blue dots)</span>, and for the empirical memory correction (black dots). The bottom panel shows the deviation of the water concentration from the reference value at 20000ppmv raw water concentration of the used instrument. %s<p>',figwid,hfdir,settings.runID,config.waterCorrection);
end % if rawfig

if injfig==1
  % plot overview figure with instrument and spectral fitting parameters
  %------------------------------------------------------------------------
  fig = 10;
  if settings.includeE17==1
    maxx=3;
  else
    maxx=2;
  end
  dimfigure(fig,10*maxx,10);
  set(fig,'Visible','Off');
  clf
  cnt=0;
  for i = [7, 4, 9, 11, 10, 12]
    cnt=cnt+1;
    h(fig*10+cnt+6) = subplot(2,3,cnt);
    plot(data.(iso{i}),'k.');
    hold on
    md=mean(data.(iso{i}),'omitnan');
    sd=std(data.(iso{i}),'omitnan');
    plot([0 length(data.date)],[md md],'r-');
    plot([0 length(data.date)],[md+sd md+sd],'r--');
    plot([0 length(data.date)],[md-sd md-sd],'r--');
    ylabel(labels{i},'FontSize',fs);
    xlabel('injection number','FontSize',fs);
    set(gca,'box','on','ygrid','on','xgrid','on','FontSize',fs,'TickDir','out');
    set(gca,'XLim',[0 length(data.Line)]);
  end
  if settings.writeReport==1
    print(fig,'-dpng',figres,sprintf('%s/%s_injections.png',fdir,settings.runID));
  end
  figstr{8}=sprintf('<img width="%s" src=%s/%s_injections.png><br><b>Figure 8: Instrument and fitting parameters during the run.</b> Panels show for each injection the data acquisition system (DAS) temperature T<sub>DAS</sub>, the water concentration, the slope shift, the CH<sub>4</sub> concentration,the fitting residuals, and the and H<sub>2</sub>O slope. <span style="color:#ff0000">Red solid and dashed line</span> indicate the mean and standard deviation for each parameter during the run.<p>',figwid,hfdir,settings.runID);
end % if injfig

if settings.includeE17==1
  maxx=3;
else
  maxx=2;
end

tmin=min([meas(:).date]);
tmax=max([meas(:).date]);
mtim=[meas(:).date]';
mTim=[ones(length(mtim),1) mtim];

% compute average linear drift of standards
for i=1:maxx
  xs = [];
  Xs = [];
  ys = [];
  tstdav=0;
  for s=1:standards
    % if at least measured two times
    if length(meas(stdrep{s}))>1
      savg(s).(iso{i})=mean([meas(stdrep{s}).(iso{i})],'omitnan'); % save average
      savg(s).(isoSD{i})=std([meas(stdrep{s}).(iso{i})],'omitnan'); % save standard deviation
      % calculate average linear slope of all standards
      x=[meas(stdrep{s}).date]';
      y=[meas(stdrep{s}).(iso{i})]'-meas(stdrep{s}(1)).(iso{i}); % first relative to first measurement
      X=[ones(length(x),1) x];
      xs=[xs; x];
      ys=[ys; y];
      Xs=[Xs; X];
      % make the drift become zero at average injection time of the used calibration standards
      %tstdav=mean(Xs);
    end
  end
  for s=1:length(settings.standardsDrift)
    % if at least measured two times
    if length(meas(stddri{s}))>1
      davg(s).(iso{i})=mean([meas(stddri{s}).(iso{i})],'omitnan'); % save average
      davg(s).(isoSD{i})=std([meas(stddri{s}).(iso{i})],'omitnan'); % save standard deviation
      % calculate average linear slope of all standards
      x=[meas(stddri{s}).date]';
      y=[meas(stddri{s}).(iso{i})]'-meas(stddri{s}(1)).(iso{i}); % first relative to first measurement
      X=[ones(length(x),1) x];
      xs=[xs; x];
      ys=[ys; y];
      Xs=[Xs; X];
    end
  end

  % make relative to start time 
  %xs=xs-min(xs);
  %Xs(:,2)=Xs(:,2)-min(Xs(:,2));
  
  % make the drift become zero at the middle time of all 
  % standard measurements useful for drift correction
  tstdav=[1 mean([min(xs),max(xs)])];

  % compute linear regression and correlation coefficient
  slp{1,i}=Xs\ys;
  ut=corrcoef(xs,ys);
  dcor(i)=ut(1,2);
  
  if tstdav>0
    % subtract offset at center time of run, if available
    drift_offset(i)=(slp{1,i}(2)*tstdav(2)+slp{1,i}(1));
    slp{1,i}(1)=slp{1,i}(1)-drift_offset(i);
  else
    % no drift correction possible
    slp{1,i}=[0;0];
    drift_offset(i)=0;
  end

  % compute RMSE of residuals
  ipstd.(iso{i})=Xs*slp{1,i};
  drmse(i)=sqrt(mean((ipstd.(iso{i})-ys).^2));
  
  % skip drift correction, if requested
  if settings.useDriftCorrection==0
    slp{1,i}=[0;0];
    drift_offset(i)=0;
  end
end

if resfig==1
  
  % calibration standard overview
  %------------------------------------------------------------------------
  
  fig = 10;
  dimfigure(fig,10*maxx,10);
  set(fig,'Visible','Off');
  clf
  
  for i=1:maxx
    
    % recreate the data once more
    xs = [];
    Xs = [];
    ys = [];
    for s=1:standards
      % if at least measured two times
      if length(meas(stdrep{s}))>1
        savg(s).(iso{i})=mean([meas(stdrep{s}).(iso{i})],'omitnan'); % save average
        savg(s).(isoSD{i})=std([meas(stdrep{s}).(iso{i})],'omitnan'); % save standard deviation
        % calculate average linear slope of all standards
        x=[meas(stdrep{s}).date]';
        y=[meas(stdrep{s}).(iso{i})]'-meas(stdrep{s}(1)).(iso{i});
        X=[ones(length(x),1) x];
        xs=[xs; x];
        ys=[ys; y];
        Xs=[Xs; X];
      end
    end
    for s=1:length(settings.standardsDrift)
      % if at least measured two times
      if length(meas(stddri{s}))>1
        davg(s).(iso{i})=mean([meas(stddri{s}).(iso{i})],'omitnan'); % save average
        davg(s).(isoSD{i})=std([meas(stddri{s}).(iso{i})],'omitnan'); % save standard deviation
        % calculate average linear slope of all standards
        x=[meas(stddri{s}).date]';
        y=[meas(stddri{s}).(iso{i})]'-meas(stddri{s}(1)).(iso{i});
        X=[ones(length(x),1) x];
        xs=[xs; x];
        ys=[ys; y];
        Xs=[Xs; X];
      end
    end
    % now plot
    subplot(2,maxx,i);
    %plot(xs,ys,'k.','MarkerSize',8);
    hold on
    stem(xs,ys-drift_offset(i),'filled','-','color',[0.0 0.0 0.0],'LineWidth',1.0,'MarkerSize',3);
    Ys=Xs*slp{1,i};
    plot(xs,Ys,'b-');
    ylabel(labels(i),'FontSize',fs);
    datetick('x','dd/mm');
    set(gca,'XLim',[tmin-2/24 tmax+2/24]);
    title('Drift and calibration standards','FontSize',fs2);
    set(gca,'box','on','ygrid','on','xgrid','on','FontSize',fs,'TickDir','out');
    subplot(2,maxx,i+maxx);
    plot([tmin tmax],[0 0],'k-');
    hold on
    %plot(xs,Ys-ys,'bx');
    stem(xs,ys-drift_offset(i)-Ys,'filled','-','color',[0.6 0.0 0.0],'LineWidth',1.0,'MarkerSize',3);
    ylabel(labels(i),'FontSize',fs);
    datetick('x','dd/mm');
    set(gca,'XLim',[tmin-2/24 tmax+2/24]);
    title('Residuals','FontSize',fs2);
    set(gca,'box','on','ygrid','on','xgrid','on','FontSize',fs,'TickDir','out');
  end
  
  % plot the correction and residuals for each species
  figstr{4}=sprintf('<img width="%s" src=%s/%s_driftcorr.png><br><b>Figure 4: Drift correction during run:</b> Estimated drift of the raw measurements of the drift and calibration standards during the run (top row) and the <span style="color:#990000">corresponding residuals</span> (bottom row). <span style="color:#0000ff">Blue solid line</span> shows a linear fit, used for linear drift correction.<p>',figwid17,hfdir,settings.runID);
  if settings.writeReport==1
    print(fig,'-dpng',figres,sprintf('%s/%s_driftcorr.png',fdir,settings.runID));
  end
end % if resfig

% apply drift to all samples
for m = 1:length(meas)
  for i=1:maxx
    meas(m).(iso{i})=meas(m).(iso{i})-(mTim(m,1)*slp{1,i}(1)+mTim(m,2)*slp{1,i}(2));
    %dcor(m)=-(mTim(m,1)*slp{1,i}(1)+mTim(m,2)*slp{1,i}(2));
  end
end

% update standards after drift correction
for s=1:standards
  for i=1:maxx
    savg(s).(iso{i})=mean([meas(stdrep{s}).(iso{i})],'omitnan'); % average
    savg(s).(isoSD{i})=std([meas(stdrep{s}).(iso{i})],'omitnan'); % standard deviation
  end
end

% also determine internal reproducibility
repint(1:3)=0;
repintcnt(1:3)=0;
for i=1:maxx
  for s=1:length(settings.standardsDrift)
    davg(s).(iso{i})=mean([meas(stddri{s}).(iso{i})],'omitnan'); % average
    davg(s).(isoSD{i})=std([meas(stddri{s}).(iso{i})],'omitnan'); % standard deviation
    if length([meas(stddri{s}).(iso{i})])>1
      repint(i)=repint(i)+davg(s).(isoSD{i});
      repintcnt(i)=repintcnt(i)+1;
    end
  end
  if repintcnt(i)>0
    repint(i)=repint(i)/repintcnt(i);
  else
    repint(i)=NaN;
  end
end

if stdfig==1
  % calibration standard overview
  %------------------------------------------------------------------------
  
  if preview==1
    dimfigure(112,10*maxx,5*standards,'Calibration standard preview');
  else
    fig = 10;
    dimfigure(fig,10*maxx,5*standards);
    set(fig,'Visible','Off');
  end
  
  for s=1:standards
    for i=1:maxx
      subplot(standards,maxx,maxx*(s-1)+i);
      hold on;
      errorbar([meas(stdrep{s}).date],[meas(stdrep{s}).(iso{i})],[meas(stdrep{s}).(isoSD{i})],'kx');
      x=[meas(stdrep{s}).date]';
      X=[ones(length(x),1) x];
      % removed here, in drift plot
      %yCalc=X*slp{1,i};
      %plot(x,yCalc+savg(s).(iso{i}),'b-');
      % plot nominal uncertainty of standards
      %plot([tmin tmax],[certs(s).(iso{i}) certs(s).(iso{i})],'g-');
      errorbar(tmax-0.25,savg(s).(iso{i}),certs(s).(isoSD{i}),certs(s).(isoSD{i}),'-','color',[0.7 0.2 0.0],'Linewidth',2);
      %plot([tmin tmax],[savg(s).(iso{i})-certs(s).(isoSD{i}) savg(s).(iso{i})-certs(s).(isoSD{i})],'g--');
      % plot interpolated standard at sample time
      %plot(mtim,mTim*slp{1,i}+savg(s).(iso{i}),'k.');
      plot([tmin tmax],[savg(s).(iso{i}) savg(s).(iso{i})],'-','color',[0.0 0.8 0.0]);
      plot([tmin tmax],[savg(s).(iso{i})+savg(s).(isoSD{i}) savg(s).(iso{i})+savg(s).(isoSD{i})],'--','color',[0.0 0.8 0.0]);
      plot([tmin tmax],[savg(s).(iso{i})-savg(s).(isoSD{i}) savg(s).(iso{i})-savg(s).(isoSD{i})],'--','color',[0.0 0.8 0.0]);
      ylabel(labels(i),'FontSize',fs);
      datetick('x','dd/mm');
      set(gca,'XLim',[tmin-2/24 tmax+2/24]);
      title(settings.standardNames{s},'FontSize',fs2);
      set(gca,'box','on','ygrid','on','xgrid','on','FontSize',fs,'TickDir','out');
    end
  end
  if settings.writeReport==1
    print(fig,'-dpng',figres,sprintf('%s/%s_cal.png',fdir,settings.runID));
  end
  if settings.calibrationInterpolation==1 || settings.calibrationInterpolation==3
    % for no linear interpolation
    figstr{3}=sprintf('<img width="%s" src=%s/%s_cal.png><br><b>Figure 5: Calibration standard variability during run:</b> Average and drift of the raw measurements of the calibration standards during the run for &delta;<sup>18</sup>O (top row) and &delta;<sup>2</sup>H (bottom row) for each of the %d calibration standards. <span style="color:#00f000">Green solid and dashed lines</span> show the mean and the standard deviation of the standard measurements which has been used for calculating uncertainty from calibration for the entire run. <span style="color:#aa2000">Brown error bar</span> shows the assigned uncertainty of the calibration standard.<p>',figwid17,hfdir,settings.runID,settings.calibrationPoints);
  else
    % for linear interpolation
    figstr{3}=sprintf('<img width="%s" src=%s/%s_cal.png><br><b>Figure 5: Calibration standard variability during run:</b> Average and drift of the raw measurements of the calibration standards during the run for &delta;<sup>18</sup>O (top row) and &delta;<sup>2</sup>H (bottom row) for each of the %d calibration standards. <span style="color:#00f000">Green solid and dashed lines</span> show the mean and the standard deviation of the standard measurements, which has been used for calculating uncertainty from calibration for the entire run. <span style="color:#aa2000">Brown error bar</span> shows the assigned uncertainty of the calibration standard.<p>',figwid17,hfdir,settings.runID,settings.calibrationPoints);
  end
end % if stdfig

% assign the measured values of standards to all vials
% - interpolate delta values corresponding to the linear instrument drift
% to obtain magnitude
for i=1:maxx
  ipstd.date=mtim;
  ipstd.(iso{i})=mTim*slp{1,i};
  dmag(i)=max(abs(ipstd.(iso{i})));
end

%  for s = 1:length(settings.standardNames)
%    cp = length(stdrep{s});     % number of calibration points
%    if (cp==1),
%      fprintf(fid,'No interpolation possible for standard %s, only 1 vial<br>',settings.standardNames{s});
%    end
%    for i = 1:3
%      M = [meas(stdrep{s}).(iso{i})]; % make matrix
%      M_SD = [meas(stdrep{s}).(isoSD{i})];
%savg(s).(iso{i})=mean([meas(stdrep{s}).(iso{i})]); % save average
%savg(s).(isoSD{i})=mean([meas(stdrep{s}).(isoSD{i})]); % save mean standard deviation
%for p = 1:cp-1
%	slope.(iso{i})(s,p) = (M(p+1)-M(p))/(stdrep{s}(p+1)-stdrep{s}(p)); % slope between every two calibration points in permil per inj
%	slope.(isoSD{i})(s,p) = sqrt((M_SD(p+1)^2+M_SD(p)^2)/(stdrep{s}(p+1)-stdrep{s}(p))^2); % HS: what is this?
%	for v = stdrep{s}(p):stdrep{s}(p+1)
%	  ipstd(v,s).(iso{i}) = meas(stdrep{s}(p)).(iso{i}) + slope.(iso{i})(s,p)*(v-stdrep{s}(p)); % interpolated measured values of standards
%	  ipstd(v,s).(isoSD{i}) = sqrt(meas(stdrep{s}(p)).(isoSD{i})^2 + (slope.(isoSD{i})(s,p)*(v-stdrep{s}(p)))^2);
%	end
%      end
%    end
%  end
% make an interpolation check plot
% interpolation curves and points vs. mean and mean standard deviation for each standard
%  fig = fig+1;
%  dimfigure(fig,15,15);
%  cnt=0;
%  for i=1:2,
%    for s = 1:length(settings.standardNames),
%      cnt=cnt+1;
%      if (length(stdrep{i})>1),
%  	subplot(2,length(settings.standardNames),cnt)
%  	plot(stdrep{i}(1):stdrep{i}(end),[ipstd(:,s).(iso{i})],'-o')
%  	hold on
%  	plot(stdrep{i},[meas(stdrep{s}).(iso{i})],'r*')
%  	plot([1 vials],[savg(s).(iso{i}) savg(s).(iso{i})],'k-');
%  	plot([1 vials],[savg(s).(iso{i})+savg(s).(isoSD{i}) savg(s).(iso{i})+savg(s).(isoSD{i})],'k--');
%  	plot([1 vials],[savg(s).(iso{i})-savg(s).(isoSD{i}) savg(s).(iso{i})-savg(s).(isoSD{i})],'k--');
%  	set(gca,'TickDir','out','XGrid','On','YGrid','On','TickDir','out','FontSize',fs);
%  	xlabel('Vial','FontSize',fs);
%  	ylabel(sprintf('%s / permil',iso{i}),'FontSize',fs);
%        end
%      end
%    end
%    print(fig,'-dpng',figres,sprintf('%s/%s_interpolation.png',fdir,settings.runID));
%    figstr{fig}=sprintf('<h4>Figure 4: Time interpolation of standards</h4><br><img width="%s" src=%s/%s_interpolation.png><p>',figwid,hfdir,settings.runID);

% - calibrate each sample according to the assigned values of standards
% e.g. cert(1,2,3) --> {'GSM1','VATS','DI'};

% for two-point calibration: fixed indices

% chose standards used for calibration from all standard vials
stdn = find(strcmp(settings.standardNames,settings.standardNames{1}));
stdm = find(strcmp(settings.standardNames,settings.standardNames{2}));
% n=LS1=enriched
% m=LS2=depleted
% put the enriched and depleted one in the right order
if certs(stdn).d18O<certs(stdm).d18O
  dummy=stdn;
  stdn=stdm;
  stdm=dummy;
end

% for three-point calibration: indices depend on vial
% define two sets, starting from the intermediate standard
[~, stdmin]=min([certs.d18O]);
[~, stdmax]=max([certs.d18O]);
stdmid=setdiff([1 2 3],[stdmin stdmax]);
if settings.calibrationPoints==3
  logf(handles,'Identified standard ranges as %s-%s and %s-%s',certs(stdmin).name1,certs(stdmid).name1,certs(stdmid).name1,certs(stdmax).name1);
end

stdv = find(ismember(({meas.name1}),(settings.standardNames)));  % index for standards vials
logf(handles,'Calibration standards at vials %s',int2str(stdv));
driv = find(ismember(({meas.name1}),(settings.standardsDrift)));  % index for drift vials
logf(handles,'Drift standards at vials %s',int2str(driv));
smpv = find(~ismember(({meas.name1}),(settings.standardNames)) & ~ismember(({meas.name1}),(settings.standardsDrift)));  % index for sample vials
logf(handles,'Samples at vials %s',int2str(smpv));

% ignore value of calibrationInterpolation and calibrationPoints
settings.calibrationInterpolation=1;
settings.calibrationPoints=2;

if settings.calibrationInterpolation==2 % use time interpolation of standard drift
  for i = 1:maxx
    for j = 1:length(meas) % assign calibrated delta values to samples
      % for 3-point calibration, need to find out standards for each sample
      if settings.calibrationPoints==3
        % use mid point to divide into two branches
        if meas(j).(iso{i})<certs(stdmid).(iso{i})
          stdn = stdmin;
          stdm = stdmid;
        else
          stdn = stdmid;
          stdm = stdmax;
        end
      end
      cal(j).name1 = meas(j).name1;
      cal(j).name2 = meas(j).name2;
      %cal(j).(iso{i}) = (certs(stdm).(iso{i})-certs(stdn).(iso{i}))/(ipstd(stdm).(iso{i})(j)-ipstd(stdn).(iso{i})(j))*(meas(j).(iso{i})-ipstd(stdn).(iso{i})(j)) + certs(stdn).(iso{i});
      cal(j).(iso{i}) = (certs(stdm).(iso{i})-certs(stdn).(iso{i}))/(savg(stdm).(iso{i})-savg(stdn).(iso{i}))*(meas(j).(iso{i})-savg(stdn).(iso{i})) + certs(stdn).(iso{i});
      cal(j).(isoSD{i}) = meas(j).(isoSD{i});
      cal(j).date = meas(j).date;
    end
  end
else % use mean of all respective standard measurements
  for i = 1:maxx
    % calculate normalisation factor f for this isotope species
    f = (certs(stdm).(iso{i})-certs(stdn).(iso{i}))/(savg(stdm).(iso{i})-savg(stdn).(iso{i}));
    for j = 1:length(meas) % assign calibrated delta values to samples
      % for 3-point calibration, need to find out standards for each sample
      if settings.calibrationPoints==3
        % use mid point to divide into two branches
        if meas(j).(iso{i})<certs(stdmid).(iso{i})
          stdn = stdmin;
          stdm = stdmid;
        else
          stdn = stdmid;
          stdm = stdmax;
        end
      end
      cal(j).name1 = meas(j).name1;
      cal(j).name2 = meas(j).name2;
      cal(j).(iso{i}) =  certs(stdn).(iso{i}) + (meas(j).(iso{i})-savg(stdn).(iso{i}))*f;
      cal(j).(isoSD{i}) = meas(j).(isoSD{i});
      cal(j).date = meas(j).date;
    end
  end
end

% calculate standard uncertainty
for j = 1:length(meas)
  for i = 1:maxx
    % uncertainty components a, b, c, d, e
    % index m: LS2, index n: LS1
    % a: sh*u(h), sensitivity to measured values
    sens=1-(meas(j).(iso{i})-savg(stdn).(iso{i}))/(savg(stdm).(iso{i})-savg(stdn).(iso{i}));
    u(j).a(i)=sens*certs(stdn).(isoSD{i});
    % b: sl*u(l), sensitivity to measured values
    sens=(meas(j).(iso{i})-savg(stdn).(iso{i}))/(savg(stdm).(iso{i})-savg(stdn).(iso{i}));
    u(j).b(i)=sens*certs(stdm).(isoSD{i});
    % find SEM for standard measurements (u(H), u(L)) using reproducibility and repeatability
    % depleted, u(L), LS2
    stdmsea=std(cat(1,vial(stdrep{1}).(iso{i})),'omitnan')/sqrt(length(cat(1,vial(stdrep{1}).(iso{i}))));
    stdmseb=savg(stdm).(isoSD{i})/sqrt(length(stdrep{1}));
    stdmse=sqrt(stdmsea^2+stdmseb^2);
    % enriched, u(H), LS1
    stdnsea=std(cat(1,vial(stdrep{2}).(iso{i})),'omitnan')/sqrt(length(cat(1,vial(stdrep{2}).(iso{i}))));
    stdnseb=savg(stdn).(isoSD{i})/sqrt(length(stdrep{2}));
    stdnse=sqrt(stdnsea^2+stdnseb^2);
    % c: sH*u(H)
    sens=(certs(stdm).(iso{i})-certs(stdn).(iso{i}))*(meas(j).(iso{i})-savg(stdn).(iso{i}))/((savg(stdm).(iso{i})-savg(stdn).(iso{i}))^2)-(certs(stdm).(iso{i})-certs(stdn).(iso{i}))/(savg(stdm).(iso{i})-savg(stdn).(iso{i}));
    u(j).c(i)=sens*stdnse;
    % d: sL*u(L)
    sens=(-(certs(stdm).(iso{i})-certs(stdn).(iso{i}))*(meas(j).(iso{i})-savg(stdn).(iso{i}))/((savg(stdm).(iso{i})-savg(stdn).(iso{i}))^2));
    u(j).d(i)=sens*stdmse;
    % e: sS*u(S)
    sens=(certs(stdm).(iso{i})-certs(stdn).(iso{i}))/(savg(stdm).(iso{i})-savg(stdn).(iso{i}));
    if isnan(LTP(i))
      u(j).e(i)=sens*std([cal(driv).(isoSD{i})]); % estimate from standard dev. over last injections if no LTP available
    else
      u(j).e(i)=sens*LTP(i); % from long-term measurements of drift standards
    end
    % obtain combined uncertainty for this sample
    cal(j).(isoSD{i})=sqrt(u(j).a(i)^2+u(j).b(i)^2+u(j).c(i)^2+u(j).d(i)^2+u(j).e(i)^2);
    u(j).t(i)=cal(j).(isoSD{i});
    % compute the contributions to the budget
    u(j).a(i)=u(j).a(i)^2/u(j).t(i)^2*100.0;
    u(j).b(i)=u(j).b(i)^2/u(j).t(i)^2*100.0;
    u(j).c(i)=u(j).c(i)^2/u(j).t(i)^2*100.0;
    u(j).d(i)=u(j).d(i)^2/u(j).t(i)^2*100.0;
    u(j).e(i)=u(j).e(i)^2/u(j).t(i)^2*100.0;
  end
end
% calculate d-excess with SD
for j = 1:length(meas)
  cal(j).(iso{5}) = cal(j).(iso{2}) - 8*cal(j).(iso{1});
  cal(j).(isoSD{5}) = sqrt(cal(j).(isoSD{2})^2 + 8^2*cal(j).(isoSD{1})^2);
end
if settings.includeE17==1
  % calculate 17-O excess
  for j = 1:length(meas)
    cal(j).(iso{6}) = (log(cal(j).(iso{3})./1000+1) - 0.528.*log(cal(j).(iso{1})./1000+1)).*1e6;
    cal(j).(isoSD{6}) = sqrt((cal(j).(isoSD{3})./1000./(cal(j).(iso{3})./1000+1)).^2 + (-0.528.*cal(j).(isoSD{1})./1000./(cal(j).(iso{1})./1000+1)).^2).*1e6;
    cal(j).(isoSD{6}) = cal(j).(isoSD{6})/sqrt(vial(j).ends);
    %data.d17O_excess = (log(data.d17O./1000+1) - 0.528.*log(data.d18O./1000+1)).*1e6;
    %data.d17O_excess_SD = sqrt((data.d17O_SD./1000./(data.d17O./1000+1)).^2 + (-0.528.*data.d18O_SD./1000./(data.d18O./1000+1)).^2).*1e6;
  end
end

% detect vials for drift standards
for s = 1:length(settings.standardsDrift)
  dstdrep{s}  = find(strcmpi({meas.name1},settings.standardsDrift{s}));
  cdstdrep{s} = find(strcmpi({cal.name1},settings.standardsDrift{s}));
end

if driftfig==1
  % calibrated drift standard overview
  %------------------------------------------------------------------------
  if preview==1
    dimfigure(111,20,5*maxx,'Drift standard preview');
  else
    fig = 10;
    dimfigure(fig,20,5*maxx);
    set(fig,'Visible','Off');
  end
  clf
  tmin=min([meas(:).date]);
  tmax=max([meas(:).date]);
  for s = 1:length(settings.standardsDrift)
    for i=1:3
      k = find(strcmp(calcor.names,settings.standardsDrift{s}));
      if isempty(k)
        logf(handles,'ERROR: Name of drift standard %s unknown.\n       Standard names must be one of this list:      \n',settings.standardsDrift{s});
        logf(handles,'%s ',calcor.names{:});
        return
      end
      dnom(s).(iso{i})=certall(k).(iso{i});
      dnom(s).(isoSD{i})=certall(k).(isoSD{i});
    end
    % [meas(stddri{s}).(iso{i})]
    for i = 1:maxx
      % detect vials for standards
      h(fig*10+(s-1)*maxx+i) = subplot(maxx,length(settings.standardsDrift),(s-1)*maxx+i);  % d18O
      hold on;
      %errorbar([meas(dstdrep{s}).date],[meas(dstdrep{s}).(iso{1})],[meas(dstdrep{s}).(isoSD{1})],'bx');
      %errorbar([cal(cdstdrep{s}).date],[cal(cdstdrep{s}).(iso{i})],[cal(cdstdrep{s}).(isoSD{i})],'kx');
      errorbar([cal(stddri{s}).date],[cal(stddri{s}).(iso{i})],[cal(stddri{s}).(isoSD{i})],'kx');
      % calculate linear slope of standard
      x=[cal(stddri{s}).date]';
      y=[cal(stddri{s}).(iso{i})]';
      X=[ones(length(x),1) x];
      cdslp{s,i}=X\y;
      yCalc=X*cdslp{s,i};
      %plot([meas(dstdrep{s}).date],[meas(dstdrep{s}).(iso{1})],'bx-');
      plot([tmin tmax],[dnom(s).(iso{i}) dnom(s).(iso{i})],'-','color',[0.7 0.2 0.0]);
      plot([tmin tmax],[dnom(s).(iso{i})+LTP(i) dnom(s).(iso{i})+LTP(i)],'-','color',[0.0 0.8 0.0]);
      plot([tmin tmax],[dnom(s).(iso{i})-LTP(i) dnom(s).(iso{i})-LTP(i)],'-','color',[0.0 0.8 0.0]);
      plot([tmin tmax],[dnom(s).(iso{i})+2*LTP(i) dnom(s).(iso{i})+2*LTP(i)],'--','color',[0.0 0.8 0.0]);
      plot([tmin tmax],[dnom(s).(iso{i})-2*LTP(i) dnom(s).(iso{i})-2*LTP(i)],'--','color',[0.0 0.8 0.0]);
      plot([tmin tmax],[dnom(s).(iso{i})+3*LTP(i) dnom(s).(iso{i})+3*LTP(i)],':','color',[0.0 0.8 0.0],'LineWidth',2);
      plot([tmin tmax],[dnom(s).(iso{i})-3*LTP(i) dnom(s).(iso{i})-3*LTP(i)],':','color',[0.0 0.8 0.0],'LineWidth',2);
      plot(x,yCalc,'b--');
      driftBias(i)=mean(y)-dnom(s).(iso{i});
      ylabel(labels(i),'FontSize',fs);
      datetick('x','dd/mm');
      set(gca,'XLim',[tmin-2/24 tmax+2/24]);
      title(settings.standardsDrift{s},'FontSize',fs2);
      set(gca,'box','on','ygrid','on','xgrid','on','FontSize',fs,'TickDir','out');
    end
  end
  if settings.writeReport==1
    print(fig,'-dpng',figres,sprintf('%s/%s_drift.png',fdir,settings.runID));
  end
  if settings.includeE17==1
    figstr{6}=sprintf('<img width="%s" src=%s/%s_drift.png><br><b>Figure 6: Drift standard during run:</b> Drift of the calibrated measurements of the drift standard during the run for &delta;<sup>18</sup>O (top row), &delta;<sup>2</sup>H (middle row), and &delta;<sup>17</sup>O (bottom row) for each of the %d drift standards. <span style="color:#aa2000">Brown solid line</span> shows the true value of the standard and in <span style="color:#00aa00">green</span> its 1x standard deviation (solid), 2x standard deviation (dashed), and 3x standard deviation (dotted, warning boundary). <span style="color:#0000ff">Blue solid line</span> shows a linear interpolation to the corrected, calibrated drift standard measurements.<p>',figwid,hfdir,settings.runID,length(settings.standardsDrift));
  else
    figstr{6}=sprintf('<img width="%s" src=%s/%s_drift.png><br><b>Figure 6: Drift standard during run:</b> Drift of the calibrated measurements of the drift standard during the run for &delta;<sup>18</sup>O (top row) and &delta;<sup>2</sup>H (bottom row) for each of the %d drift standards. <span style="color:#aa2000">Brown solid lines</span> shows the true value of the standard and in <span style="color:#00aa00">green</span> its standard deviation, its 1x standard deviation (solid), 2x standard deviation (dashed), and 3x standard deviation (dotted, warning boundary). <span style="color:#0000ff">Blue solid line</span> shows a linear interpolation to the corrected, calibrated drift standard measurements.<p>',figwid,hfdir,settings.runID,length(settings.standardsDrift));
  end
end % if driftfig

% create output data
str = fieldnames(cal);
for s = 2:length(str)
  cout.(str{s}) = [cal(smpv).(str{s})]';
end
cout.name1={cal(smpv).name1}';
cout.name2={cal(smpv).name2}';

% if desired, also output raw measurement data
mout=meas;

if sumfig==1
  % create overview figure of raw and calibrated data
  %------------------------------------------------------------------------
  fig = 10;
  dimfigure(fig,20,10);
  set(fig,'Visible','Off');
  clf
  for i = 1:2
    h(fig*10+i) = subplot(2,2,i);  % d18O, dD
    hold on;
    errorbar([meas(smpv).(iso{i})],[meas(smpv).(isoSD{i})],'.r');
    errorbar([cal(smpv).(iso{i})],[cal(smpv).(isoSD{i})],'ok','MarkerSize',5);
    set(gca,'XLim',[0 length(smpv)+1]);
    ylabel(labels{i},'FontSize',fs);
    xlabel('Sample number');
    set(gca,'box','on','ygrid','on','xgrid','on','FontSize',fs,'TickDir','out');
    %axis tight;
    
    if (i==2 && settings.includeE17~=1)
      axis off
    else
      h(fig*10+i+2) = subplot(2,2,i+2);  % dxs, e17
      hold on;
      if i==1
        errorbar([meas(smpv).(iso{i+4})],[meas(smpv).(isoSD{i+4})],'.r');
      end
      errorbar([cal(smpv).(iso{i+4})],[cal(smpv).(isoSD{i+4})],'ok','MarkerSize',5);
      set(gca,'XLim',[0 length(smpv)+1]);
      ylabel(labels{i+4},'FontSize',fs);
      xlabel('Sample number');
      set(gca,'box','on','ygrid','on','xgrid','on','FontSize',fs,'TickDir','out');
      %axis tight;
    end
  end
  subplot(2,2,2);
  axis on;
  legend(h(fig*10+2),'raw','calibrated','location','best');
  legend(h(fig*10+2),'boxoff');
  print(fig,'-dpng',figres,sprintf('%s/%s_calibrated.png',fdir,settings.runID));
  figstr{1}=sprintf('<img width="%s" src=%s/%s_calibrated.png><br><b>Figure 1: Calibrated data for all samples.</b> Calibrated measurements using the %d-point calibration for all of the %d samples. Black data points denote calibrated values and their standard deviation for &delta;<sup>18</sup>O, &delta;<sup>2</sup>H, d-excess (permil) and e17(permeg) vs. sample number.<p>',figwid,hfdir,settings.runID,settings.calibrationPoints,length(smpv));
end % if sumfig

if calsumfig==1
  % create calibration overview figure
  %------------------------------------------------------------------------
  
  fig = 10;
  dimfigure(fig,10*maxx,10);
  set(fig,'Visible','Off');
  
  clf
  for i=1:maxx
    subplot(1,maxx,i);
    hold on
    % plot 1:1 line
    plot([min([meas(:).(iso{i})]) max([meas(:).(iso{i})])],[min([meas(:).(iso{i})]) max([meas(:).(iso{i})])],'--','Color',[0.7 0.7 0.7]);
    % plot standards
    if settings.calibrationPoints==2
      plot([meas(stdv(stdn)).(iso{i}),meas(stdv(stdm)).(iso{i})],[cal(stdv(stdn)).(iso{i}),cal(stdv(stdm)).(iso{i})],'-','Color',[0.6 0.6 0.6]);
    else
      plot([meas(stdv(stdmin)).(iso{i}),meas(stdv(stdmid)).(iso{i})],[cal(stdv(stdmin)).(iso{i}),cal(stdv(stdmid)).(iso{i})],'-','Color',[0.6 0.6 0.6]);
      plot([meas(stdv(stdmid)).(iso{i}),meas(stdv(stdmax)).(iso{i})],[cal(stdv(stdmid)).(iso{i}),cal(stdv(stdmax)).(iso{i})],'-','Color',[0.6 0.6 0.6]);
    end
    errorbar([meas(stdv).(iso{i})],[cal(stdv).(iso{i})],[cal(stdv).(isoSD{i})],'xr','MarkerSize',8);
    errorbar([meas(stdv).(iso{i})],[cal(stdv).(iso{i})],[cal(stdv).(isoSD{i})],'or','MarkerSize',8);
    % plot drift standards
    errorbar([meas(driv).(iso{i})],[cal(driv).(iso{i})],[cal(driv).(isoSD{i})],'xb','MarkerSize',8);
    errorbar([meas(driv).(iso{i})],[cal(driv).(iso{i})],[cal(driv).(isoSD{i})],'ob','MarkerSize',8);
    % plot samples
    errorbar([meas(smpv).(iso{i})],[cal(smpv).(iso{i})],[cal(smpv).(isoSD{i})],'.k','MarkerSize',10);
    text([meas(smpv).(iso{i})],[cal(smpv).(iso{i})],{meas(smpv).name1},'FontSize',fs);
    % settings
    ylabel([labels{i},' calibrated'],'FontSize',fs);
    xlabel([labels{i},' raw'],'FontSize',fs);
    set(gca,'box','on','ygrid','on','xgrid','on','FontSize',fs,'TickDir','out');
    axis square
  end
  print(fig,'-dpng',figres,sprintf('%s/%s_calibration_overview.png',fdir,settings.runID));
  figstr{2}=sprintf('<img width="%s" src=%s/%s_calibration_overview.png><br><b>Figure 2: Calibrated vs. raw data for standards and samples.</b> Calibrated measurements using the %d-point calibration for all of the %d samples are plotted in relation to the standards. <span style="color:#ff0000">Red dots</span> denote nominal standards vs. raw values, <span style="color:#0000ff">blue dots</span> denote drift standards, and black dots with labels are samples. A 1:1 relation is shown by the grey dashed line.<p>',figwid17,hfdir,settings.runID,settings.calibrationPoints,length(smpv));
end % if calsumfig

if memfig==1
  % create summary figure for memory correction
  %------------------------------------------------------------------------
  if sum(settings.mem.enableCorrection)>0
    fig = 10;
    dimfigure(fig,20,8);
    set(fig,'Visible','Off');
    clf
    
    if settings.includeE17==1
      maxk=3;
    else
      maxk=2;
    end
    
    for k = 1:maxk
      subplot(1,maxk,k);  % d18O, dD, d17O
      hold on;
      
      % find the max number of injections per sequential sample
      maxInjNr=0;
      for i=1:vials
        vial(v).maxInjNr=max(vial(v).InjNr);
        maxInjNr=max(maxInjNr,vial(v).maxInjNr);
      end
      
      % exclude vials with less than required injections
      midx=(mdata.maxInjNr>=settings.mem.minInj(k));
      % exclude manually excluded vials
      eidx=~ismember(mdata.SampleLine,settings.ignoreVialsCal);
      % include only standards as chosen
      if ~isempty(settings.mem.standards)
        str=strsplit(settings.mem.standards);
        sidx=zeros(size(midx));
        for i=1:length(str)
          sidx=sidx | strcmp(mdata.Identifier1,str{i});
        end
        midx=[midx & eidx] & sidx;
      else
        midx=[midx & eidx];
      end
      mdat=mdata.([iso{k},'_mem'])(midx);
      mweight=mdata.mstep(midx,k);
      maxInjNr=max(mdata.maxInjNr(midx));
      for i=1:maxInjNr
        saidx=mdata.InjNr(midx)==i;
        naidx=find(isnan(mdat(saidx))==0);
        mmdat=mdat(saidx);
        mmweight=mweight(saidx);
        mm(i)=sum(mmdat(naidx).*(mmweight(naidx)/sum(mmweight(naidx),'omitnan')),'omitnan');
        %mm(i)=mean(mdat(saidx),'omitnan');
        ms(i)=std(mdat(saidx),'omitnan');
      end
      errorbar(1:maxInjNr,mm,ms,'kx');
      clear h;
      h(1)=plot(mdata.InjNr(midx),mdat,'.','Color',[0.4 0.4 0.4],'MarkerSize',3);
      h(2)=plot(1:maxInjNr,mfc(1:maxInjNr,k),'r.-');
      h(3)=plot(1:maxInjNr,mfd(1:maxInjNr,k),'g.--');
      h(4)=plot(1:maxInjNr,mfe(1:maxInjNr,k),'b.--');
      legend(h,{'Injections';'Combined';'Fast memory';'Slow memory'},'Location','NorthEast');
      legend boxoff
      
      %set('XLim',[0 length(vial(idx).Line)+1]);
      set(gca,'YAxisLocation','right');
      %ylabel(ytitles{get(parameterPopup,'Value')+1});
      xlabel(gca,'Injection nr.');
      box on
      
      %errorbar([cal(smpv).(iso{i})],[cal(smpv).(isoSD{i})],'ok','MarkerSize',5);
      set(gca,'XLim',[0 maxInjNr+1]);
      ylabel(['Memory for ',iso{k},' / %'],'FontSize',fs);
      xlabel(gca,'Injection number');
      set(gca,'box','on','ygrid','on','xgrid','on','FontSize',fs,'TickDir','out');
    end
    print(fig,'-dpng',figres,sprintf('%s/%s_memory_correction.png',fdir,settings.runID));
    if settings.includeE17==1
      o17str=', &delta;<sup>2</sup>H and &delta;<sup>17</sup>O (permil)';
    else
      o17str=' and &delta;<sup>2</sup>H (permil)';
    end
    figstr{5}=sprintf('<img width="%s" src=%s/%s_memory_correction.png><br><b>Figure 3: Memory correction fitting function.</b> Fits of the empirical memory correction function for &delta;<sup>18</sup>O%s. Grey dots are data points from chosen samples, black bars are used for calculing the RMSE with respect to the fitting function f(i) given above. <span style="color:#ff0000">Red line</span> denotes the combined fit, made up of a <span style="color:#007700">fast component (green)</span> and a <span style="color:#0000ff">slow component (blue)</span>.<p>',figwid17,hfdir,settings.runID,o17str);
  end
end % if memfig

if indfig==1
  % plot an overview figure for each vial for the Appendix
  %------------------------------------------------------------------------
  qflags=zeros(vials,6); % quality flag array
  fig = 10;
  if settings.includeE17==1
    imax=4;
  else
    imax=3;
  end
  dimfigure(fig,20,4);
  set(fig,'Visible','Off');
  clf
  logf(handles,'Plotting all injections for each vial... ');
  for v = 1:vials
    excl=settings.excludeInj{v};
    fprintf('%d ',v);
    clf
    for i = 1:imax
      ax=subplot(1,imax,i);
      hold on
      if i<imax
        md=mean(vial(v).(iso{i})(end-vial(v).ends:end),'omitnan');
        sd=std(vial(v).(iso{i})(end-vial(v).ends:end),'omitnan');
        %sd=mean(vial(v).(isoSD{i})(end-vial(v).ends:end),'omitnan')/sqrt(vial(v).ends);
        plot(ax,[0 max(vial(v).InjNr)],[md md],'-','Color',[0.6 0.6 0.6]);
        plot(ax,[0 max(vial(v).InjNr)],[md+sd md+sd],'--','Color',[0.6 0.6 0.6]);
        plot(ax,[0 max(vial(v).InjNr)],[md-sd md-sd],'--','Color',[0.6 0.6 0.6]);
      end
      if i<imax
        ext='_corr';
      else
        ext='';
      end
      % plot H2O for either with or without 17O
      if i==imax && i==3
        ii=4;
      else
        ii=i;
      end
      if i<imax
        plot(ax,vial(v).InjNr,vial(v).([iso{ii},'_uc']),'kx');
      end
      errorbar(ax,vial(v).InjNr,vial(v).([iso{ii},ext]),vial(v).(isoSD{ii}),'k.');
      if i<imax
        if settings.mem.enableCorrection(i)==1
          errorbar(ax,vial(v).InjNr,vial(v).(iso{i}),vial(v).(isoSD{i}),'o','Color',[0.0 0.5 0]);
          errorbar(ax,vial(v).InjNr(end-vial(v).ends:end),vial(v).(iso{i})(end-vial(v).ends:end),vial(v).(isoSD{i})(end-vial(v).ends:end),'o','Color','b');
        else
          errorbar(ax,vial(v).InjNr(end-vial(v).ends:end),vial(v).([iso{ii},ext])(end-vial(v).ends:end),vial(v).(isoSD{ii})(end-vial(v).ends:end),'b.');
        end
        if excl>0
          errorbar(ax,vial(v).InjNr(excl),vial(v).([iso{i},'_corr'])(excl),vial(v).([isoSD{i},'_corr'])(excl),'ro');
        end
      end
      if excl>0
        errorbar(ax,vial(v).InjNr(excl),vial(v).([iso{ii},ext])(excl),vial(v).(isoSD{ii})(excl),'r.');
      end
      ylabel(ax,labels{ii},'FontSize',fs); 
      if (i==1)
        title(ax,vial(v).Identifier1(end),'FontSize',fs2);
      end
      set(ax,'box','on','ygrid','on','xgrid','on','FontSize',fs,'TickDir','out');
      xlim(ax,[0 length(vial(v).(iso{ii}))+1]);
      % chose ylim for non-excluded samples
      ee=1:length(vial(v).InjNr);
      if excl>0
        ee(excl)=[];
      end
      ylim1=[min(vial(v).(iso{ii})(ee)-mean(vial(v).(isoSD{ii})(ee),'omitnan')*1.5) max(vial(v).(iso{ii})(ee)+mean(vial(v).(isoSD{ii})(ee),'omitnan')*1.5)];
      ylim2=[min(vial(v).([iso{ii},ext])(ee)-mean(vial(v).(isoSD{ii})(ee),'omitnan')*1.5) max(vial(v).([iso{ii},ext])(ee)+mean(vial(v).(isoSD{ii})(ee),'omitnan')*1.5)];
      % fix rare problem from missing SD
      if isnan(ylim1)
        ylim1=[min(vial(v).(iso{ii})(ee)) max(vial(v).(iso{ii})(ee))];
        ylim2=[min(vial(v).([iso{ii},ext])(ee)) max(vial(v).([iso{ii},ext])(ee))];
      end
      if ylim1(1)==ylim1(2)
        ylim1(1)=ylim1(1)-0.05;
        ylim1(2)=ylim1(2)+0.05;
      end
      if ylim2(1)==ylim2(2)
        ylim2(1)=ylim2(1)-0.05;
        ylim2(2)=ylim2(2)+0.05;
      end
      ylim([min(ylim1(1),ylim2(1)),max(ylim1(2),ylim2(2))]);
    end
    % TODO: complete quality checks
    % (1) Humidity variation during injection
    if mean(vial(v).H2O_SD(end-vial(v).ends:end))>config.H2O_SD
      qflags(v,1)=1;
    end
    % (2) Large humidity variation between injections
    %if abs(meas(v).H2O-20000)>3000,
    if std(vial(v).H2O(end-vial(v).ends:end))>config.H2O_inj_SD
      qflags(v,2)=1;
    end
    % (3) Large isotopic variability
    if ((cal(v).dD_SD>config.dD_SD) || (cal(v).d18O_SD>config.d18O_SD))
      qflags(v,3)=1;
    end
    % (4) Sample outside standard range
    if (cal(v).d18O<min([certs(:).d18O]) || cal(v).dD<min([certs(:).dD]) || cal(v).d18O>max([certs(:).d18O]) || cal(v).d18O>max([certs(:).d18O])),
      if ~ismember(v,[stdrep{:}]) % if not a standard vial
        qflags(v,4)=1;
        logf(handles,'WARNING: Sample %d (%s) outside of range of calibration standards.',v,vial(v).Identifier1{1});
      end
    end
    % (5) Instrument temperature variability
    if (std(vial(v).DASTemp)>config.T_DAS_SD)
      qflags(v,5)=1;
    end
    % (6) Matrix gas N2 (0) or air (1)
    if vial(v).n2_flag~=1
      qflags(v,6)=1;
    end
    if settings.writeReport==1
      print(fig,'-dpng',figres,sprintf('%s/%s_vial_%03d.png',fdir,settings.runID,v));
    end
    figstr{v+10}=sprintf('<br><img width="%s" src=%s/%s_vial_%03d.png><br><b>Figure A%d: Overview over all injections for vial %d (%s).</b> See above for panel description.<p>',figwid,hfdir,settings.runID,v,v,v,vial(v).Identifier1{end});
  end
  logf(handles,'Plotting of vials completed');
end % if indfig

% clear the 17O if not requested
if settings.includeE17==0
  for i=1:length(cal)
    cal(i).d17O=0;
    cal(i).d17O_SD=0;
    cal(i).e17=0;
    cal(i).e17_SD=0;
  end
end

% do not write report if preview was requested
if preview==1
  return
end
close(10);

%------------------Plotting completed-----------------------------------------

% deactivate images
if (showfig==0)
  for j=1:length(figstr)
    figstr{j}=strrep(figstr{j},'src=','id=');
  end
end

% Section 1: Summary
interp={'constant';'linear'};
fprintf(fid,'<table><tr><td>');
fprintf(fid,'<h2>Liquid water isotope measurements report for project %s, run %s</h2>\n',settings.project,settings.runID);
fprintf(fid,'<h3>%s %s</h3><p>\n',config.reference,datestr(settings.reportDate));
fprintf(fid,'<h3>Table of Contents</h3><p>\n');
fprintf(fid,'<a href="#sec1">1. Summary</a><br>\n');
fprintf(fid,'<a href="#sec2">2. Calibrated measurements</a><br>\n');
fprintf(fid,'<a href="#sec3">3. Acknowledgement section for publications</a><br>\n');
fprintf(fid,'<a href="#sec4">4. Method description for publications</a><br>\n');
if settings.writeReport==1
  fprintf(fid,'<a href="#sec5">5. Memory correction</a><br>\n');
  fprintf(fid,'<a href="#sec6">6. Drift correction</a><br>\n');
  fprintf(fid,'<a href="#sec7">7. Calibration to VSMOW-SLAP scale</a><br>\n');
  fprintf(fid,'<a href="#sec8">8. Uncertainty budget</a><br>\n');
  fprintf(fid,'<a href="#sec9">9. Preprocessing and correction details</a><br>\n');
  fprintf(fid,'<a href="#sec10">10. Instrument stability</a><br>\n');
  fprintf(fid,'<a href="#sec11">Appendix: Raw data for all vials</a><p></td>\n');
end
fprintf(fid,'<td valign="top" align="right"><img src="%s/%s" width="150px"></td></td></table>\n',hfdir,config.logofile);
fprintf(fid,'<hr>\n');
fprintf(fid,'<h3 id="sec1">1. Summary</h3><p>\n');
fprintf(fid,'<table>\n');
fprintf(fid,'<tr><td align=left>Project run ID      :</td><td align=left>%s</td></tr>\n',settings.runID);
fprintf(fid,'<tr><td align=left>Project name        :</td><td align=left>%s</td></tr>\n',settings.project);
fprintf(fid,'<tr><td align=left>Project type        :</td><td align=left>%s</td></tr>\n',settings.projectType);
fprintf(fid,'<tr><td align=left>Customer name       :</td><td align=left>%s',settings.customer);
fprintf(fid,' (email: <a href="mailto:%s">%s)</a></td></tr><tr></tr>\n',settings.contact,settings.contact);
fprintf(fid,'<tr><td align=left>Instrument          :</td><td align=left>%s</td></tr>\n',settings.instrument);
fprintf(fid,'<tr><td align=left>Analysis start      :</td><td align=left>%s</td></tr>\n',datestr(data.TimeCode(1)));
fprintf(fid,'<tr><td align=left>Analysis end        :</td><td align=left>%s</td></tr>\n',datestr(data.TimeCode(end)));
fprintf(fid,'<tr><td align=left>Number of samples   :</td><td align=left>%d</td></tr>\n',length(smpv));
if ~isempty(smpv)
  fprintf(fid,'<tr><td align=left>Injections/sample   :</td><td align=left>%d</td></tr>\n',meas(smpv(1)).InjNr);
end
% make sure -1 is shown in case it has been selected
if ends==0
  ends=-2;
end
if ends1==0
  ends1=-2;
end

fprintf(fid,'<tr><td align=left>Averaged inj 1st smp:</td><td align=left>%d (-1 for all)</td></tr>\n',ends1+1);
fprintf(fid,'<tr><td align=left>Averaged injections :</td><td align=left>%d (-1 for all)</td></tr>\n',ends+1);
fprintf(fid,'<tr><td align=left>Calibration standards:</td><td align=left>%s</td></tr>\n',sprintf('%s ',settings.standardNames{:}));
fprintf(fid,'<tr><td align=left>Drift standards:</td><td align=left>%s</td></tr>\n',sprintf('%s ',settings.standardsDrift{:}));
fprintf(fid,'<tr><td align=left>Injections/standard :</td><td align=left>%d</td></tr>\n',meas(stdv(1)).InjNr);
fprintf(fid,'<tr><td align=left>Number of injections:</td><td align=left>%d</td></tr>\n',length(data.Line));
fprintf(fid,'<tr><td align=left>Method              :</td><td align=left>%s</td></tr>\n',data(1).Method{1});
fprintf(fid,'<tr><td align=left>Calibration points  :</td><td align=left>%d</td></tr>\n',settings.calibrationPoints);
fprintf(fid,'<tr><td align=left>Calibration type    :</td><td align=left>%s</td></tr>\n',interp{settings.calibrationInterpolation});
fprintf(fid,'<tr><td align=left>Operator name       :</td><td align=left>%s',settings.operator);
fprintf(fid,' (email: <a href="mailto:%s">%s)</a></td>',settings.operatorContact,settings.operatorContact);
fprintf(fid,'<tr><td align=left>17-O analysis       :</td><td align=left>%s</td></tr>\n',tf{settings.includeE17+1});
fprintf(fid,'<tr><td align=left>Humidity Correction :</td><td align=left>%s</td></tr>\n',tf{settings.useHumidityCorrection+1});
fprintf(fid,'<tr><td align=left>Drift Correction    :</td><td align=left>%s</td></tr>\n',tf{settings.useDriftCorrection+1});
fprintf(fid,'<tr><td align=left>Memory Correction   :</td><td align=left>%s</td></tr>\n',tf{settings.useMemoryCorrection+1});
fprintf(fid,'<tr><td align=left>Calibration log file:</td><td align=left><a href=%s>calibration_%s.log</a></td></tr>\n',...
  sprintf('%s/calibration_%s.log',rdir,datestr(settings.reportDate,'yyyymmdd')),...
  datestr(settings.reportDate,'yyyymmdd'));
fprintf(fid,'<tr><td align=left>Operator comments   :</td><td align=left>%s</td></tr>\n',settings.comment);
fprintf(fid,'</table><hr>\n');

% Section 2: Calibrated measurements
fprintf(fid,'<h3 id="sec2">2. Calibrated measurements</h3><p>');

% open csv file for writing
cid = fopen(sprintf('%s/%s_calibrated.csv',rdir,settings.runID),'w');

% open csv file for writing
sid = fopen(sprintf('%s/%s_averages.csv',rdir,settings.runID),'w');

% open csv file for writing
uid = fopen(sprintf('%s/%s_uncertainty.csv',rdir,settings.runID),'w');

if settings.writeReport==1
  % open accounting csv file for writing
  oid = fopen(sprintf('%s/%s_accounting.csv',rdir,settings.runID),'w');
  % open extended output csv file for writing
  aid = fopen(sprintf('%s/%s_alldata.csv',rdir,settings.runID),'w');
  % open parameter output csv file for writing
  pid = fopen(sprintf('%s/%s_parameters.csv',rdir,settings.runID),'w');
end

fprintf(fid,'<b>Table 1: Calibrated measurement values for each sample (&permil;).</b> Combined uncertainty provided according to Gr&ouml;ning (2018). Last column provides quality flags as explained in the legend at the bottom.<p>\n');
fprintf(fid,'<table>');
if settings.includeE17==1
  fprintf(fid,'<tr><th>%4s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%8s</th></tr>',...
    'Vial','Sample name',[config.labname,' ID'],'&delta;<sup>2</sup>H','&delta;<sup>18</sup>O','&delta;<sup>17</sup>O','dxs','e17','Quality flags');
else
  fprintf(fid,'<tr><th>%4s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%8s</th></tr>',...
    'Vial','Sample name',[config.labname,' ID'],'&delta;<sup>2</sup>H','&delta;<sup>18</sup>O','dxs','Quality flags');
end
if settings.includeE17==1
  fprintf(cid,'%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n','"Vial"','"Sample_name"','"Lab_ID"','"dD"','"dD_u"','"d18O"','"d18O_u"','"d17O"','"d17O_u"','"dxs"','"dxs_u"','"e17"','"e17_u"','"Quality_flags"');
else
  fprintf(cid,'%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n','"Vial"','"Sample_name"','"Lab_ID"','"dD"','"dD_u"','"d18O"','"d18O_u"','"dxs"','"dxs_u"','"Quality_flags"');
end
if settings.writeReport==1
  fprintf(oid,'%s,%s,%s,%s,%s,%s,%s\n','"Sample"','"Sample_name"','"Lab_ID"','"Vial"','"Run"','"Operator"','"Lab_Filename"');
  if settings.includeE17==1
    fprintf(aid,'%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s ,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n','"Vial"','"Sample_name"','"Lab_ID"','"dD_raw"','"dD_raw_u"','"d18O_raw"','"d18O_raw_u"','"d17O_raw"','"d17O_raw_u"','"dxs_raw"','"dxs_raw_u"','"e17_raw"','"e17_raw_u"','"dD_cor"','"dD_cor_u"','"d18O_cor"','"d18O_cor_u"','"d17O_cor"','"d17O_cor_u"','"dxs_cor"','"dxs_cor_u"','"e17_cor"','"e17_cor_u"','"dD_cal"','"dD_cal_u"','"d18O_cal"','"d18O_cal_u"','"d17O_cal"','"d17O_cal_u"','"dxs_cal"','"dxs_cal_u"','"e17_cal"','"e17_cal_u"','"Quality_flags"');
  else
    fprintf(aid,'%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n','"Vial"','"Sample_name"','"Lab_ID"','"dD_raw"','"dD_raw_u"','"d18O_raw"','"d18O_raw_u"','"dxs_raw"','"dxs_raw_u"','"dD_cor"','"dD_cor_u"','"d18O_cor"','"d18O_cor_u"','"dxs_cor"','"dxs_cor_u"','"dD_cal"','"dD_cal_u"','"d18O_cal"','"d18O_cal_u"','"dxs_cal"','"dxs_cal_u"','"Quality_flags"');
  end
end
for j = 1:length(smpv)
  flags{j}='';
  flagn{j}=0;
  for q = 1:6
    if qflags(j,q)==1
      flags{j}=[flags{j},num2str(q),' '];
      flagn{j}=flagn{j}+2^(q-1);
    end
  end
  if settings.includeE17==1
    fprintf(fid,'<tr><td align=left>%4d</td><td align=left>%20s</td><td align=left>%20s</td><td align=right>%20.2f&plusmn;%20.2f</td><td align=right>%20.4f&plusmn;%20.4f</td><td align=right>%20.4f&plusmn;%20.4f</td><td align=right>%20.2f&plusmn;%20.2f</td><td align=right>%20.1f&plusmn;%20.1f</td><td align=right><i>%s</i></td></tr>\n',...
      smpv(j),...
      meas(smpv(j)).name1,...
      meas(smpv(j)).name2,...
      cal(smpv(j)).dD,cal(smpv(j)).dD_SD,...
      cal(smpv(j)).d18O,cal(smpv(j)).d18O_SD,...
      cal(smpv(j)).d17O,cal(smpv(j)).d17O_SD,...
      cal(smpv(j)).dxs,cal(smpv(j)).dxs_SD,...
      cal(smpv(j)).e17,cal(smpv(j)).e17_SD,...
      sprintf('%s (%d)',flags{j},flagn{j}));
  else
    fprintf(fid,'<tr><td align=left>%4d</td><td align=left>%20s</td><td align=left>%20s</td><td align=right>%20.1f&plusmn;%20.1f</td><td align=right>%20.2f&plusmn;%20.2f</td><td align=right>%20.1f&plusmn;%20.1f</td><td align=right><i>%s</i></td></tr>\n',...
      smpv(j),...
      meas(smpv(j)).name1,...
      meas(smpv(j)).name2,...
      cal(smpv(j)).dD,cal(smpv(j)).dD_SD,...
      cal(smpv(j)).d18O,cal(smpv(j)).d18O_SD,...
      cal(smpv(j)).dxs,cal(smpv(j)).dxs_SD,...
      sprintf('%s (%d)',flags{j},flagn{j}));
  end
  if settings.includeE17==1
    fprintf(cid,'%4d,"%s","%s",%8.2f,%5.2f,%10.4f,%7.4f,%10.4f,%7.4f,%8.2f,%5.2f,%8.1f,%5.1f,%d\n',...
      smpv(j),...
      meas(smpv(j)).name1,...
      meas(smpv(j)).name2,...
      cal(smpv(j)).dD,cal(smpv(j)).dD_SD,...
      cal(smpv(j)).d18O,cal(smpv(j)).d18O_SD,...
      cal(smpv(j)).d17O,cal(smpv(j)).d17O_SD,...
      cal(smpv(j)).dxs,cal(smpv(j)).dxs_SD,...
      cal(smpv(j)).e17,cal(smpv(j)).e17_SD,...
      flagn{j});
  else
    fprintf(cid,'%4d,"%s","%s",%8.1f,%5.1f,%10.2f,%7.2f,%8.1f,%5.1f,%d\n',...
      smpv(j),...
      meas(smpv(j)).name1,...
      meas(smpv(j)).name2,...
      cal(smpv(j)).dD,cal(smpv(j)).dD_SD,...
      cal(smpv(j)).d18O,cal(smpv(j)).d18O_SD,...
      cal(smpv(j)).dxs,cal(smpv(j)).dxs_SD,...
      flagn{j});
  end
  if settings.writeReport==1
    % accounting data file for excel accounting/database upload
    fprintf(oid,'%4d,"%s","%s",%4d,"%s","%s","%s"\n',...
      j,...
      meas(smpv(j)).name1,...
      meas(smpv(j)).name2,...
      smpv(j),...
      settings.runID,...
      settings.operator,...
      meas(smpv(j)).filename);
  end
end
fclose(cid);
if settings.writeReport==1
  for j = 1:length(meas)
    flagn{j}=0;
    for q = 1:6
      if qflags(j,q)==1
        flagn{j}=flagn{j}+2^q;
      end
    end
    % extended data file with all data
    if settings.includeE17==1
      fprintf(aid,'%4d,"%s","%s",%10.2f,%7.2f,%10.4f,%7.4f,%10.4f,%7.4f,%8.2f,%5.2f,%8.1f,%5.1f,%10.2f,%7.2f,%10.4f,%7.4f,%10.4f,%7.4f,%8.2f,%5.2f,%8.1f,%5.1f,%8.2f,%5.2f,%8.4f,%5.4f,%10.4f,%7.4f,%8.2f,%5.2f,%8.1f,%5.1f,%d\n',...
        j,...
        meas(j).name1,...
        meas(j).name2,...
        meas(j).dD_uc,meas(j).dD_SD_uc,...
        meas(j).d18O_uc,meas(j).d18O_SD_uc,...
        meas(j).d17O_uc,meas(j).d17O_SD_uc,...
        meas(j).dxs_uc,meas(j).dxs_SD_uc,...
        meas(j).e17_uc,meas(j).e17_SD_uc,...
        meas(j).dD,meas(j).dD_SD,...
        meas(j).d18O,meas(j).d18O_SD,...
        meas(j).d17O,meas(j).d17O_SD,...
        meas(j).dxs,meas(j).dxs_SD,...
        meas(j).e17,meas(j).e17_SD,...
        cal(j).dD,cal(j).dD_SD,...
        cal(j).d18O,cal(j).d18O_SD,...
        cal(j).d17O,cal(j).d17O_SD,...
        cal(j).dxs,cal(j).dxs_SD,...
        cal(j).e17,cal(j).e17_SD,...
        flagn{j});
    else
      fprintf(aid,'%4d,"%s","%s",%10.1f,%7.1f,%10.2f,%7.2f,%8.1f,%5.1f,%10.1f,%7.1f,%10.2f,%7.2f,%8.1f,%5.1f,%8.1f,%5.1f,%8.2f,%5.2f,%8.1f,%5.1f,%d\n',...
        j,...
        meas(j).name1,...
        meas(j).name2,...
        meas(j).dD_uc,meas(j).dD_SD_uc,...
        meas(j).d18O_uc,meas(j).d18O_SD_uc,...
        meas(j).dxs_uc,meas(j).dxs_SD_uc,...
        meas(j).dD,meas(j).dD_SD,...
        meas(j).d18O,meas(j).d18O_SD,...
        meas(j).dxs,meas(j).dxs_SD,...
        cal(j).dD,cal(j).dD_SD,...
        cal(j).d18O,cal(j).d18O_SD,...
        cal(j).dxs,cal(j).dxs_SD,...
        flagn{j});
    end
  end
end
if settings.writeReport==1
  % write parameters file
  if settings.includeE17==1
    fprintf(pid,'"drift_bias_d18O",%5.2f\n"drift_bias_dD",%5.1f\n"drift_bias_d17O",%5.1f\n',...
      driftBias(1),driftBias(2),driftBias(3));
    fprintf(pid,'"internal_reproducibility_d18O",%5.2f\n"internal_reproducibility_dD",%5.1f\n"internal_reproducibility_d17O",%5.1f\n',...
      repint(1),repint(2),repint(3));
    fprintf(pid,'"c0_d18O",%4.2f\n"alpha_d18O",%4.2f\n"beta_d18O",%4.2f\n"b_d18O",%4.2f\n"RMSE_d18O",%5.3f\n',settings.mem.c0(1),settings.mem.alpha(1),settings.mem.beta(1),settings.mem.b(1),settings.mem.rmse(1));
    fprintf(pid,'"c0_dD",%4.2f\n"alpha_dD",%4.2f\n"beta_dD",%4.2f\n"b_dD",%4.2f\n"RMSE_dD",%5.3f\n',settings.mem.c0(2),settings.mem.alpha(2),settings.mem.beta(2),settings.mem.b(2),settings.mem.rmse(2));
    fprintf(pid,'"c0_d17O",%4.2f\n"alpha_d17O",%4.2f\n"beta_d17O",%4.2f\n"b_d17O",%4.2f\n"RMSE_d17O",%5.3f\n',settings.mem.c0(3),settings.mem.alpha(3),settings.mem.beta(3),settings.mem.b(3),settings.mem.rmse(3));
  else
    fprintf(pid,'"drift_bias_d18O",%5.2f\n"drift_bias_dD",%5.1f\n',...
      driftBias(1),driftBias(2));
    fprintf(pid,'"internal_reproducibility_d18O",%5.2f\n"internal_reproducibility_dD",%5.1f\n',...
      repint(1),repint(2));
    fprintf(pid,'"c0_d18O",%4.2f\n"alpha_d18O",%4.2f\n"beta_d18O",%4.2f\n"b_d18O",%4.2f\n"RMSE_d18O",%5.3f\n',settings.mem.c0(1),settings.mem.alpha(1),settings.mem.beta(1),settings.mem.b(1),settings.mem.rmse(1));
    fprintf(pid,'"c0_d17O",%4.2f\n"alpha_d17O",%4.2f\n"beta_d17O",%4.2f\n"b_d17O",%4.2f\n"RMSE_d17O",%5.3f\n',settings.mem.c0(3),settings.mem.alpha(3),settings.mem.beta(3),settings.mem.b(3),settings.mem.rmse(3));
    fprintf(pid,'"c0_dD",%4.2f\n"alpha_dD",%4.2f\n"beta_dD",%4.2f\n"b_dD",%4.2f\n"RMSE_dD",%5.3f\n',settings.mem.c0(2),settings.mem.alpha(2),settings.mem.beta(2),settings.mem.b(2),settings.mem.rmse(2));
    fprintf(pid,'"project_run_ID","%s"\n',settings.runID);
    fprintf(pid,'"project_name","%s"\n',settings.project);
    fprintf(pid,'"project_type","%s"\n',settings.projectType);
    fprintf(pid,'"customer_name","%s"\n',settings.customer);
    fprintf(pid,'"operator_name","%s"\n',settings.operator);
    fprintf(pid,'"instrument_serial_number","%s"\n',settings.instrument);
    fprintf(pid,'"start_date","%s"\n',datestr(data.TimeCode(1),'yyyy-mm-dd HH:MM'));
    fprintf(pid,'"end_date","%s"\n',datestr(data.TimeCode(end),'yyyy-mm-dd HH:MM'));
    fprintf(pid,'"method","%s"\n',data(1).Method{1});
    fprintf(pid,'"averaged_inj_first",%d\n',ends1+1);
    fprintf(pid,'"averaged_inj_all",%d\n',ends+1);
    fprintf(pid,'"calibration standards","%s"\n',sprintf('%s ',settings.standardNames{:}));
    fprintf(pid,'"drift_standards","%s"\n',sprintf('%s ',settings.standardsDrift{:}));
    fprintf(pid,'"injections_per_standard",%d\n',meas(stdv(1)).InjNr);
    fprintf(pid,'"calibration_points",%3d\n',settings.calibrationPoints);
    fprintf(pid,'"calibration_type",%1d\n',settings.calibrationInterpolation);
    fprintf(pid,'"humidity_correction",%1d\n',settings.useHumidityCorrection);
    fprintf(pid,'"drift_correction",%1d\n',settings.useDriftCorrection);
    fprintf(pid,'"memory_correction",%1d\n',settings.useMemoryCorrection);
    fprintf(pid,'"samples_count",%3d\n',length(smpv));
    if ~isempty(smpv)
      fprintf(pid,'"injections_per_sample",%3d\n',meas(smpv(1)).InjNr);
    end
    fprintf(pid,'"total_injections",%d\n',length(data.Line));
  end
  % close all files
  fclose(pid);
  fclose(oid);
  fclose(aid);
end
fprintf(fid,'</table><p>\n');
fprintf(fid,'<i>Quality flag legend: (Bit 1, value 1): Humidity variation during injections, (Bit 2, value 2): Humidity variation between injections, (Bit 3, value 4): Large isotopic variability, (Bit 4, value 8): Sample outside range of standards, (Bit 5, value 16): Instrument temperature variability, (Bit 6, value 32): Air mode</i><p>\n');

% write link to regular result tables
fprintf(fid,'<a href="%s_calibrated.csv">Download calibrated data file</a> in CSV (comma separated values) format for import in Excel)<br>\n',settings.runID);

% Write averages table with samples and drift standard
% find the number of lines to print
if settings.writeReport==1
  as=[driv,smpv];
else
  as=smpv;
end
anames=unique({meas(as).name1});

% determine if this table should be written
n=1;
for a = 1:length(anames)
  js=find(strcmp({meas(as).name1},anames{a}));
  js=as(js);
  n=max(n,length(js));
end

if n>1
  fprintf(fid,'<b>Table 2:</b> Mean calibrated measurement values per sample and combined uncertainty (&permil;).<p>\n');
  fprintf(fid,'<table>');
  if settings.includeE17==1
    fprintf(fid,'<tr><th>%4s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%8s</th></tr>',...
      'Vial','Sample name',[config.labname,' ID'],'&delta;<sup>2</sup>H','&delta;<sup>18</sup>O','&delta;<sup>17</sup>O','dxs','e17','Count');
  else
    fprintf(fid,'<tr><th>%4s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%8s</th></tr>',...
      'Vial','Sample name',[config.labname,' ID'],'&delta;<sup>2</sup>H','&delta;<sup>18</sup>O','dxs','Count');
  end
  if settings.includeE17==1
    fprintf(sid,'%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n','"Vial"','"Sample_name"','"Lab_ID"','"dD"','"dD_u"','"d18O"','"d18O_u"','"d17O"','"d17O_u"','"dxs"','"dxs_u"','"e17"','"e17_u"','"Count"');
  else
    fprintf(sid,'%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n','"Vial"','"Sample_name"','"Lab_ID"','"dD"','"dD_u"','"d18O"','"d18O_u"','"dxs"','"dxs_u"','"Count"');
  end
  for a = 1:length(anames)
    js=find(strcmp({meas(as).name1},anames{a}));
    js=as(js);
    n=sqrt(length(js));
    % only write lines for repeated samples
    if n>1
      if settings.includeE17==1
        fprintf(fid,'<tr><td align=left>%4d</td><td align=left>%20s</td><td align=left>%20s</td><td align=right>%20.2f&plusmn;%20.2f</td><td align=right>%20.4f&plusmn;%20.4f</td><td align=right>%20.4f&plusmn;%20.4f</td><td align=right>%20.2f&plusmn;%20.2f</td><td align=right>%20.1f&plusmn;%20.1f</td><td align=right><i>%d</i></td></tr>\n',...
          a,...
          meas(js(1)).name1,...
          meas(js(1)).name2,...
          mean([cal(js).dD]),sqrt((std([cal(js).dD])/n)^2+mean([cal(js).dD_SD])^2),...
          mean([cal(js).d18O]),sqrt((std([cal(js).d18O])/n)^2+mean([cal(js).d18O_SD])^2),...
          mean([cal(js).d17O]),sqrt((std([cal(js).d17O])/n)^2+mean([cal(js).d17O_SD])^2),...
          mean([cal(js).dxs]),sqrt((std([cal(js).dxs])/n)^2+mean([cal(js).dxs_SD])^2),...
          mean([cal(js).e17]),sqrt((std([cal(js).e17])/n)^2+mean([cal(js).e17_SD])^2),...
          length(js));
      else
        fprintf(fid,'<tr><td align=left>%4d</td><td align=left>%20s</td><td align=left>%20s</td><td align=right>%20.1f&plusmn;%20.1f</td><td align=right>%20.2f&plusmn;%20.2f</td><td align=right>%20.1f&plusmn;%20.1f</td><td align=right><i>%d</i></td></tr>\n',...
          a,...
          meas(js(1)).name1,...
          meas(js(1)).name2,...
          mean([cal(js).dD]),sqrt((std([cal(js).dD])/n)^2+mean([cal(js).dD_SD])^2),...
          mean([cal(js).d18O]),sqrt((std([cal(js).d18O])/n)^2+mean([cal(js).d18O_SD])^2),...
          mean([cal(js).dxs]),sqrt((std([cal(js).dxs])/n)^2+mean([cal(js).dxs_SD])^2),...
          length(js));
      end
      if settings.includeE17==1
        fprintf(sid,'%4d,"%s","%s",%8.2f,%5.2f,%10.4f,%7.4f,%10.4f,%7.4f,%8.2f,%5.2f,%8.1f,%5.1f,%d\n',...
          a,...
          meas(js(1)).name1,...
          meas(js(1)).name2,...
          mean([cal(js).dD]),sqrt((std([cal(js).dD])/n)^2+mean([cal(js).dD_SD])^2),...
          mean([cal(js).d18O]),sqrt((std([cal(js).d18O])/n)^2+mean([cal(js).d18O_SD])^2),...
          mean([cal(js).d17O]),sqrt((std([cal(js).d17O])/n)^2+mean([cal(js).d17O_SD])^2),...
          mean([cal(js).dxs]),sqrt((std([cal(js).dxs])/n)^2+mean([cal(js).dxs_SD])^2),...
          mean([cal(js).e17]),sqrt((std([cal(js).e17])/n)^2+mean([cal(js).e17_SD])^2),...
          length(js));
      else
        fprintf(sid,'%4d,"%s","%s",%8.1f,%5.1f,%10.2f,%7.2f,%8.1f,%5.1f,%d\n',...
          a,...
          meas(js(1)).name1,...
          meas(js(1)).name2,...
          mean([cal(js).dD]),sqrt((std([cal(js).dD])/n)^2+mean([cal(js).dD_SD])^2),...
          mean([cal(js).d18O]),sqrt((std([cal(js).d18O])/n)^2+mean([cal(js).d18O_SD])^2),...
          mean([cal(js).dxs]),sqrt((std([cal(js).dxs])/n)^2+mean([cal(js).dxs_SD])^2),...
          length(js));
      end
    end
  end
  fclose(sid);
  fprintf(fid,'</table><p>\n');
  fprintf(fid,'<a href="%s_averages.csv">Download averages data file</a> in CSV (comma separated values) format for import in Excel)<br>\n',settings.runID);
else
  fprintf(fid,'<p>No samples or standards were measured repeatedly.<p>\n');
end


% write accounting information file
if settings.writeReport==1
  fprintf(fid,'<a href="%s_accounting.csv">Download accouting file</a> in CSV (comma separated values) format for import in Excel)<br>\n',settings.runID);
end
if settings.writeReport==1
  fprintf(fid,'<a href="%s_alldata.csv">Download extended data file with raw and calibrated data</a> in CSV (comma separated values) format for import in Excel)<br>\n',settings.runID);
  fprintf(fid,'<a href="%s_parameters.csv">Download parameter data file with run parameters and characteristics</a> in CSV (comma separated values) format for import in Excel)<br>\n',settings.runID);
  
  % links for upload to database, specific to FARLAB
  if strcmp(config.labname,'FARLAB')==1
    fprintf(fid,'<br><b>Upload to FARLAB data base (within UiB network, login required):</b><br>\n');
    fprintf(fid,'- <a target="_blank" rel="noopener" href=http://echo.geo.uib.no:8085/samples.html>Samples table</a> (use file %s_accounting.csv)<br>',settings.runID);
    fprintf(fid,'- <a target="_blank" rel="noopener" href=http://echo.geo.uib.no:8085/results.html>Results table</a> (use file %s_calibrated.csv)<br>',settings.runID);
  end
  
end
fprintf(fid,'%s\n',figstr{1}); % Figure 1

fprintf(fid,'<p>%s\n',figstr{2}); % Figure 2
fprintf(fid,'<hr>\n');

% Section 3: Acknowledgement description
fprintf(fid,'<h3 id="sec3">3. Acknowledgement section for publications</h3><p>\n');
fprintf(fid,config.acknowledgement);
fprintf(fid,'<hr>\n');

% Section 4: Method description
fprintf(fid,'<h3 id="sec4">4. Method description for publications</h3><p>\n');
fprintf(fid,'Samples were filtered, if needed, with 25mm Nylon filters with a 0.2 um PTFE membrane (part #514-0066, VWR, USA), and transferred to 1.5 ml glass vials with rubber/PTFE septa (part #548-0907, VWR, USA). ');
fprintf(fid,'An autosampler (A0325, Picarro Inc) transferred ca. 2ul per injection into a high-precision vapourizer (A0211, Picarro Inc, USA) heated to 110&deg;C. After blending with dry N<sub>2</sub> (< 5 ppm H<sub>2</sub>O) the gas mixture was directed into the measurement cavity of a Cavity-Ring Down Spectrometer (L2140-i, Picarro Inc) for about 7 min with a typical water concentration of 20 000 ppm.<p>\n');
% duration of vapour stream
if ~isempty(smpv)
  fprintf(fid,'Memory effects were reduced by two times measuring a vapour mixture at a mixing ratio of 50 000 ppm, obtained from 2 injections of 2 ul for 5 min at the beginning of each new sample vial. Thereafter, another %d injections of 2 ul per sample were measured individually as described above, and averages of the last %d injections were used for further processing.<p>\n',meas(smpv(1)).InjNr,ends+1);
end
% duration of wet flush
fprintf(fid,'Two or three standards were measured at the beginning and end of each run, after June 2021 only at the beginning of a run. ');
% frequency of drift standard
fprintf(fid,'runs consisted typically of 20 samples, with drift standard ');
for j=1:length(settings.standardsDrift)
  if settings.includeE17==1
    fprintf(fid,'%s %s (&delta;<sup>2</sup>H: %8.2f&plusmn;%4.2f permil, d<sup>18</sup>O: %10.4f&plusmn;%6.4f permil, d<sup>17</sup>O: %10.4f&plusmn;%6.4f permil), ',...
      certd(j).name1,certd(j).name2,certd(j).dD,certd(j).dD_SD,certd(j).d18O,certd(j).d18O_SD,certd(j).d17O,certd(j).d17O_SD);
  else
    fprintf(fid,'%s %s (&delta;<sup>2</sup>H: %8.2f&plusmn;%4.2f permil, d<sup>18</sup>O: %10.4f&plusmn;%6.4f permil), ',...
      certd(j).name1,certd(j).name2,certd(j).dD,certd(j).dD_SD,certd(j).d18O,certd(j).d18O_SD);
  end
end
fprintf(fid,' measured typically every 5 samples. ');
fprintf(fid,'For calibration according to IAEA recommendations, the laboratory standards ');
for j=1:standards
  if settings.includeE17==1
    fprintf(fid,'%s %s (&delta;<sup>2</sup>H: %8.2f&plusmn;%4.2f permil, d<sup>18</sup>O: %10.4f&plusmn;%6.4f permil, d<sup>17</sup>O: %10.4f&plusmn;%6.4f permil), ',...
      certs(j).name1,certs(j).name2,certs(j).dD,certs(j).dD_SD,certs(j).d18O,certs(j).d18O_SD,certs(j).d17O,certs(j).d17O_SD);
  else
    fprintf(fid,'%s %s (&delta;<sup>2</sup>H: %8.2f&plusmn;%4.2f permil, d<sup>18</sup>O: %10.4f&plusmn;%6.4f permil), ',...
      certs(j).name1,certs(j).name2,certs(j).dD,certs(j).dD_SD,certs(j).d18O,certs(j).d18O_SD,certs(j).d17O,certs(j).d17O_SD);
  end
end
fprintf(fid,' were used, and averaged over the beginning and end of each run for calibration.<p>\n');
% uncertainty calculation
fprintf(fid,'Combined uncertainty u<sub>x</sub> (Table 1) was computed from the square root of the squared sum of all error components, including the assigned uncertainty of the calibration standards, the measurement uncertainty of the calibration standards, and the long-term measurement reproducibility and repeatability.<p>\n');
% humidity correction
if settings.useHumidityCorrection==1
  fprintf(fid,'An empirical humidity dependency correction was applied to all measurements. The linear correction functions have been obtained from linear regression to injections with variable water amounts with several secondary water standards in use at %s. The humidity correction is on the order of 0.2 permil for d18O and 2 permil for &delta;<sup>2</sup>H within the range of 15000 to 25000 ppmv.<p>\n',config.labname);
end
% memory correction
if settings.useMemoryCorrection==1
  fprintf(fid,'An empirical memory correction to represent fast and slow memory compartments in the system has been applied. The correction is based on a fit of the 3-parameter function f(i) = M1 * (b exp(-alpha*i)+(1-b) exp(-beta*i)), where M1 is the initial memory amplitude (calculated), b is the weight between the fast and slow component, and alpha and beta are the decay constants of the fast and slow component, respectively, to a sufficiently long sequence of injections (typically 12 injections from all standard measurements).<p>\n');
end
% drift correction
if settings.useDriftCorrection==1
  fprintf(fid,'Linear drift across a run was computed from the deviation of the mean of repeated measurements of both calibration and drift standards, taking the time of measurement into account. Drift correction was then calculated from a linear regression to the offset values and applied to all measured vials.<p>\n');
end
if sum(repint(:))>0
  if settings.includeE17==1
    fprintf(fid,'Internal reproducibility was estimated from the 1-sigma standard deviation of repeated measurements of a drift standard to %5.3f permil for d<sup>18</sup>O, %5.3f permil for &delta;<sup>2</sup>H, and %5.3 permil for d<sup>17</sup>O. ',repint(1),repint(2),repint(3));
  else
    fprintf(fid,'Internal reproducibility was estimated from the 1-sigma standard deviation of repeated measurements of a drift standard to %5.3f permil for d<sup>18</sup>O and %5.3f permil for &delta;<sup>2</sup>H. ',repint(1),repint(2));
  end
end
% different for 3-point calibration
fprintf(fid,sprintf('Long-term reproducibility across different instruments is %7.2f permil for d<sup>18</sup>O,  %7.2f permil for &delta;<sup>2</sup>H, and %7.2f permil for d-excess, evaluated from the 1-sigma standard deviation for the repeated analysis of an internal laboratory standard over more than 1 year.<p>\n',LTP(1),LTP(2),LTP(3)));
% long term reproducibility of both isotopes
fprintf(fid,'<hr>\n');

if settings.writeReport==1
  
  % open csv file for writing
  cid = fopen(sprintf('%s/%s_standards.csv',rdir,settings.runID),'w');
  
  % Section 5: Memory correction
  fprintf(fid,'<h3 id="sec5">5. Memory correction</h3><p>');
  fprintf(fid,'<p>\n');
  mspec='';
  minj=[];
  % chose max index depending on 17O-mode or not
  if settings.includeE17==1
    maxj=3;
  else
    maxj=2;
  end
  for j=1:maxj
    if settings.mem.enableCorrection(j)==1 && settings.useMemoryCorrection==1
      mspec=[mspec, iso{j},', '];
      minj=[minj, settings.mem.minInj(j)];
    end
  end
  if ~isempty(mspec)
    fprintf(fid,'<p>Empirical memory correction was used for species %s.',mspec(1:end-2));
    fprintf(fid,' The fit was obtained using the memory effect with at least %s injections, respectively. No memory correction was performed when the between-sample difference was less than 0.75 permil for &delta;<sup>18</sup>O and &delta;<sup>17</sup>O, and less than 5 permil for &delta;<sup>2</sup>H, or when preceeding samples were excluded from analysis.<br>',sprintf('%d, ',minj));
    if ~isempty(settings.mem.standards)
      fprintf(fid,' In addition, the memory correction fit was restricted to the following sample names: %s.',settings.mem.standards);
    end
    fprintf(fid,'<p>The following settings were used for the fitting function of each species:<br>');
    fprintf(fid,'f(i) = M1 * (b exp(-alpha*i)+(1-b) exp(-beta*i))<br>');
    for j=1:maxj
      fprintf(fid,'%s: alpha: %5.2f, beta: %5.2f, b: %4.2f, M1: %4.2f with a RMSE of %5.3f. Note that the initial memory M1 is named c0 in the output files.<br>',iso{j},settings.mem.alpha(j),settings.mem.beta(j),settings.mem.b(j),settings.mem.c0(j),settings.mem.rmse(j));
    end
    fprintf(fid,'<p>The impact of the memory correction can be inspected from the green symbols in the individual sample plots in the Appendix (Figures Ax).<br>');
    fprintf(fid,'%s\n',figstr{5}); % Figure 3
    fprintf(fid,'<hr>\n');
  else
    fprintf(fid,'<p>No empirical memory correction was used for processing this run.');
    fprintf(fid,'<hr>\n');
  end
  
  % Section 6: Drift correction
  fprintf(fid,'<h3 id="sec6">6. Drift correction</h3><p>');
  
  if settings.useDriftCorrection==1
    fprintf(fid,'<p>Linear drift across the run was computed from the deviation of the mean of repeated measurements of both calibration and drift standards, taking the time of measurement into account. Drift correction was then calculated from a linear regression to the offset values and applied to all measured vials.\n');
    if settings.includeE17==1
      fprintf(fid,'Linear regressions resulted in slopes of s=%5.3f permil/day for &delta;<sup>18</sup>O, s=%5.3f permil/day for &delta;<sup>2</sup>H, and s=%5.3f permil/day for &delta;<sup>17</sup>O (Fig. 6).\n',slp{1,1}(2),slp{1,2}(2),slp{1,3}(2));
      fprintf(fid,'The respective correlation coefficients were r=%5.3f for &delta;<sup>18</sup>O, r=%5.3f for &delta;<sup>2</sup>H, and r=%5.3f for &delta;<sup>17</sup>O. Correction magnitude was up to %5.3f permil for &delta;<sup>18</sup>O, %5.3f permil for &delta;<sup>2</sup>H, and %5.3f permil for &delta;<sup>17</sup>O.',dcor(1),dcor(2),dcor(3),dmag(1),dmag(2),dmag(3));
      fprintf(fid,'The RMSE from the residuals is %5.3f permil for &delta;<sup>18</sup>O and %5.3f permil for &delta;<sup>2</sup>H, and %5.3f permil for &delta;<sup>17</sup>O.',drmse(1),drmse(2),drmse(3));
    else
      fprintf(fid,'Linear regressions resulted in slopes of s=%5.3f permil/day for &delta;<sup>18</sup>O and s=%5.3f permil/day for &delta;<sup>2</sup>H (Fig. 6).\n',slp{1,1}(2),slp{1,2}(2));
      fprintf(fid,'The respective correlation coefficients were r=%5.3f for &delta;<sup>18</sup>O and r=%5.3f for &delta;<sup>2</sup>H. Correction magnitude was up to %5.3f permil for &delta;<sup>18</sup>O and %5.3f permil for &delta;<sup>2</sup>H.',dcor(1),dcor(2),dmag(1),dmag(2));
      fprintf(fid,'The RMSE from the residuals is %5.3f permil for &delta;<sup>18</sup>O and %5.3f permil for &delta;<sup>2</sup>H.',drmse(1),drmse(2));
    end
    fprintf(fid,'<br>\n');
    fprintf(fid,'%s\n',figstr{4}); % Figure 4 in report
  else
    fprintf(fid,'<p>No empirical drift correction was used for processing this run.<br>\n');
  end
  fprintf(fid,'<hr>\n');
  
  % Section 7: Calibration
  fprintf(fid,'<h3 id="sec7">7. Calibration to VSMOW-SLAP scale</h3><p>\n');
  fprintf(fid,'Standards:<br>\n');
  for j=1:standards
    if settings.includeE17==1
      fprintf(fid,'%d. %s %s (&delta;<sup>2</sup>H: %8.2f&plusmn;%4.2f permil, d<sup>18</sup>O: %10.4f&plusmn;%10.4f permil, d<sup>17</sup>O: %10.4f&plusmn;%6.4f permil)<br>',...
        j,certs(j).name1,certs(j).name2,certs(j).dD,certs(j).dD_SD,certs(j).d18O,certs(j).d18O_SD,certs(j).d17O,certs(j).d17O_SD);
    else
      fprintf(fid,'%d. %s %s (&delta;<sup>2</sup>H: %8.2f&plusmn;%4.2f permil, d<sup>18</sup>O: %10.4f&plusmn;%6.4f permil)<br>',...
        j,certs(j).name1,certs(j).name2,certs(j).dD,certs(j).dD_SD,certs(j).d18O,certs(j).d18O_SD);
    end
  end
  if settings.calibrationInterpolation==1
    fprintf(fid,'A constant average of the standards has been used for calibration throughout the run.<br>');
  else
    fprintf(fid,'A linear interpolation between the standards has been used for calibration interpolated to each individual sample at the time during the run.<br>');
  end
  if settings.calibrationPoints==2
    fprintf(fid,'Standard %s chosen for zero-point and %s for slope correction.<br>\n',settings.standardNames{stdn},settings.standardNames{stdm});
  else
    fprintf(fid,'Standard ranges of the 3-point calibration have been identified as %s-%s and %s-%s\n',certs(stdmin).name1,certs(stdmid).name1,certs(stdmid).name1,certs(stdmax).name1);
  end
  for j=1:settings.calibrationPoints
    fprintf(fid,'Standard %s at vials %s<br>\n',settings.standardNames{j},int2str(stdrep{j}));
  end
  fprintf(fid,'%s\n',figstr{3}); % Figure 5
  
  fprintf(fid,'<p>Bias from drift standards:<br>\n');
  if settings.includeE17==1
    fprintf(fid,' After drift correction, the remaining bias of the calibrated drift standard samples was %5.3f permil for &delta;<sup>18</sup>O, %5.3f permil for &delta;<sup>2</sup>H, and %5.3f permil for &delta;<sup>17</sup>O.<br>',driftBias(1),driftBias(2),driftBias(3));
  else
    fprintf(fid,' After drift correction, the remaining bias of the calibrated drift standard samples was %5.3f permil for &delta;<sup>18</sup>O and %5.3f permil for &delta;<sup>2</sup>H.<br>',driftBias(1),driftBias(2));
  end
  
  fprintf(fid,'<p><b>Table 3: Calibrated measurement values per drift standard.</b> Combined uncertainty <i>u<sub>x</sub>c</i> calculated according to Gr&ouml;ning (2018). Last column provides quality flags as explained in the legend at the bottom.<p>\n');
  fprintf(fid,'<table>');
  if settings.includeE17==1
    fprintf(fid,'<tr><th>%4s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%8s</th></tr>',...
      'Vial','Sample name','Lab ID','&delta;<sup>2</sup>H','&delta;<sup>18</sup>O','&delta;<sup>17</sup>O','dxs','e17','Quality flags');
    fprintf(cid,'%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n','"Vial"','"Sample_name"','"Lab_ID"','"dD"','"dD_SD"','"d18O"','"d18O_SD"','"d17O"','"d17O_SD"','"dxs"','"dxs_SD"','"e17"','"e17_SD"','"Quality_flags"');
  else
    fprintf(fid,'<tr><th>%4s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%8s</th></tr>',...
      'Vial','Sample name','Lab ID','&delta;<sup>2</sup>H','&delta;<sup>18</sup>O','dxs','Quality flags');
    fprintf(cid,'%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n','"Vial"','"Sample_name"','"Lab_ID"','"dD"','"dD_SD"','"d18O"','"d18O_SD"','"dxs"','"dxs_SD"','"Quality_flags"');
  end
  for j = 1:length(driv)
    flags{j}='';
    flagn{j}=0;
    for q = 1:6
      if qflags(driv(j),q)==1
        flags{j}=[flags{j},num2str(q),' '];
        flagn{j}=flagn{j}+2^(q-1);
      end
    end
    if settings.includeE17==1
      fprintf(fid,'<tr><td align=left>%4d</td><td align=left>%20s</td><td align=left>%20s</td><td align=right>%20.2f&plusmn;%20.2f</td><td align=right>%20.4f&plusmn;%20.4f</td><td align=right>%20.4f&plusmn;%20.4f</td><td align=right>%20.2f&plusmn;%20.2f</td><td align=right>%20.1f&plusmn;%20.1f</td><td align=right><i>%s</i></td></tr>\n',...
        driv(j),...
        meas(driv(j)).name1,...
        meas(driv(j)).name2,...
        cal(driv(j)).dD,cal(driv(j)).dD_SD,...
        cal(driv(j)).d18O,cal(driv(j)).d18O_SD,...
        cal(driv(j)).d17O,cal(driv(j)).d17O_SD,...
        cal(driv(j)).dxs,cal(driv(j)).dxs_SD,...
        cal(driv(j)).e17,cal(driv(j)).e17_SD,...
        sprintf('%s (%d)',flags{j},flagn{j}));
      fprintf(cid,'%4d,"%s","%s",%8.2f,%5.2f,%10.4f,%5.4f,%10.4f,%7.4f,%8.2f,%5.2f,%8.1f,%5.1f,%d\n',...
        driv(j),...
        meas(driv(j)).name1,...
        meas(driv(j)).name2,...
        cal(driv(j)).dD,cal(driv(j)).dD_SD,...
        cal(driv(j)).d18O,cal(driv(j)).d18O_SD,...
        cal(driv(j)).d17O,cal(driv(j)).d17O_SD,...
        cal(driv(j)).dxs,cal(driv(j)).dxs_SD,...
        cal(driv(j)).e17,cal(driv(j)).e17_SD,...
        flagn{j});
    else
      fprintf(fid,'<tr><td align=left>%4d</td><td align=left>%20s</td><td align=left>%20s</td><td align=right>%20.1f&plusmn;%20.1f</td><td align=right>%20.2f&plusmn;%20.2f</td><td align=right>%20.1f&plusmn;%20.1f</td><td align=right><i>%s</i></td></tr>\n',...
        driv(j),...
        meas(driv(j)).name1,...
        meas(driv(j)).name2,...
        cal(driv(j)).dD,cal(driv(j)).dD_SD,...
        cal(driv(j)).d18O,cal(driv(j)).d18O_SD,...
        cal(driv(j)).dxs,cal(driv(j)).dxs_SD,...
        sprintf('%s (%d)',flags{j},flagn{j}));
      fprintf(cid,'%4d,"%s","%s",%8.1f,%5.1f,%10.2f,%5.2f,%8.1f,%5.1f,%d\n',...
        driv(j),...
        meas(driv(j)).name1,...
        meas(driv(j)).name2,...
        cal(driv(j)).dD,cal(driv(j)).dD_SD,...
        cal(driv(j)).d18O,cal(driv(j)).d18O_SD,...
        cal(driv(j)).dxs,cal(driv(j)).dxs_SD,...
        flagn{j});
    end
  end
  fclose(cid);
  fprintf(fid,'</table><p>\n');
  fprintf(fid,'<i>Quality flag legend: (Bit 1, value 1): Humidity variation during injections, (Bit 2, value 2): Humidity variation between injections, (Bit 3, value 4): Large isotopic variability, (Bit 4, value 8): Sample outside range of standards, (Bit 5, value 16): Instrument temperature variability, (Bit 6, value 32): Air mode</i><p>\n');
  fprintf(fid,'<a href="%s_standards.csv">Download drift standards data file</a> in CSV (comma separated values) format for import in Excel)\n',settings.runID);
  fprintf(fid,'%s\n',figstr{6}); % Figure 6
  fprintf(fid,'<hr>\n');
  
  % Section 8: Uncertainty budget
  fprintf(fid,'<h3 id="sec8">8. Uncertainty budget</h3><p>');

  if settings.writeReport==1
    fprintf(fid,'<b>Table 4:</b> Uncertainty budget of calibrated measurements per sample. u<sub>x</sub>c: Combined uncertainty (&permil;) and relative contributions u<sub>x</sub>a: assigned uncertainty of depleted standard (&percnt;), u<sub>x</sub>b: measured uncertainty of enriched standard (&percnt;), u<sub>x</sub>c: measured uncertainty of depleted standard (&percnt;), u<sub>x</sub>d: measured uncertainty of depleted standard (&percnt;), u<sub>x</sub>e: sample measurement uncertainty (&percnt;). Components a to e sum up to 100&percnt;.<p>\n');
    fprintf(fid,'<table>');
    if settings.includeE17==1
      fprintf(fid,'<tr><th>%4s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th></tr>',...
        'Vial','Sample name',[config.labname,' ID'],'u<sub>2</sub>c','u<sub>2</sub>a','u<sub>2</sub>b','u<sub>2</sub>c','u<sub>2</sub>d','u<sub>2</sub>e','u<sub>18</sub>c','u<sub>18</sub>a','u<sub>18</sub>b','u<sub>18</sub>c','u<sub>18</sub>d','u<sub>18</sub>e',...
        'u<sub>17</sub>c','u<sub>17</sub>a','u<sub>17</sub>b','u<sub>17</sub>c','u<sub>17</sub>d','u<sub>17</sub>e');
    else
      fprintf(fid,'<tr><th>%4s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th></tr>',...
        'Vial','Sample name',[config.labname,' ID'],'u<sub>2</sub>c','u<sub>2</sub>a','u<sub>2</sub>b','u<sub>2</sub>c','u<sub>2</sub>d','u<sub>2</sub>e','u<sub>18</sub>c','u<sub>18</sub>a','u<sub>18</sub>b','u<sub>18</sub>c','u<sub>18</sub>d','u<sub>18</sub>e');
    end
    if settings.includeE17==1
      fprintf(uid,'%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n','"Vial"','"Sample_name"','"Lab_ID"','"u2_c"','"u2_a"','"u2_b"','"u2_c"','"u2_d"','"u2_e"','"u18_c"','"u18_a"','"u18_b"','"u18_c"','"u18_d"','"u18_e"','"u17_c"','"u17_a"','"u17_b"','"u17_c"','"u17_d"','"u17_e"');
    else
      fprintf(uid,'%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n','"Vial"','"Sample_name"','"Lab_ID"','"u2_c"','"u2_a"','"u2_b"','"u2_c"','"u2_d"','"u2_e"','"u18_c"','"u18_a"','"u18_b"','"u18_c"','"u18_d"','"u18_e"');
    end
    for j = 1:length(meas)
      if ismember(j,[driv,smpv])
        if settings.includeE17==1
          fprintf(fid,'<tr><td align=left>%4d</td><td align=left>%20s</td><td align=left>%20s</td><td align=right>%5.2f</td><td align=right>%4.1f</td><td align=right>%4.1f</td><td align=right>%4.1f</td><td align=right>%4.1f</td><td align=right>%4.1f</td><td align=right>%5.4f</td><td align=right>%4.1f</td><td align=right>%4.1f</td><td align=right>%4.1f</td><td align=right>%4.1f</td><td align=right>%4.1f</td><td align=right>%5.4f</td><td align=right>%4.1f</td><td align=right>%4.1f</td><td align=right>%4.1f</td><td align=right>%4.1f</td><td align=right>%4.1f</td></tr>\n',...
            j,...
            meas(j).name1,...
            meas(j).name2,...
            u(j).t(2),u(j).a(2),u(j).b(2),u(j).c(2),u(j).d(2),u(j).e(2),...
            u(j).t(1),u(j).a(1),u(j).b(1),u(j).c(1),u(j).d(1),u(j).e(1),...
            u(j).t(3),u(j).a(3),u(j).b(3),u(j).c(3),u(j).d(3),u(j).e(3));
        else
          fprintf(fid,'<tr><td align=left>%4d</td><td align=left>%20s</td><td align=left>%20s</td><td align=right>%5.2f</td><td align=right>%4.1f</td><td align=right>%4.1f</td><td align=right>%4.1f</td><td align=right>%4.1f</td><td align=right>%4.1f</td><td align=right>%5.4f</td><td align=right>%4.1f</td><td align=right>%4.1f</td><td align=right>%4.1f</td><td align=right>%4.1f</td><td align=right>%4.1f</td></tr>\n',...
            j,...
            meas(j).name1,...
            meas(j).name2,...
            u(j).t(2),u(j).a(2),u(j).b(2),u(j).c(2),u(j).d(2),u(j).e(2),...
            u(j).t(1),u(j).a(1),u(j).b(1),u(j).c(1),u(j).d(1),u(j).e(1));
        end
        if settings.includeE17==1
          fprintf(uid,'%4d,"%s","%s",%8.2f,%5.1f,%5.1f,%5.1f,%5.1f,%5.1f,%8.4f,%5.1f,%5.1f,%5.1f,%5.1f,%5.1f,%8.4f,%5.1f,%5.1f,%5.1f,%5.1f,%5.1f\n',...
            j,...
            meas(j).name1,...
            meas(j).name2,...
            u(j).t(2),u(j).a(2),u(j).b(2),u(j).c(2),u(j).d(2),u(j).e(2),...
            u(j).t(1),u(j).a(1),u(j).b(1),u(j).c(1),u(j).d(1),u(j).e(1),...
            u(j).t(3),u(j).a(3),u(j).b(3),u(j).c(3),u(j).d(3),u(j).e(3));
        else
          fprintf(uid,'%4d,"%s","%s",%8.2f,%5.1f,%5.1f,%5.1f,%5.1f,%5.1f,%8.4f,%5.1f,%5.1f,%5.1f,%5.1f,%5.1f\n',...
            j,...
            meas(j).name1,...
            meas(j).name2,...
            u(j).t(2),u(j).a(2),u(j).b(2),u(j).c(2),u(j).d(2),u(j).e(2),...
            u(j).t(1),u(j).a(1),u(j).b(1),u(j).c(1),u(j).d(1),u(j).e(1));
        end
      end
    end
    fclose(uid);
    fprintf(fid,'</table><p>\n');
  end
  
  fprintf(fid,'<p><a href="%s_uncertainty.csv">Download uncertainty data file</a> in CSV (comma separated values) format for import in Excel)<br>\n',settings.runID);
  fprintf(fid,'<p><hr>\n');
  
  % Section 9: Preprocessing
  fprintf(fid,'<h3 id="sec9">9. Preprocessing and correction details</h3><p>');
  
  % report excluded vials
  if ~isempty(del_vials)
    for i=1:length(del_vials)
      k=del_vials(i);
      fprintf(fid,'Excluded vial %d (%s) from the analysis, thereby excluding injections %d to %d<br>',k,dvial(i).Identifier1{1},min(dvial(i).Line),max(dvial(i).Line));
    end
  else
    fprintf(fid,'All vials included in the analysis.<p>');
  end
  
  % remove bad injections, i.e. where the sampling interval was more than 15 std above mean interval
  trsh=mean(data.interval,'omitnan')+min(10*std(data.interval,'omitnan'),0.01);
  bad=find(data.interval>trsh);
  fprintf(fid,'There are %d injections identified with relatively low sampling frequency (<%6.4f Hz): %s\n',length(bad),1/trsh,sprintf('%d ',bad));
  
  % report manually excluded injections
  if isfield(settings,'excludeInj')
    fprintf(fid,'Manually excluded injections:<br>\n');
    for v = 1:vials
      excl=settings.excludeInj{v};
      if excl(1)>-1
        fprintf(fid,'Manually excluded %d injections (%s) in vial %d (%s)<br>\n',length(excl),sprintf('%d ',excl),v,vial(v).Identifier1{1});
        dels=vial(v).Line(excl);
        for e=1:length(dels)
          bad(bad==dels(e))=[];
        end
      end
    end
    fprintf(fid,'<p>\n');
  else
    fprintf(fid,'No manually excluded injections<p>\n');
  end
  
  % report sample frequency filtering
  if ~isempty(bad)
    fprintf(fid,'There are %d injections with relatively low sampling frequency remaining after manual removal.<br>\n',length(bad));
    for j=1:length(bad)
      fprintf(fid,'Injection %d (%s), <br>',bad(j),data.Identifier1{bad(j)});
    end
    fprintf(fid,'<p>\n');
  else
    fprintf(fid,'No injections with low sampling frequency remaining after manual removal.<p>\n');
  end
  
  fprintf(fid,'<p>\n');
  fprintf(fid,'<b>Table 5: Isotope dependency correction per sample.</b> Correction for deviation from reference values at 20000 ppm raw measured water concentration in permil and relative to the measured value (in italics).<p>\n');
  fprintf(fid,'<table>');
  if settings.includeE17==1
    fprintf(fid,'<tr><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th></tr>',...
      'Sample name',[config.labname,' ID'],'H2O','&delta;<sup>2</sup>H','% change','&delta;<sup>18</sup>O','% change','&delta;<sup>17</sup>O','% change');
    for j = 1:vials
      fprintf(fid,'<tr><td align=left>%20s</td><td align=left>%20s</td><td align=right>%20.0f&plusmn;%20.0f</td><td align=right>%20.3f&plusmn;%20.3f</td><td align=center><i>%5.3f</i></td><td align=right>%20.3f&plusmn;%20.3f</td><td align=center><i>%5.3f</i></td><td align=right>%20.3f&plusmn;%20.3f</td><td align=center><i>%5.3f</i></td></tr>\n',...
        meas(j).name1,...
        meas(j).name2,...
        mean(vial(j).H2O-20000.0),std(vial(j).H2O-20000.0,'omitnan'),...
        mean(vial(j).dD_corr-vial(j).dD_uc,'omitnan'),std(vial(j).dD_corr-vial(j).dD_uc,'omitnan'),...
        mean(vial(j).dD_corr-vial(j).dD_uc,'omitnan')/mean(vial(j).dD_corr,'omitnan')*100.0,...
        mean(vial(j).d18O_corr-vial(j).d18O_uc,'omitnan'),std(vial(j).d18O_corr-vial(j).d18O_uc,'omitnan'),...
        mean(vial(j).d18O_corr-vial(j).d18O_uc,'omitnan')/mean(vial(j).d18O_corr,'omitnan')*100.0,...
        mean(vial(j).d17O_corr-vial(j).d17O_uc,'omitnan'),std(vial(j).d17O_corr-vial(j).d17O_uc,'omitnan'),...
        mean(vial(j).d17O_corr-vial(j).d17O_uc,'omitnan')/mean(vial(j).d17O_corr,'omitnan')*100.0);
    end
  else
    fprintf(fid,'<tr><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th></tr>',...
      'Sample name',[config.labname,' ID'],'H2O','&delta;<sup>2</sup>H','% change','&delta;<sup>18</sup>O','% change');
    for j = 1:vials
      fprintf(fid,'<tr><td align=left>%20s</td><td align=left>%20s</td><td align=right>%20.0f&plusmn;%20.0f</td><td align=right>%20.3f&plusmn;%20.3f</td><td align=center><i>%5.3f</i></td><td align=right>%20.3f&plusmn;%20.3f</td><td align=center><i>%5.3f</i></td></tr>\n',...
        meas(j).name1,...
        meas(j).name2,...
        mean(vial(j).H2O-20000.0),std(vial(j).H2O-20000.0,'omitnan'),...
        mean(vial(j).dD_corr-vial(j).dD_uc,'omitnan'),std(vial(j).dD_corr-vial(j).dD_uc,'omitnan'),...
        mean(vial(j).dD_corr-vial(j).dD_uc,'omitnan')/mean(vial(j).dD_corr,'omitnan')*100.0,...
        mean(vial(j).d18O_corr-vial(j).d18O_uc,'omitnan'),std(vial(j).d18O_corr-vial(j).d18O_uc,'omitnan'),...
        mean(vial(j).d18O_corr-vial(j).d18O_uc,'omitnan')/mean(vial(j).d18O_corr,'omitnan')*100.0);
    end
  end
  fprintf(fid,'</table><p>\n');
  fprintf(fid,'%s\n',figstr{7}); % Figure 7
  
  fprintf(fid,'<hr>\n');
  
  % Section 9: Instrument stability
  fprintf(fid,'<h3 id="sec10">10. Instrument stability</h3><p>');
  
  fprintf(fid,'<p>\n');
  fprintf(fid,'<b>Table 6: Summary of instrument stability during run.</b> Each line provides averaged quantities with standard deviations for a set of injections from each sample. The error code is taken from the Picarro analyzer output file. ');
  fprintf(fid,'Quality flag legend: (Bit 1, value 1): Humidity variation during injections, (Bit 2, value 2): Humidity variation between injections, (Bit 3, value 4): Large isotopic variability, (Bit 4, value 8): Sample outside range of standards, (Bit 5, value 16): Instrument temperature variability, (Bit 6, value 32): Air mode<p>\n');
  fprintf(fid,'<table>');
  fprintf(fid,'<tr><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th><th>%40s</th></tr>',...
    'Vial','Injections','Name',[config.labname,' ID'],'T<sub>DAS</sub>','H<sub>2</sub>O','H<sub>2</sub>O_SD','baseline_shift','slope_shift','residuals','Flags','ErrorCode');
  for j = 1:vials
    flags{j}='';
    flagn{j}=0;
    for q = 1:6
      if qflags(j,q)==1
        flags{j}=[flags{j},num2str(q),' '];
        flagn{j}=flagn{j}+2^(q-1);
      end
    end
    fprintf(fid,'<tr><td align=left>%2d</td><td align=left>%d-%d</td><td align=left>%20s</td><td align=left>%20s</td><td align=right>%9.2f&plusmn;%6.2f</td><td align=right>%9.2f&plusmn;%6.2f</td><td align=right>%20.2f&plusmn;%20.2f</td><td align=right>%20.2f&plusmn;%20.2f</td><td align=right>%20.2f&plusmn;%20.2f</td><td align=right>%20.2f&plusmn;%20.2f</td><td align=right>%s</td><td align=right>%s</td></tr>\n',...
      j,min(vial(j).Line),max(vial(j).Line),...
      meas(j).name1,...
      meas(j).name2,...
      mean(vial(j).DASTemp,'omitnan'),std(vial(j).DASTemp,'omitnan'),...
      mean(vial(j).H2O,'omitnan'),std(vial(j).H2O,'omitnan'),...
      mean(vial(j).H2O_SD,'omitnan'),std(vial(j).H2O_SD,'omitnan'),...
      mean(vial(j).baseline_shift,'omitnan'),std(vial(j).baseline_shift,'omitnan'),...
      mean(vial(j).slope_shift,'omitnan'),std(vial(j).slope_shift,'omitnan'),...
      mean(vial(j).residuals,'omitnan'),std(vial(j).residuals,'omitnan'),...
      sprintf('%s (%d)',flags{j},flagn{j}),...
      strrep(num2str(vial(j).ErrorCode'),' ',''));
  end
  fprintf(fid,'</table><p>\n');
  
  fprintf(fid,'%s\n',figstr{8}); % Figure 8
  fprintf(fid,'<hr>\n');
  
  % Appendix
  fprintf(fid,'<h3 id="sec11">Appendix: Raw data for all vials</h3><br>\n');
  fprintf(fid,'<b>Figure panel description:</b> From left to right, panels depict the raw measurement data for &delta;<sup>18</sup>O, &delta;<sup>2</sup>H, &delta;<sup>17</sup>O, and the water concentration from all injections with their standard deviation. <span style="color:#ff0000">Red marker color</span> denotes injections that have been excluded from the analysis, <span style="color:#0000ff">blue markers</span> denote injections included in the calculation of the sample mean value and standard deviation, and <span style="color:#007700">green markers</span> denote injections after memory correction (if applicable).<p>');
  
  for j=1:vials
    fprintf(fid,'%s\n',figstr{j+10}); % Figure 11 and onwards
  end
  
end % if writeReport

% close HTML file
fprintf(fid,'<hr>\n');
fprintf(fid,'<p>Report created on %s using FLIIMP %s',datestr(settings.reportDate),config.version);
fprintf(fid,'<br>Measurement laboratory: %s\n',config.reference);
fprintf(fid,'</body></html>\n');
fclose(fid);
logf(handles,'Report written to %s/index.html',rdir);

% TODO: write .htaccess and .htpasswd file

end
% fin
