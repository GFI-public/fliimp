% -----------------------------------------------------------------------------------
% last changes: HS, 20.10.2017
% FLIIMP Version 1.0
% -----------------------------------------------------------------------------------
function varargout = FLIIMP_form_preproc(varargin)
% FLIIMP_FORM_PREPROC MATLAB code for FLIIMP_form_preproc.fig
%      FLIIMP_FORM_PREPROC, by itself, creates a new FLIIMP_FORM_PREPROC or raises the existing
%      singleton*.
%
%      H = FLIIMP_FORM_PREPROC returns the handle to a new FLIIMP_FORM_PREPROC or the handle to
%      the existing singleton*.
%
%      FLIIMP_FORM_PREPROC('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FLIIMP_FORM_PREPROC.M with the given input arguments.
%
%      FLIIMP_FORM_PREPROC('Property','Value',...) creates a new FLIIMP_FORM_PREPROC or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before FLIIMP_form_preproc_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to FLIIMP_form_preproc_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help FLIIMP_form_preproc

% Last Modified by GUIDE v2.5 05-Dec-2023 14:33:32

global config
config=FLIIMP_config;

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @FLIIMP_form_preproc_OpeningFcn, ...
                   'gui_OutputFcn',  @FLIIMP_form_preproc_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% helper function validateSample(vial)
% return color coding for valid/warming sample
function col=validateSample(vial)
    global config
    col='#000000'; % default black
    if max(vial.InjNr)<3
      col='#CC5500';
    end
    if vial.mH2O<config.H2O_min || vial.mH2O>config.H2O_max
      col='#CC5500';
    end
    if vial.sH2O>config.H2O_inj_SD
      col='#CC5500';
    end
    if max(isnan(vial.d18O),isnan(vial.dD))>0
      col='#CC5500';
    end
    if std(vial.d18O)>config.d18O_SD
      col='#CC5500';
    end
    if std(vial.dD)>config.dD_SD
      col='#CC5500';
    end


% --- Executes just before FLIIMP_form_preproc is made visible.
function FLIIMP_form_preproc_OpeningFcn(hObject, ~, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to FLIIMP_form_preproc (see VARARGIN)

% apply position information
if isfield(varargin{1},'position')
  if length(varargin{1}.position)>1
    set(hObject,'position',varargin{1}.position{2});
  end
  handles.position=varargin{1}.position;
end

% Choose default command line output for FLIIMP_form_preproc
handles.output = hObject;
handles.logfigure = varargin{1}.logfigure;
handles.logtext = varargin{1}.logtext;

set(hObject,'Name','FLIIMP - Step 2');

if ~isempty(varargin)
  % copy settings if applicable
  if isfield(varargin{1},'settings')
    handles.settings=varargin{1}.settings;
    handles.inputPath=varargin{1}.inputPath;
    handles.outputPath=varargin{1}.outputPath;
  end
end

% Update handles structure
guidata(hObject, handles);

% load the data file
settings=handles.settings;

% default is humidity correction off
% unified parameter is now useHumidityCorrection
if ~isfield(settings,'useHumCorr') && ~isfield(settings,'useHumidityCorrection')
  settings.useHumidityCorrection=0; 
else
  settings.useHumidityCorrection=1;
end
set(handles.checkboxHum,'Value',settings.useHumidityCorrection);

% create intermediate structure with paths
lsettings=settings;
lsettings.inputPath=handles.inputPath;
lsettings.outputPath=handles.outputPath;

% read in the files which is pointed at
data=FLIIMP_read_liquid_injections(handles,lsettings);

if isempty(data)
  logf(handles,'ERROR: No data found at given date and location. Returning to input window');
  FLIIMP;
  return
end

% create mem structure if it does not yet exist
if ~isfield(settings,'mem')
  % set the default fitting parameters
  settings.mem.alpha(1:3)=1.1;
  settings.mem.beta(1:3)=0.4;
  settings.mem.b(1:3)=0.83;
  settings.mem.c0(1:3)=4;
  settings.mem.rmse(1:3)=0;
  settings.mem.minInj(1:3)=10;
  settings.mem.avgInj=3;
  settings.mem.enableCorrection(1:3)=0;
  settings.mem.standards='';
else
  if ~isfield(handles.settings.mem,'minInj')
    settings.mem.minInj(1:3)=10;
  end
  if ~isfield(handles.settings.mem,'avgInj')
    settings.mem.avgInj=3;
  end
  if ~isfield(handles.settings.mem,'standards')
    settings.mem.standards='';
  end
end

settings.useHumidityCorrection=get(handles.checkboxHum,'Value');
handles.wdata=FARLAB_wconc_corr(data,settings.instrument,settings.useHumidityCorrection);
if ~isfield(settings,'excludeInj')
  settings.excludeInj={[-1]};
end
data=FLIIMP_memory_analysis(handles.wdata,settings.excludeInj,settings.mem.avgInj);

% update GUI data
handles.settings=settings;
guidata(hObject, handles);

% update the selection options according to 17O availability
if isfield(data,'e17')==1
  titles1={'d18O (raw)';'dD (raw)';'d17O (raw)';'d-excess (raw)';'e17 (raw)'};
  titles2={'H2O (ppmv)';'d18O mem';'dD mem';'d17O mem';'dxs mem';'e17 mem';'H2O slope';'d18O slope';'dD slope'};
  set(handles.parameterPopup,'String',titles1);
  set(handles.parameterPopup2,'String',titles2)
  handles.settings.includeE17=1;
else
  titles1={'d18O (raw)';'dD (raw)';'d-excess (raw)'};
  titles2={'H2O (ppmv)';'d18O mem';'dD mem';'dxs mem';'H2O slope';'d18O slope';'dD slope'};
  set(handles.parameterPopup,'String',titles1);    
  set(handles.parameterPopup2,'String',titles2);
  handles.settings.includeE17=0;
end

if isempty(data)
  logf(handles,'ERROR: No data found at given date and location. Returning to input window');
  FLIIMP;
  return
end

% split vials
vials = sum(diff(data.InjNr)<1)+1;
vialstart = [1;find(diff(data.InjNr)<1)+1;length(data.InjNr)+1];
f = fieldnames(data);
for i = 1:vials
  for k = 1:length(f)
    vial(i).(f{k}) = data.(f{k})(vialstart(i):vialstart(i+1)-1);
  end
  vial(i).mH2O=mean(vial(i).H2O,'omitnan');
  vial(i).sH2O=std(vial(i).H2O,'omitnan');
end
handles.vial=vial;

% find the mean and std of the measuring interval
handles.iavg=mean(data.interval.^(-1),'omitnan');
handles.istd=std(data.interval.^(-1),'omitnan');

% find max injections
injs=[];
for i = 1:vials
  injs=[injs, max(vial(i).InjNr)];
end

% report number of vials, injections
logf(handles,'Found %d vials with max. injections: %s.',vials,num2str(unique(injs)));
for i=1:vials
  nnan=max(sum(isnan(vial(i).d18O)),sum(isnan(vial(i).dD)));
  if nnan>0
    logf(handles,'WARNING: Vial %d: found %d NaN injections, consider excluding vial/injections.',i,nnan);
  end
  ninj=min(length(vial(i).d18O),length(vial(i).dD));
  if ninj<4
    logf(handles,'WARNING: Vial %d: found only %d injections, consider excluding vial/injections.',i,ninj);
  end
end

% exclude individual injections
if ~isfield(settings,'excludeInj')
  settings.excludeInj=cell(1,vials);
  for i = 1:vials
    settings.excludeInj{i}=[-1];
  end
elseif length(settings.excludeInj)<vials
  for i = 1:vials
    settings.excludeInj{i}=[-1];
  end
end

% build a selection list
if ~isfield(settings,'ignoreInjections')
  settings.ignoreInjections=zeros(vials,1);
end
settings.ignoreVials=find(settings.ignoreInjections==1);
if ~isfield(settings,'ignoreInjectionsCal')
  if isfield(settings,'mem')
    % before V1.9, recalculate c0 to account for removal of injection offset
    % from earlier versions of FLIIMP
    for i=1:3
      alpha=handles.settings.mem.alpha(i);
      beta=handles.settings.mem.beta(i);
      b=handles.settings.mem.b(i);
      c0=handles.settings.mem.c0(i);
      
      % calculate for large number of injections
      settings.mem.c0(i)=c0*(b*exp(-alpha)+(1-b)*exp(-beta));
    end
  end
  settings.ignoreInjectionsCal=settings.ignoreInjections;
end
settings.ignoreVialsCal=find(settings.ignoreInjectionsCal==1);

handles.tableEntry=cell(vials,1);
% adjust if length of vials has changed
if length(settings.ignoreInjections)>vials
  settings.ignoreInjections=settings.ignoreInjections(1:vials);
elseif length(settings.ignoreInjections)<vials
  settings.ignoreInjections=zeros(vials,1);
end
for i=1:vials
  if settings.ignoreInjections(i)==1
    check='C';
  else 
    check=' ';
  end
  if settings.ignoreInjectionsCal(i)==1
    checkCal='M';
  else 
    checkCal=' ';
  end
  col=validateSample(handles.vial(i));
  handles.tableEntry{i}=sprintf('<HTML><BODY color="%s"><PRE>%c %c %3d %2d %8s %6.0f %4.0f</PRE></BODY></HTML>',col,check,checkCal,i,max(handles.vial(i).InjNr),handles.vial(i).Identifier1{1},handles.vial(i).mH2O,handles.vial(i).sH2O);
end
set(handles.sampleList,'String',handles.tableEntry);
handles.settings=settings;
guidata(hObject,handles);
set(handles.sampleList,'Value',1);
sampleList_Callback(handles.sampleList,0,handles);

% UIWAIT makes FLIIMP_form_preproc wait for user response (see UIRESUME)
% uiwait(handles.FLIIMP_FLIIMP_form_preproc);

function updateInjectionList(hObject,handles,i)
if handles.settings.ignoreInjections(i)==1
  check='C';
else 
  check=' ';
end
if handles.settings.ignoreInjectionsCal(i)==1
  checkCal='M';
else 
  checkCal=' ';
end
col=validateSample(handles.vial(i));
handles.tableEntry{i}=sprintf('<HTML><BODY color="%s"><PRE>%c %c %3d %2d %8s %6.0f %4.0f</PRE></BODY></HTML>',col,check,checkCal,i,max(handles.vial(i).InjNr),handles.vial(i).Identifier1{1},handles.vial(i).mH2O,handles.vial(i).sH2O);
guidata(hObject,handles);
set(handles.sampleList,'String',handles.tableEntry);


% --- Outputs from this function are returned to the command line.
function varargout = FLIIMP_form_preproc_OutputFcn(hObject, ~, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in parameterPopup.
function parameterPopup_Callback(hObject, ~, handles)
% hObject    handle to parameterPopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns parameterPopup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from parameterPopup
% refresh plot
sampleList_Callback(handles.sampleList,0,handles);


% --- Executes during object creation, after setting all properties.
function parameterPopup_CreateFcn(hObject, ~, handles)
% hObject    handle to parameterPopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in sampleList.
function sampleList_Callback(hObject, ~, handles)
% hObject    handle to sampleList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns sampleList contents as cell array
%        contents{get(hObject,'Value')} returns selected item from sampleList
global config
idx=get(hObject,'Value');

if isfield(handles.settings,'includeE17') % to capture problem during UI buildup
  if handles.settings.includeE17==1
    ytitles1={'H2O / ppm','d18O mem / %','dD mem / %','d17O mem / %','d-excess mem / %','17O-excess mem / %','H2O slope / ppm','d18O slope / permil','dD slope / permil'};
    ytitles2={'d18O / permil','dD / permil','d17O / permil','d-excess / permil','17O-excess / permeg'};
    variables1={'H2O','d18O_mem','dD_mem','d17O_mem','dxs_mem','e17_mem','H2O_Sl','d18O_Sl','dD_Sl'};
    variables2={'d18O','dD','d17O','dxs','e17'};
  else
    ytitles1={'H2O / ppm','d18O mem / %','dD mem / %','d-excess mem / %','H2O slope / ppm','d18O slope / permil','dD slope / permil'};
    ytitles2={'d18O / permil','dD / permil','d-excess / permil'};
    variables1={'H2O','d18O_mem','dD_mem','dxs_mem','H2O_Sl','d18O_Sl','dD_Sl'};
    variables2={'d18O','dD','dxs','d17O','e17'};
  end
else 
  ytitles1={'H2O / ppm','d18O mem / %','dD mem / %','d-excess mem / %','H2O slope / ppm','d18O slope / permil','dD slope / permil'};
  ytitles2={'d18O / permil','dD / permil','d-excess / permil'};
  variables1={'H2O','d18O_mem','dD_mem','dxs_mem','H2O_Sl','d18O_Sl','dD_Sl'};
  variables2={'d18O','dD','dxs','d17O','e17'};
end

excl=-1;

if isfield(handles.settings,'excludeInj')
  try
    set(handles.excludeInjText,'String',sprintf('%d ',handles.settings.excludeInj{idx}));
    excl=handles.settings.excludeInj{idx};
  catch
    set(handles.excludeInjText,'String','');
  end
else
  handles.settings.excludeInj={};
end

pa(1)=handles.plotAxes;
pa(2)=handles.memAxes;

for n=1:2

    % always plot H2O or memory estimate in first plot
    % second plot based on selection
    if n==1
      pidx=1;
    else
      pidx=get(handles.parameterPopup2,'Value');
    end
    var=variables1{pidx};
    varsd=[variables1{pidx},'_SD'];

    if contains(var,'_Sl') % no standard deviation available, set to zero
      for i=1:length(handles.vial)
        handles.vial(i).(varsd)=zeros(size(handles.vial(i).(var)));
      end
    end

    % plotting
    cla(pa(n));
    hold(pa(n),'on');
    if pidx==1
      errorbar(pa(n),handles.vial(idx).InjNr,handles.vial(idx).(var),handles.vial(idx).(varsd),'kx');
      % show injections with outside range humidity
      hrange=find(handles.vial(idx).H2O>config.H2O_max | handles.vial(idx).H2O<config.H2O_min);
      if hrange>0 
        plot(pa(n),handles.vial(idx).InjNr(hrange),handles.vial(idx).(var)(hrange),'mx');
      end
      % show excluded injections
      if excl>0
        errorbar(pa(n),handles.vial(idx).InjNr(excl),handles.vial(idx).(var)(excl),handles.vial(idx).(varsd)(excl),'rx');
      end
    else % plot memory without SD information
      plot(pa(n),handles.vial(idx).InjNr,handles.vial(idx).(var),'kx');
      if excl>0
        plot(pa(n),handles.vial(idx).InjNr(excl),handles.vial(idx).(var)(excl),'rx');
      end
    end
    %set(pa(n),'XLim',[min(handles.vial(idx).Line)-1 max(handles.vial(idx).Line)+1]);
    set(pa(n),'XLim',[0 max(handles.vial(idx).InjNr)+1]);
    set(pa(n),'YAxisLocation','right');
    ylabel(pa(n),ytitles1{pidx});
    xlabel(pa(n),'Injection nr.');
    box(pa(n),'on');
end

ia(1)=handles.isoAxes;
ia(2)=handles.isoAxes2;

for n=1:2

    if n==1
      % on axis 1, always plot O18
      pidx=1;
    else
      % select between isotope parameters on panel 2
      pidx=get(handles.parameterPopup,'Value');
    end
    var=variables2{pidx};
    varsd=[variables2{pidx},'_SD'];

    % plotting
    cla(ia(n));
    hold(ia(n),'on');
    % original injections
    errorbar(ia(n),handles.vial(idx).InjNr,handles.vial(idx).(var),handles.vial(idx).(varsd),'kx');
    useHumidityCorrection=get(handles.checkboxHum,'Value');
    if useHumidityCorrection==1
      % show humidity corrected values
      plot(ia(n),handles.vial(idx).InjNr,handles.vial(idx).([var,'_corr']),'b.');
    end

    % find the max number of injections per sequential sample
    maxInjNr=0;
    ends=3;
    ends1=3;
    for v=1:length(handles.vial)
      if ~ismember(v,handles.settings.ignoreVials)
        handles.vial(v).maxInjNr=max(handles.vial(v).InjNr);
        maxInjNr=max(maxInjNr,handles.vial(v).maxInjNr);

        % assign specific ends value to each vial
        if v==1
          if ends1>0
            handles.vial(v).ends=ends1;
          else
            handles.vial(v).ends=length(handles.vial(v).InjNr)-1;
          end
        else
          if ends>0
            handles.vial(v).ends=ends;
          else
            handles.vial(v).ends=length(handles.vial(v).InjNr)-1;
          end
        end
      end

    end

    if pidx<4 % only for main isotopes

      if isfield(handles.settings,'mem')
        % correct for memory effect
        clear c;
        c=zeros(maxInjNr,3);
        alpha=handles.settings.mem.alpha(pidx);
        beta=handles.settings.mem.beta(pidx);
        b=handles.settings.mem.b(pidx);
        c0=handles.settings.mem.c0(pidx);
        
        % calculate for large number of injections
        for i=1:maxInjNr
          c(i,pidx)=c0*(b*exp(-alpha*(i-1))+(1-b)*exp(-beta*(i-1)));
          if i>(maxInjNr-1)
            c(i,pidx)=0;
          end
        end
       for i=1:(maxInjNr-1)
         c(i,pidx)=c(i,pidx)-c(maxInjNr-1,pidx)*(1-(maxInjNr-i)/(maxInjNr-1));
       end

        % initialize to original value
        handles.vial(idx).([var,'_mem'])=handles.vial(idx).([var,'_corr']);
        % calculate memory correction with current settings
        if idx>1
            %if ~ismember(idx,handles.settings.ignoreVials)
                nmax=length(handles.vial(idx-1).InjNr);
                for i=1:length(handles.vial(idx).InjNr)
                    mf=1-c(i,pidx)/100.0;                    
                    lastmean=mean(handles.vial(idx-1).([var,'_corr'])(max(1,nmax-handles.settings.mem.avgInj):nmax),'omitnan');
                    if ~isnan(lastmean)
                        handles.vial(idx).([var,'_mem'])(i)=(handles.vial(idx).([var,'_corr'])(i)-(1-mf)*lastmean)/mf;
                        %[handles.vial(idx).([var,'_corr'])(i) handles.vial(idx).([var,'_mem'])(i)]
                    end
                end
            %else
            %    handles.vial(idx).([var,'_mem'])(:)=NaN;
            %end
        end

        % show memory-corrected injections
        if handles.settings.mem.enableCorrection(pidx)>0
          % plot in dark green
          errorbar(ia(n),handles.vial(idx).InjNr,handles.vial(idx).([var,'_mem']),handles.vial(idx).(varsd),'o','Color',[0.0 0.6 0.0]);
        end

        vs=handles.vial(idx).([var,'_mem']);
        if excl>0
          vs(excl)=NaN;
        end
        md=mean(vs,'omitnan');
        sd=std(vs,'omitnan');
        % overlay 
        plot(ia(n),[0.5 max(handles.vial(idx).InjNr)+0.5],[md md],'-','Color',[0.0 1.0 0.0]);
        plot(ia(n),[0.5 max(handles.vial(idx).InjNr)+0.5],[md+sd md+sd],'--','Color',[0.0 1.0 0.0]);
        plot(ia(n),[0.5 max(handles.vial(idx).InjNr)+0.5],[md-sd md-sd],'--','Color',[0.0 1.0 0.0]);
        plot(ia(n),[0.5 max(handles.vial(idx).InjNr)+0.5],[md+2*sd md+2*sd],':','Color',[0.0 1.0 0.0]);
        plot(ia(n),[0.5 max(handles.vial(idx).InjNr)+0.5],[md-2*sd md-2*sd],':','Color',[0.0 1.0 0.0]);
        % add average and std as text information
        fprintf('%s: %5.2f±%5.2f\n',var,md,sd)
        text(ia(n),0.5,md+sd,sprintf('%5.2f±%5.2f',md,sd));
        % overlay long-term reproducibility of lab
        sd=config.longTermReproducibility(n);
        errorbar(ia(n),max(handles.vial(idx).InjNr)+0.5,md,sd,sd,'-','Color',[0.7 0.2 0.0],'LineWidth',2);
        %plot(ia(n),[0 max(handles.vial(idx).InjNr)+1],[md-sd md-sd],':','Color',[0.7 0.2 0.0]);

      end
    end

    % show injections with outside range humidity
    hrange=find(handles.vial(idx).H2O>config.H2O_max | handles.vial(idx).H2O<config.H2O_min);
    if hrange>0 
      plot(ia(n),handles.vial(idx).InjNr(hrange),handles.vial(idx).(var)(hrange),'mx');
    end

    % show excluded injections
    if excl>0
      errorbar(ia(n),handles.vial(idx).InjNr(excl),handles.vial(idx).(var)(excl),handles.vial(idx).(varsd)(excl),'rx');
    end

    %set(ia(n),'XLim',[min(handles.vial(idx).Line)-1 max(handles.vial(idx).Line)+1]);
    set(ia(n),'XLim',[0 max(handles.vial(idx).InjNr)+1]);
    % chose ylim for non-excluded samples
    ee=1:length(handles.vial(idx).InjNr);
    if excl>0
      ee(excl)=[];
    end
    ylim1=[min(handles.vial(idx).(var)(ee)-handles.vial(idx).(varsd)(ee)*1.5) max(handles.vial(idx).(var)(ee)+handles.vial(idx).(varsd)(ee)*1.5)];
    if isfield(handles.settings,'mem')
      ylim2=[min(handles.vial(idx).([var,'_mem'])(ee)-handles.vial(idx).(varsd)(ee)*1.5) max(handles.vial(idx).([var,'_mem'])(ee)+handles.vial(idx).(varsd)(ee)*1.5)];
    else
      ylim2=ylim1;
    end
    if isnan(ylim1(1)) && ~isnan(ylim2(1))
      ylim1=ylim2;
    elseif isnan(ylim2(1)) && ~isnan(ylim1(1))
      ylim2=ylim1;
    elseif isnan(ylim2(1)) && isnan(ylim2(1))
      ylim1=[-1 1];
      ylim2=[-1 1];
    end
    ylim(ia(n),[min(ylim1(1),ylim2(1)),max(ylim1(2),ylim2(2))]);
    set(ia(n),'YAxisLocation','right');
    ylabel(ia(n),ytitles2{pidx});
    xlabel(ia(n),'Injection nr.');
    box(ia(n),'on');
    set(handles.includeCheckbox,'Value',handles.settings.ignoreInjections(idx));
    set(handles.includeCheckboxCal,'Value',handles.settings.ignoreInjectionsCal(idx));
end

% --- Executes during object creation, after setting all properties.
function sampleList_CreateFcn(hObject, ~, handles)
% hObject    handle to sampleList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in checkButton.
function checkButton_Callback(hObject, ~, handles)
% hObject    handle to checkButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% rebuild a selection list
handles.settings.ignoreInjections=ones(length(handles.vial),1);
for i=1:length(handles.vial)
  if handles.settings.ignoreInjections(i)==1
    check='C';
  else 
    check=' ';
  end
  if handles.settings.ignoreInjectionsCal(i)==1
    checkCal='M';
  else 
    checkCal=' ';
  end
  col=validateSample(handles.vial(i));
  handles.tableEntry{i}=sprintf('<HTML><BODY color="%s"><PRE>%c %c %3d %2d %8s %6.0f %4.0f</PRE></BODY></HTML>',col,check,checkCal,i,max(handles.vial(i).InjNr),handles.vial(i).Identifier1{1},handles.vial(i).mH2O,handles.vial(i).sH2O);
end
set(handles.sampleList,'String',handles.tableEntry);
guidata(hObject,handles);
sampleList_Callback(hObject, 0, handles);


% --- Executes on button press in uncheckButton.
function uncheckButton_Callback(hObject, ~, handles)
% hObject    handle to uncheckButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.settings.ignoreInjections=zeros(length(handles.vial),1);
for i=1:length(handles.vial)
  if handles.settings.ignoreInjections(i)==1
    check='C';
  else 
    check=' ';
  end
  if handles.settings.ignoreInjectionsCal(i)==1
    checkCal='M';
  else 
    checkCal=' ';
  end
  col=validateSample(handles.vial(i));
  handles.tableEntry{i}=sprintf('<HTML><BODY color="%s"><PRE>%c %c %3d %2d %8s %6.0f %4.0f</PRE></BODY></HTML>',col,check,checkCal,i,max(handles.vial(i).InjNr),handles.vial(i).Identifier1{1},handles.vial(i).mH2O,handles.vial(i).sH2O);
end
set(handles.sampleList,'String',handles.tableEntry);
guidata(hObject,handles);
sampleList_Callback(hObject, 0, handles);


% --- Executes on button press in backButton.
function backButton_Callback(hObject, ~, handles)
% hObject    handle to backButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.settings.ignoreVials=find(handles.settings.ignoreInjections==1);
handles.settings.ignoreVialsCal=find(handles.settings.ignoreInjectionsCal==1);
handles.position{2}=get(handles.output,'Position');
%set(gcf,'Visible','Off');
close(gcf);
FLIIMP(handles);


% --- Executes on button press in nextButton.
function nextButton_Callback(hObject, ~, handles)
% hObject    handle to nextButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.position{2}=get(handles.output,'Position');
handles.settings.ignoreVials=find(handles.settings.ignoreInjections==1);
handles.settings.ignoreVialsCal=find(handles.settings.ignoreInjectionsCal==1);
%set(gcf,'Visible','Off');
close(gcf);
FLIIMP_form_rename(handles);


% --- Executes on button press in saveButton.
function saveButton_Callback(hObject, ~, handles)
% hObject    handle to saveButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles=guidata(hObject);
settings=handles.settings;
[newfile newpath]=uiputfile({'*.mat','MATLAB data file'},'Save run settings to...',[handles.outputPath,'/',settings.filename]);
if ~isequal(newpath,0) && ~isequal(newfile,0)
  save(fullfile(newpath,newfile),'settings');
end


% --- Executes on button press in loadButton.
function loadButton_Callback(hObject, eventdata, handles)
% hObject    handle to loadButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[newfile newpath]=uigetfile({'*.mat','MATLAB data file'},'Load run settings from...',handles.outputPath);
% save log file during loading
logfile=handles.settings.logfile;
if ~isequal(newpath,0) && ~isequal(newfile,0)
  load(fullfile(newpath,newfile));
  settings.lid=handles.settings.lid;
  settings.logfile=logfile;
  settings.filename=newfile;
else 
  return
end
handles.settings=settings;
settings.rename
guidata(hObject,handles);

% --- Executes on button press in includeCheckbox.
function includeCheckbox_Callback(hObject, eventdata, handles)
% hObject    handle to includeCheckbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of includeCheckbox
handles.settings.ignoreInjections(get(handles.sampleList,'Value'))=get(hObject,'Value');
guidata(hObject,handles);
updateInjectionList(hObject,handles,get(handles.sampleList,'Value'));

function excludeInjText_Callback(hObject, eventdata, handles)
% hObject    handle to excludeInjText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of excludeInjText as text
%        str2double(get(hObject,'String')) returns contents of excludeInjText as a double
if ~isfield(handles.settings,'excludeInj')
  handles.settings.excludeInj={};
end
try 
  %disp(['[',get(hObject,'String'),']'])
  ei=sort(unique(eval(['[',get(hObject,'String'),']'])));
  handles.settings.excludeInj{get(handles.sampleList,'Value')}=ei(:)';

  % redo memory analysis
  data=FLIIMP_memory_analysis(handles.wdata,handles.settings.excludeInj,handles.settings.mem.avgInj);
  % split vials
  vials = sum(diff(data.InjNr)<1)+1;
  vialstart = [1;find(diff(data.InjNr)<1)+1;length(data.InjNr)+1];
  f = fieldnames(data);
  for i = 1:vials
    for k = 1:length(f)
      vial(i).(f{k}) = data.(f{k})(vialstart(i):vialstart(i+1)-1);
    end
    vial(i).mH2O=mean(vial(i).H2O,'omitnan');
    vial(i).sH2O=std(vial(i).H2O,'omitnan');
  end
  handles.vial=vial;
  guidata(hObject,handles);
  updateInjectionList(hObject,handles,get(handles.sampleList,'Value'));
  sampleList_Callback(handles.sampleList,0,handles);
catch
  logf(handles.settings.lid,'Error in excluded injections, use index nr seperated by space');
  handles.settings
end

% --- Executes during object creation, after setting all properties.
function excludeInjText_CreateFcn(hObject, eventdata, handles)
% hObject    handle to excludeInjText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over checkButton.
function checkButton_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to checkButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in detectBads.
function detectBads_Callback(hObject, eventdata, handles)
% hObject    handle to detectBads (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% remove bad injections, i.e. where the sampling frequency dropped to below 1.1 Hz (interval > 0.9)
snb=0;
logf(handles,'Average sampling frequency: %6.4f (std: %6.4f) Hz',handles.iavg,handles.istd);
trsh=max(1.1,handles.iavg+min(10*handles.istd,0.1));
for i=1:length(handles.vial)
  nb=sum(handles.vial(i).interval>trsh);
  if(nb>0)
    logf(handles,'Vial %d (%s): %d bad injections detected.',i,handles.vial(i).Identifier1{1},nb);
    snb=snb+nb;
    bi=find(handles.vial(i).interval>trsh);
    if handles.settings.excludeInj{i}(1)==-1
      handles.settings.excludeInj{i}=bi(:)';
    else
      handles.settings.excludeInj{i}=(unique(sort([handles.settings.excludeInj{i}(:)',bi(:)'])));
    end
  end
end
logf(handles,'In total %d injections with low measurement frequency (<%6.4f Hz ) detected, please inspect results manually.',snb,1/trsh);
guidata(hObject,handles);
sampleList_Callback(handles.sampleList,0,handles);


% --- Executes on selection change in parameterPopup2.
function parameterPopup2_Callback(hObject, eventdata, handles)
% hObject    handle to parameterPopup2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns parameterPopup2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from parameterPopup2
sampleList_Callback(handles.sampleList,0,handles);


% --- Executes during object creation, after setting all properties.
function parameterPopup2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to parameterPopup2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in MemoryButton.
function MemoryButton_Callback(hObject, eventdata, handles)
% hObject    handle to MemoryButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.position{2}=get(handles.output,'Position');
handles.settings.ignoreVials=find(handles.settings.ignoreInjections==1);
handles.settings.ignoreVialsCal=find(handles.settings.ignoreInjectionsCal==1);
set(gcf,'Visible','Off');
FLIIMP_form_mem(handles);


% --------------------------------------------------------------------
function FileMenu_Callback(hObject, eventdata, handles)
% hObject    handle to FileMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in includeCheckboxCal.
function includeCheckboxCal_Callback(hObject, eventdata, handles)
% hObject    handle to includeCheckboxCal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of includeCheckboxCal
handles.settings.ignoreInjectionsCal(get(handles.sampleList,'Value'))=get(hObject,'Value');
guidata(hObject,handles);
updateInjectionList(hObject,handles,get(handles.sampleList,'Value'));

% --- Executes on button press in checkButtonCal.
function checkButtonCal_Callback(hObject, eventdata, handles)
% hObject    handle to checkButtonCal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.settings.ignoreInjectionsCal=ones(length(handles.vial),1);
for i=1:length(handles.vial)
  if handles.settings.ignoreInjections(i)==1
    check='C';
  else 
    check=' ';
  end
  if handles.settings.ignoreInjectionsCal(i)==1
    checkCal='M';
  else 
    checkCal=' ';
  end
  col=validateSample(handles.vial(i));
  handles.tableEntry{i}=sprintf('<HTML><BODY color="%s"><PRE>%c %c %3d %2d %8s %6.0f %4.0f</PRE></BODY></HTML>',col,check,checkCal,i,max(handles.vial(i).InjNr),handles.vial(i).Identifier1{1},handles.vial(i).mH2O,handles.vial(i).sH2O);
end
set(handles.sampleList,'String',handles.tableEntry);
guidata(hObject,handles);
sampleList_Callback(hObject, 0, handles);

% --- Executes on button press in uncheckButtonCal.
function uncheckButtonCal_Callback(hObject, eventdata, handles)
% hObject    handle to uncheckButtonCal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.settings.ignoreInjections=zeros(length(handles.vial),1);
for i=1:length(handles.vial)
  if handles.settings.ignoreInjections(i)==1
    check='C';
  else 
    check=' ';
  end
  if handles.settings.ignoreInjectionsCal(i)==1
    checkCal='M';
  else 
    checkCal=' ';
  end
  col=validateSample(handles.vial(i));
  handles.tableEntry{i}=sprintf('<HTML><BODY color="%s"><PRE>%c %c %3d %2d %8s %6.0f %4.0f</PRE></BODY></HTML>',col,check,checkCal,i,max(handles.vial(i).InjNr),handles.vial(i).Identifier1{1},handles.vial(i).mH2O,handles.vial(i).sH2O);
end
set(handles.sampleList,'String',handles.tableEntry);
guidata(hObject,handles);
sampleList_Callback(hObject, 0, handles);

% --------------------------------------------------------------------
function LoadMenu_Callback(hObject, eventdata, handles)
% hObject    handle to LoadMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
loadButton_Callback(hObject, eventdata, handles);

% --------------------------------------------------------------------
function SaveMenu_Callback(hObject, eventdata, handles)
% hObject    handle to SaveMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
saveButton_Callback(hObject, eventdata, handles);

% --------------------------------------------------------------------
function BackMenu_Callback(hObject, eventdata, handles)
% hObject    handle to BackMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
backButton_Callback(hObject, eventdata, handles);

% --------------------------------------------------------------------
function NextMenu_Callback(hObject, eventdata, handles)
% hObject    handle to NextMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
nextButton_Callback(hObject, eventdata, handles);

% --------------------------------------------------------------------
function QuitMenu_Callback(hObject, eventdata, handles)
% hObject    handle to QuitMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% confirm quit

global config;

%selection = questdlg('OK to quit FLIIMP?','Quit FLIIMP','Quit','Cancel','Cancel');
%if strcmp(selection,'Quit')==0
%    return;
%end

% store current position
handles.position{2}=get(handles.output,'Position');

% save the file path options
clear paths
paths.inputPath=handles.inputPath;
paths.outputPath=handles.outputPath;
paths.positions=handles.position;
logf(handles,['Saving path settings to ',config.homeDirectory,'/FLIIMP_settings.mat']);
save(sprintf('%s/FLIIMP_settings.mat',config.homeDirectory),'paths');
logf(handles,'FLIIMP session ended on %s',datestr(now));

%f=get(0,'Children');
%for i=1:length(f)
%  if ~strcmp(f(i).Name,'FLIIMP - Step 2')
%    delete(f(i).Number);
%  end
%end
%delete(handles.logfigure);
delete(gcf);

% --- Executes on mouse press over axes background.
function isoAxes2_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to isoAxes2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

C = get(hObject, 'CurrentPoint');
click=fix(C(1,1)+0.5);

if ~isfield(handles.settings,'excludeInj')
  handles.settings.excludeInj={};
end
try
  idx=handles.sampleList.Value;
  ei=handles.settings.excludeInj{idx};
  
  % modify based on selection
  eidx=find(ei==click);
  if isempty(eidx)
      if ei(1)==-1
          ei=click;
      else
          ei=unique(sort([click;ei(:)]));
      end
  else
      ei(eidx)=[];
      if isempty(ei)
          ei=-1;
      end
  end
  
  try
    set(handles.excludeInjText,'String',sprintf('%d ',handles.settings.excludeInj{idx}));
    excl=handles.settings.excludeInj{idx};
  catch
    set(handles.excludeInjText,'String','[-1]');
  end

  handles.settings.excludeInj{get(handles.sampleList,'Value')}=ei(:)';

  % redo memory analysis, if it has been done
  if isfield(handles.settings,'mem')
    data=FLIIMP_memory_analysis(handles.wdata,handles.settings.excludeInj,handles.settings.mem.avgInj);
  else 
    data=handles.wdata;
  end
  % split vials
  vials = sum(diff(data.InjNr)<1)+1;
  vialstart = [1;find(diff(data.InjNr)<1)+1;length(data.InjNr)+1];
  f = fieldnames(data);
  for i = 1:vials
    for k = 1:length(f)
      vial(i).(f{k}) = data.(f{k})(vialstart(i):vialstart(i+1)-1);
    end
    vial(i).mH2O=mean(vial(i).H2O,'omitnan');
    vial(i).sH2O=std(vial(i).H2O,'omitnan');
  end
  handles.vial=vial;
  
  guidata(hObject,handles);
  updateInjectionList(hObject,handles,get(handles.sampleList,'Value'));
  sampleList_Callback(handles.sampleList,0,handles);
catch
  logf(handles.settings.lid,'Error in excluded injections, use index nr seperate by space');
  handles.settings
end


% --- Executes on mouse press over axes background.
function isoAxes_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to isoAxes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

isoAxes2_ButtonDownFcn(hObject, eventdata, handles);


% --- Executes on mouse press over axes background.
function plotAxes_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to plotAxes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

isoAxes2_ButtonDownFcn(hObject, eventdata, handles);

% --- Executes on mouse press over axes background.
function memAxes_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to memAxes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

isoAxes2_ButtonDownFcn(hObject, eventdata, handles);


% --------------------------------------------------------------------
function Tools_Callback(hObject, eventdata, handles)
% hObject    handle to Tools (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function memdrift_Callback(hObject, eventdata, handles)
% hObject    handle to memdrift (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
FLIIMP_form_memdrift(handles);

% --------------------------------------------------------------------
function LTP_Callback(hObject, eventdata, handles)
% hObject    handle to LTP (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
FLIIMP_form_LTP(handles);


% --- Executes on button press in checkboxHum.
function checkboxHum_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxHum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% load the data file
settings=handles.settings;

% create intermediate structure with paths
lsettings=settings;
lsettings.inputPath=handles.inputPath;
lsettings.outputPath=handles.outputPath;

% read in the files which is pointed at
data=FLIIMP_read_liquid_injections(handles,lsettings);
handles.settings.useHumidityCorrection=get(handles.checkboxHum,'Value');
handles.wdata=FARLAB_wconc_corr(data,settings.instrument,handles.settings.useHumidityCorrection);
% redo memory analysis, if it has been done
if ~isfield(settings,'excludeInj')
  settings.excludeInj={[-1]};
end
if ~isfield(settings,'mem')
  data=FLIIMP_memory_analysis(handles.wdata,handles.settings.excludeInj,3);
else
  if ~isfield(settings.mem,'avgInj')
    handles.settings.mem.avgInj=3;
  end
  data=FLIIMP_memory_analysis(handles.wdata,handles.settings.excludeInj,handles.settings.mem.avgInj);
end
% split vials
vials = sum(diff(data.InjNr)<1)+1;
vialstart = [1;find(diff(data.InjNr)<1)+1;length(data.InjNr)+1];
f = fieldnames(data);
for i = 1:vials
  for k = 1:length(f)
    vial(i).(f{k}) = data.(f{k})(vialstart(i):vialstart(i+1)-1);
  end
  vial(i).mH2O=mean(vial(i).H2O,'omitnan');
  vial(i).sH2O=std(vial(i).H2O,'omitnan');
end
handles.vial=vial;

guidata(hObject,handles);
sampleList_Callback(handles.sampleList,0,handles);


% --------------------------------------------------------------------
function Log_Callback(hObject, eventdata, handles)
% hObject    handle to Log (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.logfigure,'Visible','on');


% --- Executes when user attempts to close FLIIMP_form_B.
function FLIIMP_form_B_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to FLIIMP_form_B (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
QuitMenu_Callback(hObject, eventdata, handles);

% --- Executes on button press in inspectionButton.
function inspectionButton_Callback(hObject, eventdata, handles)
% hObject    handle to inspectionButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% get currently selected injection
idx=handles.sampleList.Value;
sdate=min(handles.vial(idx).date,[],'omitnan');
edate=max(handles.vial(idx).date,[],'omitnan');

% load timeframe of injections and show as figure
datapath=sprintf('%s/Instruments/%s/raw_nc',handles.settings.inputPath,handles.settings.instrument);
idata=FARLAB_vapour_data(handles.settings.instrument,sdate,edate,datapath);

% find the segment in the correct humidity range
idxh=find(idata.H2O>17000 & idata.H2O<23000);

h=dimfigure(114,20,10);
set(gcf,'Name','Inspection of injections')
subplot(2,2,1)
plot(idata.time,idata.H2O,'k-')
hold on
plot(idata.time(idxh),idata.H2O(idxh),'r.')
xlim([sdate,edate])
datetick('x','dd HH:MM','keepticks','keeplimits')
subplot(2,2,2)
plot(idata.time,idata.delta_18O,'k-')
hold on
plot(idata.time(idxh),idata.delta_18O(idxh),'r.')
xlim([sdate,edate])
datetick('x','dd HH:MM','keepticks','keeplimits')
subplot(2,2,3)
plot(idata.time,idata.delta_D,'k-')
hold on
plot(idata.time(idxh),idata.delta_D(idxh),'r.')
xlim([sdate,edate])
datetick('x','dd HH:MM','keepticks','keeplimits')
subplot(2,2,4)
plot(idata.time,idata.OutletValve,'k-')
hold on
plot(idata.time(idxh),idata.OutletValve(idxh),'r.')
xlim([sdate,edate])
datetick('x','dd HH:MM','keepticks','keeplimits')

% fin
