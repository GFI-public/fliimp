function varargout = FLIIMP_form_log(varargin)
% FLIIMP_FORM_LOG MATLAB code for FLIIMP_form_log.fig
%      FLIIMP_FORM_LOG, by itself, creates a new FLIIMP_FORM_LOG or raises the existing
%      singleton*.
%
%      H = FLIIMP_FORM_LOG returns the handle to a new FLIIMP_FORM_LOG or the handle to
%      the existing singleton*.
%
%      FLIIMP_FORM_LOG('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FLIIMP_FORM_LOG.M with the given input arguments.
%
%      FLIIMP_FORM_LOG('Property','Value',...) creates a new FLIIMP_FORM_LOG or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before FLIIMP_form_log_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to FLIIMP_form_log_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help FLIIMP_form_log

% Last Modified by GUIDE v2.5 05-Apr-2023 13:23:35

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @FLIIMP_form_log_OpeningFcn, ...
                   'gui_OutputFcn',  @FLIIMP_form_log_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before FLIIMP_form_log is made visible.
function FLIIMP_form_log_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to FLIIMP_form_log (see VARARGIN)

% Choose default command line output for FLIIMP_form_log
handles.output = hObject;
set(hObject,'Name','FLIIMP - Log window');


% Update handles structure
guidata(hObject, handles);

% UIWAIT makes FLIIMP_form_log wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = FLIIMP_form_log_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
varargout{2} = handles.logText;


function logText_Callback(hObject, eventdata, handles)
% hObject    handle to logText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of logText as text
%        str2double(get(hObject,'String')) returns contents of logText as a double


% --- Executes during object creation, after setting all properties.
function logText_CreateFcn(hObject, eventdata, handles)
% hObject    handle to logText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
set(hObject,'Visible','Off');


% --------------------------------------------------------------------
function Log_Callback(hObject, eventdata, handles)
% hObject    handle to Log (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function view_Callback(hObject, eventdata, handles)
% hObject    handle to view (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function hide_Callback(hObject, eventdata, handles)
% hObject    handle to hide (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(hObject,'Visible','Off');

% --------------------------------------------------------------------
function sizeup_Callback(hObject, eventdata, handles)
% hObject    handle to sizeup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fs=get(handles.logText,'FontSize');
set(handles.logText,'FontSize',fs+1);


% --------------------------------------------------------------------
function sizedown_Callback(hObject, eventdata, handles)
% hObject    handle to sizedown (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fs=get(handles.logText,'FontSize');
set(handles.logText,'FontSize',fs-1);

% --------------------------------------------------------------------
function copy_Callback(hObject, eventdata, handles)
% hObject    handle to copy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
txt=get(handles.logText,'String');
txtr=strrep(txt,'[',[char(13),'[']);
clipboard('copy',[txtr{:}]);

% --------------------------------------------------------------------
function Clear_Callback(hObject, eventdata, handles)
% hObject    handle to Clear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setappdata(0,'logs',{});
set(handles.logText,'String','*** Log cleared ***');
