#!/bin/bash
# FLIIMP.sh
#--------------------------------
# Description
# start FLIIMP from desktop
#--------------------------------
# HS, Thu Jun 14 15:25:18 CEST 2018 
#--------------------------------

if [ ! -d /Volumes/farlab ]; then
  echo "please mount FARLAB first!"
  exit -1
else
  cd /Users/$USER/Dropbox/FARLAB/matlab/FLIIMP/src
  if [ $USER == 'hso039' ]; then
    /Applications/MATLAB_R2020a.app/bin/matlab -nodesktop -nosplash -r FLIIMP
  else
    /Applications/MATLAB_R2016b.app/bin/matlab -nodesktop -nosplash -r FLIIMP
  fi
fi

# fin
