%> FLIIMP_standards.m
%> -----------------------------------------------------------------------------------
%> last changes: HS, 20.10.2017
%> FLIIMP Version 1.0
%> -----------------------------------------------------------------------------------
%> returns calcor structure and optional available calibration runs
%----------------------------------------------------
% own standards can be added by pasting the correct arrangement from a spreadsheet application
% blanks or commas can be used to separate numbers in a row
% the default syntax is: calcor.<property>=[ value_1, value_2, value_3, ..., value_n]';

function [calcor, calruns]=FLIIMP_standards(cal_nr)

global config
if isempty(config)
  config=FLIIMP_config;
end

% read the available calibration data sets
stdfiles=dir([config.installdir,'/standards/*.csv']);
for i=1:length(stdfiles)
  try
    fids=fopen([stdfiles(i).folder,'/',stdfiles(i).name]);
    ins=fgetl(fids); % read header line
    % parse index from file name
    npos=min(strfind(stdfiles(i).name,'-'))-1;
    n=eval(stdfiles(i).name(1:npos));
    calruns(i)=n;
    standards{n}=textscan(fids,'%s%f%f%f%f%f%f','Delimiter',',');
    descriptions{n}=stdfiles(i).name((npos+2):end-4);
    fclose(fids);
  catch
    fprintf('Calibration file %s could not be processed. Please check formatting of csv file.\n',stdfiles(i).name);
  end
end

% array containing valid options (for building user interface)
calruns=sort(calruns);

% for UI buildup
if cal_nr==-1
  calcor=[];
  return
end

% in case of non-existing calibration file
if ~ismember(calruns,cal_nr)
  fprintf('calibration %d unavailable. Please review csv files in folder standards.',cal_nr);
  calcor=[];
  return
end

calcor.dD=standards{cal_nr}{2}';
calcor.dD_SD=standards{cal_nr}{3}';
calcor.names=standards{cal_nr}{1}';
calcor.d18O=standards{cal_nr}{4}';
calcor.d18O_SD=standards{cal_nr}{5}';
calcor.d17O=standards{cal_nr}{6}';
calcor.d17O_SD=standards{cal_nr}{7}';
calcor.description=descriptions{cal_nr};

%fprintf('Using calibration %s\n',calcor.description);
return
