function varargout = FLIIMP_form_LTR(varargin)
% FLIIMP_FORM_LTR MATLAB code for FLIIMP_form_LTR.fig
%      FLIIMP_FORM_LTR, by itself, creates a new FLIIMP_FORM_LTR or raises the existing
%      singleton*.
%
%      H = FLIIMP_FORM_LTR returns the handle to a new FLIIMP_FORM_LTR or the handle to
%      the existing singleton*.
%
%      FLIIMP_FORM_LTR('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FLIIMP_FORM_LTR.M with the given input arguments.
%
%      FLIIMP_FORM_LTR('Property','Value',...) creates a new FLIIMP_FORM_LTR or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before FLIIMP_form_LTR_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to FLIIMP_form_LTR_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help FLIIMP_form_LTR

% Last Modified by GUIDE v2.5 07-Apr-2023 21:57:10
% read configuration
global config;
config=FLIIMP_config;

% assign install directory in case FLIIMP is not run from source directory
addpath(config.installdir)

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @FLIIMP_form_LTR_OpeningFcn, ...
                   'gui_OutputFcn',  @FLIIMP_form_LTR_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before FLIIMP_form_LTR is made visible.
function FLIIMP_form_LTR_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to FLIIMP_form_LTR (see VARARGIN)

global calset;
global config;

% Choose default command line output for FLIIMP_form_LTR
handles.output = hObject;
%handles.settings = varargin{1};
handles.settings.lid=varargin{1}.settings.lid;
handles.logtext=varargin{1}.logtext;

% Update handles structure
guidata(hObject, handles);

% add calibrations to popup menu
[~,cruns]=FLIIMP_standards(-1);
calnames={};
for cnr=1:length(cruns)
    ccor=FLIIMP_standards(cruns(cnr));
    if ~isempty(ccor)
      calnames{cnr}=sprintf('Cal-%03d: %s',cruns(cnr),ccor.description);
    else
      calnames{cnr}=sprintf('Cal-%03d: N/A',cruns(cnr));
    end
end
set(handles.calnrPopup,'string',calnames);
% select the maximum of the below-100 calibration (i.e. non-special)
cnum=find(cruns<100,1,'last');
set(handles.calnrPopup,'Value',cnum);
calset=FLIIMP_standards(cnum);

% This sets up the initial plot - only do when we are invisible
% so window can get raised using FLIIMP_form_LTR.
%if strcmp(get(hObject,'Visible'),'off')
%  plot(rand(1),'w.');
%end

if strcmp(get(hObject,'Visible'),'off')
  plot(handles.axesLPT,rand(2),'w.');
end

axes(handles.axesLPT);
cla;


% Populate instrument selector with available instruments
set(handles.popupInstrument,'string',config.deviceNames);

% Populate standards selector with available standards
set(handles.popupStandard,'string',calset.names);

% Pre-select current date range to last year
set(handles.editStart,'String',datestr(now-365,'yyyymmdd'));
set(handles.editEnd,'String',datestr(now,'yyyymmdd'));

% UIWAIT makes FLIIMP_form_LTR wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = FLIIMP_form_LTR_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --------------------------------------------------------------------
function FileMenu_Callback(hObject, eventdata, handles)
% hObject    handle to FileMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function OpenMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to OpenMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
file = uigetfile('*.fig');
if ~isequal(file, 0)
    open(file);
end

% --------------------------------------------------------------------
function PrintMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to PrintMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
printdlg(handles.figure1)

% --------------------------------------------------------------------
function CloseMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to CloseMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
selection = questdlg(['Close ' get(handles.figure1,'Name') '?'],...
                     ['Close ' get(handles.figure1,'Name') '...'],...
                     'Yes','No','Yes');
if strcmp(selection,'No')
    return;
end

delete(handles.figure1)


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
     set(hObject,'BackgroundColor','white');
end

set(hObject, 'String', {'plot(rand(5))', 'plot(sin(1:0.01:25))', 'bar(1:.5:10)', 'plot(membrane)', 'surf(peaks)'});


% --- Executes on selection change in popupInstrument.
function popupInstrument_Callback(hObject, eventdata, handles)
% hObject    handle to popupInstrument (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupInstrument contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupInstrument

global alldata;

syears=get(handles.editStart,'String');
eyears=get(handles.editEnd,'String');
try
  syear=datenum(syears,'yyyymmdd');
catch
  logf(handles,'No valid start date specified. Use format YYYYMMDD.\n');
  return
end
if ~isempty(eyears)
  eyear=datenum(eyears,'yyyymmdd');
else
  eyear=now;
end
if isfield(alldata,'Date')
  plotLPT(syear,eyear,handles);
end

% --- Executes during object creation, after setting all properties.
function popupInstrument_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupInstrument (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function editStart_Callback(hObject, eventdata, handles)
% hObject    handle to editStart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editStart as text
%        str2double(get(hObject,'String')) returns contents of editStart as a double

global alldata;

syears=get(handles.editStart,'String');
eyears=get(handles.editEnd,'String');
try
  syear=datenum(syears,'yyyymmdd');
catch
  logf(handles,'No valid start date specified. Use format YYYYMMDD.\n');
  return
end
if ~isempty(eyears)
  eyear=datenum(eyears,'yyyymmdd');
else
  eyear=now;
end
if isfield(alldata,'Date')
  plotLPT(syear,eyear,handles);
end

% --- Executes during object creation, after setting all properties.
function editStart_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editStart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function editEnd_Callback(hObject, eventdata, handles)
% hObject    handle to editEnd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editEnd as text
%        str2double(get(hObject,'String')) returns contents of editEnd as a double

global alldata;

syears=get(handles.editStart,'String');
eyears=get(handles.editEnd,'String');
try
  syear=datenum(syears,'yyyymmdd');
catch
  logf(handles,'No valid start date specified. Use format YYYYMMDD.\n');
  return
end
if ~isempty(eyears)
  eyear=datenum(eyears,'yyyymmdd');
else
  eyear=now;
end
if isfield(alldata,'Date')
  plotLPT(syear,eyear,handles);
end

% --- Executes during object creation, after setting all properties.
function editEnd_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editEnd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in buttonUpdate.
function buttonUpdate_Callback(hObject, eventdata, handles)
% hObject    handle to buttonUpdate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global config;

% display status
set(handles.infoText,'String','Scanning available calibration results');

clear alldata
global alldata
global calset
alldata.init=0;
runcnt=0;

% get input parameters
syears=get(handles.editStart,'String');
eyears=get(handles.editEnd,'String');
try
  syear=eval(syears(1:4));
catch
  logf(handles,'No valid start date specified. Use format YYYYMMDD.\n');
  return
end
if ~isempty(eyears)
  eyear=eval(eyears(1:4));
else
  eyear=eval(datestr(now,'yyyy'));
end

% loop through selected years
for year=syear:eyear
    
    projects=dir(sprintf('%s/Projects/%d/*',config.defaultInputPath,year));    
    set(handles.infoText,'String',sprintf('scanning %s...',sprintf('%s/Projects/%d/*',config.defaultInputPath,year)));
    logf(handles,sprintf('scanning %s...',sprintf('%s/Projects/%d/*',config.defaultInputPath,year)));
    
    for i=1:length(projects)
        runs=dir(sprintf('%s/%s/*',projects(i).folder,projects(i).name));
        for j=1:length(runs)
            accfile=dir(sprintf('%s/%s/*accounting.csv',runs(j).folder,runs(j).name));
            accfile = accfile(arrayfun(@(x) ~strcmp(x.name(1),'.'),accfile));
            allfile=dir(sprintf('%s/%s/*alldata.csv',runs(j).folder,runs(j).name));
            allfile = allfile(arrayfun(@(x) ~strcmp(x.name(1),'.'),allfile));
            % get date and instrument
            if length(accfile)==1
                %opts=detectImportOptions([accfile(1).folder,'/',accfile(1).name], 'NumHeaderLines', 1, 'FileType','text','NumVariables',7); 
                %opts.VariableNamesLine=1;
                %adata=table2struct(readtable([accfile(1).folder,'/',accfile(1).name],opts));
                %[accfile(1).folder,'/',accfile(1).name];
                adata=table2struct(readtable([accfile(1).folder,'/',accfile(1).name],'Delimiter',','));
                try 
                  if ~isfield(adata,'Lab_Filename')
                    accdata=split(adata(1).FARLABFilename,'_');
                  else
                    accdata=split(adata(1).Lab_Filename,'_');
                  end
                  instrument=find(strcmp(config.devices,accdata{1})==1);
                  dat=datenum([accdata{end-1},accdata{end}(1:6)],'yyyymmddHHMMSS');
                catch
                  logf(handles,'WARNING: error reading accdata file, found wrong number of column names.\n');
                end                  
            end
            % get standards
            if length(allfile)==1
                runcnt=runcnt+1;
                %fprintf('\n');
                if i==1
                    disp(accfile(1).folder)
                end
                set(handles.infoText,'String',sprintf('getting data from run %s',accfile(1).folder));
                logf(handles,sprintf('getting data from run %s',accfile(1).folder));
                %sprintf('%s/%s/*alldata.csv',runs(j).folder,runs(j).name);
                %opts=detectImportOptions([allfile(1).folder,'/',allfile(1).name], 'NumHeaderLines', 1); 
                %opts.VariableNamesLine=1;
                %data=table2struct(readtable([allfile(1).folder,'/',allfile(1).name],opts));
                %[allfile(1).folder,'/',allfile(1).name];
                data=table2struct(readtable([allfile(1).folder,'/',allfile(1).name],'Delimiter',','));
                if (size(fieldnames(data))~=34) & (size(fieldnames(data))~=22)
                    logf(handles,'WARNING: error reading alldata file, found %d columns instead of 22 or 34.\n',size(fieldnames(data)));
                else
                    % merge to long file with standards only
                    for l=1:length(data)
                        data(l).Instrument=instrument;
                        data(l).Date=dat+l*1/24/6; % add 10 min per sample
                        data(l).Standard=0;
                        data(l).Project=projects(i).name;
                        data(l).Run=runs(j).name;
                        data(l).RunCnt=runcnt;
                        fnames=fieldnames(data);
                        for s=1:length(calset.names)
                            standard=calset.names{s};
                            if strcmp({data(l).Sample_name},standard)==1
                                data(l).Standard=s;
                                for k=1:length(fnames)
                                    f=data(l).(fnames{k});
                                    if isfield(alldata,fnames{k})
                                        try
                                          alldata.(fnames{k})=[alldata.(fnames{k}),f];
                                        catch
                                            logf(handles,'WARNING: Could not concatenate file %s\n',allfile(1).name)
                                        end
                                    else
                                        if ischar(f)
                                            alldata.(fnames{k})={f};
                                        else
                                            alldata.(fnames{k})=f;
                                        end
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end
    end
end

set(handles.infoText,'String',sprintf('Scanning complete for time period %s to %s',get(handles.editStart,'string'),get(handles.editEnd,'string')));
logf(handles,sprintf('Scanning complete for time period %s to %s',get(handles.editStart,'string'),get(handles.editEnd,'string')));

syears=get(handles.editStart,'String');
eyears=get(handles.editEnd,'String');
try
  syear=datenum(syears,'yyyymmdd');
catch
  logf(handles,'No valid start date specified. Use format YYYYMMDD.\n');
  return
end
if ~isempty(eyears)
  eyear=datenum(eyears,'yyyymmdd');
else
  eyear=now;
end
if isfield(alldata,'Date')
  plotLPT(syear,eyear,handles);
else
  logf(handles,'WARNING: No run parameter files *alldata.csv found for date range %s to %s in folder %s. Please check time range and path.\n',syears,eyears,config.defaultInputPath);
end


function editStandard_Callback(hObject, eventdata, handles)
% hObject    handle to editStandard (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editStandard as text
%        str2double(get(hObject,'String')) returns contents of editStandard as a double


% --- Executes during object creation, after setting all properties.
function editStandard_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editStandard (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function plotLPT(syears,eyears,handles)

global alldata
global config
global calset

cols={'r';'b';'c';'k';'m'};

% FontSize
fs=9;

% return immediately if nothing to display
if ~isfield(alldata,'Date')
  return
end

%drange=[min(alldata.Date)-5 max(alldata.Date)+5];
drange=[syears,eyears];

s=get(handles.popupStandard,'Value');
i=get(handles.popupInstrument,'Value');
k=get(handles.popupIsotope,'Value');
f=get(handles.checkboxFilter,'Value');
standard=calset.names{s};

axes(handles.axesLPT);
cla;

clear h;
idx=find((alldata.Instrument==i)&(alldata.Standard==s)& alldata.Date>drange(1) & alldata.Date<drange(2));
if ~isempty(idx)
    % calculate averages
    mdD=mean(alldata.dD_cal(idx));
    sdD=std(alldata.dD_cal(idx));
    msdD=mean(alldata.dD_cal_u(idx));
    md18O=mean(alldata.d18O_cal(idx));
    sd18O=std(alldata.d18O_cal(idx));
    msd18O=mean(alldata.d18O_cal_u(idx));
    mdxs=mean(alldata.dxs_cal(idx));
    sdxs=std(alldata.dxs_cal(idx));
    msdxs=mean(alldata.dxs_cal_u(idx));
    if isfield(alldata,'d17O_cal')
      md17O=NaN;
      sd17O=NaN;
      msd17O=NaN;
      me17=NaN;
      se17=NaN;
      mse17=NaN;
      %md17O=mean(alldata.d17O_cal(idx));
      %sd17O=std(alldata.d17O_cal(idx));
      %msd17O=mean(alldata.d17O_cal_u(idx));
      %me17=mean(alldata.e17_cal(idx));
      %se17=std(alldata.e17_cal(idx));
      %mse17=mean(alldata.e17_cal_u(idx));
    else
      md17O=NaN;
      sd17O=NaN;
      msd17O=NaN;
      me17=NaN;
      se17=NaN;
      mse17=NaN;
    end
    
    % filter if requested
    if f==1
      fidx=find(abs(alldata.d18O_cal(idx)-md18O)<=3*calset.d18O_SD(s) & abs(alldata.dD_cal(idx)-mdD)<=3*calset.dD_SD(s));
      uidx=find(alldata.d18O_cal_u(idx)<3*msd18O & alldata.dD_cal_u(idx)<3*msdD);
      idx=idx(intersect(fidx,uidx));
    end

    % recalculate averages
    mdD=mean(alldata.dD_cal(idx));
    sdD=std(alldata.dD_cal(idx));
    ND=sum(~isnan(alldata.dD_cal(idx)));
    md18O=mean(alldata.d18O_cal(idx));
    sd18O=std(alldata.d18O_cal(idx));
    N18=sum(~isnan(alldata.d18O_cal(idx)));
    mdxs=mean(alldata.dxs_cal(idx));
    sdxs=std(alldata.dxs_cal(idx));
    Nd=sum(~isnan(alldata.dxs_cal(idx)));
    if isfield(alldata,'d17O_cal')
      %md17O=mean(alldata.d17O_cal(idx));
      %sd17O=std(alldata.d17O_cal(idx));
      %me17=mean(alldata.e17_cal(idx));
      %se17=std(alldata.e17_cal(idx));
      md17O=NaN;
      sd17O=NaN;
      N17=0;
      me17=NaN;
      se17=NaN;
      Ne17=0;
    else
      md17O=NaN;
      sd17O=NaN;
      N17=0;
      me17=NaN;
      se17=NaN;
      Ne17=0;
    end

    % assemble results
    syearstr=datestr(syears);
    eyearstr=datestr(eyears);
    results{1}=sprintf('FARLAB standard: %s (Instrument %s)',calset.names{s},config.devices{alldata.Instrument(idx(1))});
    results{2}=sprintf('Time period: %s to %s. ',syearstr,eyearstr);
    results{3}=[char(ones(1,length(results{1}))*45),' '];
    results{4}=sprintf('long-term dD SD:   %7.3f permil (N=%d)',sdD,ND);
    results{5}=sprintf('long-term d18O SD: %7.3f permil (N=%d)',sd18O,N18);
    %results{4}=sprintf('long-term standard deviation dxs:  %7.3f\n',sdxs)
    if isfield(alldata,'d17O_cal')
      results{6}=sprintf('long-term d17O SD: %7.3f permil (N=%d)',sd17O,N17);
      %results{6}=sprintf('long-term standard deviation e17:  %7.3f\n\n',se17)
    end
    % assemble results in csv format
    csv{1}=sprintf('FARLAB standard,Instrument,Start date,End date,Parameter,Long-term SD (permil),N (1)_');
    csv{2}=sprintf('%s,%s,%s,%s,dD,%7.3f,%d_',calset.names{s},config.devices{alldata.Instrument(idx(1))},syearstr,eyearstr,sdD,ND);
    csv{3}=sprintf('%s,%s,%s,%s,d18O,%7.3f,%d_',calset.names{s},config.devices{alldata.Instrument(idx(1))},syearstr,eyearstr,sd18O,N18);
    %results{4}=sprintf('long-term standard deviation dxs:  %7.3f\n',sdxs)
    if isfield(alldata,'d17O_cal')
      csv{4}=sprintf('%s,%s,%s,%s,d17O,%7.3f,%d_',calset.names{s},config.devices{alldata.Instrument(idx(1))},syearstr,eyearstr,sd17O,N17);
      %results{6}=sprintf('long-term standard deviation e17:  %7.3f\n\n',se17)
    end

    % show in UI
    set(handles.editResults,'String',results);
    
    % save for copy request
    handles.csv=csv;
    guidata(gcf,handles)

    % update the plot
    switch k
        case 1
            hold on
            plot(drange,[calset.dD(s)-calset.dD_SD(s) calset.dD(s)-calset.dD_SD(s)],'k--');
            plot(drange,[calset.dD(s) calset.dD(s)],'k-');
            plot(drange,[calset.dD(s)+calset.dD_SD(s) calset.dD(s)+calset.dD_SD(s)],'k--');
            plot(drange,[mdD-sdD mdD-sdD],'--','Color',cols{k});
            plot(drange,[mdD mdD],'-','Color',cols{k});
            plot(drange,[mdD+sdD mdD+sdD],'--','Color',cols{k});
            h=errorbar(alldata.Date(idx),alldata.dD_cal(idx),alldata.dD_cal_u(idx),'.','Color',cols{k});
            %b=0.7;
            %for j=1:length(idx)
            %    text(alldata.Date(idx(j)),alldata.dD_cal(idx(j))+b,alldata.Project{idx(j)}(6:7),'FontSize',fs);
            %    b=-b;
            %end
            set(gca,'TickDir','out','XLim',drange,'FontSize',fs);
            datetick('x','yyyy-mm','keeplimits');
            ylabel('\delta D / permil','FontSize',fs);
            box on
        case 2
            hold on
            plot(drange,[calset.d18O(s)-calset.d18O_SD(s) calset.d18O(s)-calset.d18O_SD(s)],'k--');
            plot(drange,[calset.d18O(s) calset.d18O(s)],'k-');
            plot(drange,[calset.d18O(s)+calset.d18O_SD(s) calset.d18O(s)+calset.d18O_SD(s)],'k--');
            plot(drange,[md18O-sd18O md18O-sd18O],'--','Color',cols{k});
            plot(drange,[md18O md18O],'-','Color',cols{k});
            plot(drange,[md18O+sd18O md18O+sd18O],'--','Color',cols{k});
            h=errorbar(alldata.Date(idx),alldata.d18O_cal(idx),alldata.d18O_cal_u(idx),'.','Color',cols{k});
            %b=0.1;
            %for j=1:length(idx)
            %    text(alldata.Date(idx(j)),alldata.d18O_cal(idx(j))+b,alldata.Project{idx(j)}(6:7),'FontSize',fs);
            %    b=-b;
            %end
            set(gca,'TickDir','out','XLim',drange,'FontSize',fs);
            datetick('x','yyyy-mm','keeplimits');
            ylabel('\delta^{18}O / permil','FontSize',fs);
            box on
        case 3
            hold on
            plot(drange,[calset.d17O(s)-calset.d17O_SD(s) calset.d17O(s)-calset.d17O_SD(s)],'k--');
            plot(drange,[calset.d17O(s) calset.d17O(s)],'k-');
            plot(drange,[calset.d17O(s)+calset.d17O_SD(s) calset.d17O(s)+calset.d17O_SD(s)],'k--');
            plot(drange,[md17O-sd17O md17O-sd17O],'--','Color',cols{k});
            plot(drange,[md17O md17O],'-','Color',cols{k});
            plot(drange,[md17O+sd17O md18O+sd17O],'--','Color',cols{k});
            h=errorbar(alldata.Date(idx),alldata.d17O_cal(idx),alldata.d17O_cal_u(idx),'.','Color',cols{k});
            %b=0.1;
            %for j=1:length(idx)
            %    text(alldata.Date(idx(j)),alldata.d18O_cal(idx(j))+b,alldata.Project{idx(j)}(6:7),'FontSize',fs);
            %    b=-b;
            %end
            set(gca,'TickDir','out','XLim',drange,'FontSize',fs);
            datetick('x','yyyy-mm','keeplimits');
            ylabel('\delta^{17}O / permil','FontSize',fs);
            box on
        case 4
            hold on
            plot(drange,[mdxs-sdxs mdxs-sdxs],'--','Color',cols{k});
            plot(drange,[mdxs mdxs],'-','Color',cols{k});
            plot(drange,[mdxs+sdxs mdxs+sdxs],'--','Color',cols{k});
            calset.dxs(s)=calset.dD(s)-8*calset.d18O(s);
            %plot(drange,[calset.dxs(s)-calset.dxs_SD(s) calset.dxs(s)-calset.dxs_SD(s)],'k--');
            plot(drange,[calset.dxs(s) calset.dxs(s)],'k-');
            %plot(drange,[calset.dxs(s)+calset.dxs_SD(s) calset.dxs(s)+calset.dxs_SD(s)],'k--');
            h=errorbar(alldata.Date(idx),alldata.dxs_cal(idx),alldata.dxs_cal_u(idx),'.','Color',cols{k});
            %b=1.0;
            %for j=1:length(idx)
            %    text(alldata.Date(idx(j)),alldata.dxs_cal(idx(j))+b,alldata.Project{idx(j)}(6:7),'FontSize',fs);
            %    b=-b;
            %end
            set(gca,'TickDir','out','XLim',drange,'FontSize',fs);
            datetick('x','yyyy-mm','keeplimits');
            ylabel('d-excess / permil','FontSize',fs);
            box on
        case 5
            hold on
            plot(drange,[me17-se17 me17-se17],'--','Color',cols{k});
            plot(drange,[me17 me17],'-','Color',cols{k});
            plot(drange,[me17+se17 me17+se17],'--','Color',cols{k});
            calset.dxs(s)=calset.dD(s)-8*calset.d18O(s);
            %plot(drange,[calset.dxs(s)-calset.dxs_SD(s) calset.dxs(s)-calset.dxs_SD(s)],'k--');
            plot(drange,[calset.dxs(s) calset.dxs(s)],'k-');
            %plot(drange,[calset.dxs(s)+calset.dxs_SD(s) calset.dxs(s)+calset.dxs_SD(s)],'k--');
            h=errorbar(alldata.Date(idx),alldata.e17_cal(idx),alldata.e17_cal_u(idx),'.','Color',cols{k});
            %b=1.0;
            %for j=1:length(idx)
            %    text(alldata.Date(idx(j)),alldata.e17_cal(idx(j))+b,alldata.Project{idx(j)}(6:7),'FontSize',fs);
            %    b=-b;
            %end
            set(gca,'TickDir','out','XLim',drange,'FontSize',fs);
            datetick('x','yyyy-mm','keeplimits');
            ylabel('d-excess / permil','FontSize',fs);
            box on
        otherwise
    end
end
if exist('h','var')
    legend(gca,h,config.devices{i});
    legend boxoff
    %  print('-dpng','-r200',sprintf('../fig/FARLAB_picarro_drift_raw_%d_%s.png',year,standard));
end

% --- Executes on selection change in popupStandard.
function popupStandard_Callback(hObject, eventdata, handles)
% hObject    handle to popupStandard (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupStandard contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupStandard
% get input parameters

global alldata;

syears=get(handles.editStart,'String');
eyears=get(handles.editEnd,'String');
try
  syear=datenum(syears,'yyyymmdd');
catch
  logf(handles,'No valid start date specified. Use format YYYYMMDD.\n');
  return
end
if ~isempty(eyears)
  eyear=datenum(eyears,'yyyymmdd');
else
  eyear=now;
end
if isfield(alldata,'Date')
  plotLPT(syear,eyear,handles);
end

% --- Executes during object creation, after setting all properties.
function popupStandard_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupStandard (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupIsotope.
function popupIsotope_Callback(hObject, eventdata, handles)
% hObject    handle to popupIsotope (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupIsotope contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupIsotope

global alldata;

syears=get(handles.editStart,'String');
eyears=get(handles.editEnd,'String');
try
  syear=datenum(syears,'yyyymmdd');
catch
  logf(handles,'No valid start date specified. Use format YYYYMMDD.\n');
  return
end
if ~isempty(eyears)
  eyear=datenum(eyears,'yyyymmdd');
else
  eyear=now;
end
if isfield(alldata,'Date')
  plotLPT(syear,eyear,handles);
end


% --- Executes during object creation, after setting all properties.
function popupIsotope_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupIsotope (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in checkboxFilter.
function checkboxFilter_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxFilter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkboxFilter

global alldata;

syears=get(handles.editStart,'String');
eyears=get(handles.editEnd,'String');
try
  syear=datenum(syears,'yyyymmdd');
catch
  logf(handles,'No valid start date specified. Use format YYYYMMDD.\n');
  return
end
if ~isempty(eyears)
  eyear=datenum(eyears,'yyyymmdd');
else
  eyear=now;
end
if isfield(alldata,'Date')
  plotLPT(syear,eyear,handles);
end



function editResults_Callback(hObject, eventdata, handles)
% hObject    handle to editResults (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editResults as text
%        str2double(get(hObject,'String')) returns contents of editResults as a double


% --- Executes during object creation, after setting all properties.
function editResults_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editResults (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in textButton.
function textButton_Callback(hObject, eventdata, handles)
% hObject    handle to textButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
txt=get(handles.editResults,'String');
% inset line breaks for copy
txtr=strrep(txt,')',[')',char(13)]);
txtp=strrep(txtr,'- ',['-',char(13)]);
txtq=strrep(txtp,'. ',['.',char(13)]);
clipboard('copy',[txtq{:}]);


% --- Executes on button press in csvButton.
function csvButton_Callback(hObject, eventdata, handles)
% hObject    handle to csvButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
txt=[handles.csv{:}];
% insert tabs for separation
txtr=strrep(txt,',',char(9));
% inset line breaks
txtr=strrep(txt,'_',char(13));
clipboard('copy',txtr);


% --- Executes on selection change in calnrPopup.
function calnrPopup_Callback(hObject, eventdata, handles)
% hObject    handle to calnrPopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns calnrPopup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from calnrPopup
global calset
cnum=get(handles.calnrPopup,'Value');
[~,cruns]=FLIIMP_standards(-1);
calset=FLIIMP_standards(cruns(cnum));
set(handles.popupStandard,'string',calset.names);


% --- Executes during object creation, after setting all properties.
function calnrPopup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to calnrPopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
