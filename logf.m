% function logf
%----------------
% prints output to screen and to log file provided as handle

function logf(handles,format,varargin)

global config
if isempty(config)
  config=FLIIMP_config;
end

if ~isfield(handles,'nolog')
  
  logs=getappdata(0,'logs');
  n=length(logs);
  
  % return immediately if warning not requested
  hasWarning=strcmp(format,'WARNING');
  if (hasWarning==1 && config.warning==0)
    return;
  end
  
  % safely log to console, file and UI
  fprintf(1,[num2str(n+1,'[%3d]'),' ',format,'\n'],varargin{:});
  if handles.settings.lid>1
    fprintf(handles.settings.lid,[num2str(n+1,'[%3d]'),' ',format,'\n'],varargin{:});
  end
  logs{n+1}=sprintf([num2str(n+1,'[%3d]'),' ',format],varargin{:});
  
  if isfield(handles,'logtext')
    if isvalid(handles.logtext)
      set(handles.logtext,'String',logs)
    end
  end
  
  setappdata(0,'logs',logs);

end

return
% fin
