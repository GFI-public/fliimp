%--------------------------------------------
%> FLIIMP_memory_analysis(data,excludeInj,averageInj)
%----------------------------------------------------
%> memory effect calculation module for FLIIMP
%>
%> HS 06.01.2020
%--------------------------------------------
function [mdata] = FLIIMP_memory_analysis(data,excludeInj,averageInj)

  global config
  if isempty(config)
    config=FLIIMP_config;
  end
  % limits to inbetween-sample difference when no memory correction is applied
  limit=config.memory_limits;
  %limit=[1.5,12,1.5,0,0];
  %limit=[0.0,0.0,0.0,0,0];

  iso={'d18O';'dD';'d17O';'dxs';'e17'};

  % add excluded injections to data
  excl=[];
  startIdx = [1;find(diff(data.InjNr)<1)+1];
  for j=1:length(excludeInj)
    if excludeInj{j}(1)>0
      r=transpose(excludeInj{j}+startIdx(j)-1);
      excl=[excl, r(:)'];
    end
  end
  % set property to mark excluded injection
  for j=1:length(data.InjNr)
    if ismember(j,excl)
      data.excluded(j,1)=1;
    else
      data.excluded(j,1)=0;
    end
  end

  mdata=data;

  % adapt to 17O mode
  if isfield(data,'e17')
    vec1=1:5;
    vec2=1:3;
  else
    vec1=[1 2 4];
    vec2=[1 2];
  end

  % find the max number of injections per sequential sample
  smps=unique(data.SampleLine);

  % set ignored injections to NaN for data struct
  for v=vec1
    mdata.([iso{v},'_corr'])(data.excluded==1)=NaN;
  end

  for i=1:length(smps)
    idx=find(data.SampleLine==smps(i));
    mdata.maxInjNr(idx,1)=max(data.InjNr(idx));
  end
  
  % calculate memory for each injection where possible
  % find the locations with InjNr==maxInjNr
  % calculate the averages for each set for injections
  % maxInjNr-ref:maxInjNr
  endidx=find(mdata.InjNr==mdata.maxInjNr);
  for i=1:length(endidx)
    for v=vec1
      if i==1
        %I=ones(mdata.InjNr(endidx(i)),1);
        I=ones(endidx(i),1);
      	rng=1:endidx(i);
      else
        I=ones((endidx(i)-endidx(i-1)),1);
      	rng=(endidx(i-1)+1):endidx(i);
      end
      if ~isempty(rng)
        % safely determine range of last ref injections
        refrng=rng(max(1,end-averageInj+1):end);
        mdata.([iso{v},'_avg'])(rng,1)=I*mean(mdata.([iso{v},'_corr'])(refrng),'omitnan');
      %else
      %  mdata.([iso{v},'_avg'])(rng,1)=I*NaN;
      end
    end
  end

  % subtract from previous sample and calculate memory quantity
  for i=(mdata.maxInjNr(1)+1):length(data.InjNr)
    for v=vec2
      mdata.mstep(i,v)=abs(mdata.([iso{v},'_avg'])(i-mdata.InjNr(i),1)-mdata.([iso{v},'_avg'])(i,1));
      if mdata.mstep(i,v)>limit(v)
	    mdata.([iso{v},'_mem'])(i,1)=max((data.([iso{v},'_corr'])(i)-mdata.([iso{v},'_avg'])(i,1))/(mdata.([iso{v},'_avg'])(i-mdata.InjNr(i),1)-mdata.([iso{v},'_avg'])(i,1))*100.0,-0.3);
        % if negative, set to zero: deactivated to prevent bias
	    %if mdata.([iso{v},'_mem'])(i,1)<=0.0
	      %mdata.([iso{v},'_mem'])(i,1)=0.0;
        %end
      else
	    % if less than treshold, no memory
	    mdata.([iso{v},'_mem'])(i,1)=NaN;
      end
    end
    mdata.dxs_mem(i,1)=max((data.dxs_corr(i)-mdata.dxs_avg(i,1))/(mdata.dxs_avg(i-mdata.InjNr(i),1)-mdata.dxs_avg(i,1))*100.0,0);
    if isfield(mdata,'e17')
      mdata.e17_mem(i,1)=max((data.e17_corr(i)-mdata.e17_avg(i,1))/(mdata.e17_avg(i-mdata.InjNr(i),1)-mdata.e17_avg(i,1))*100.0,0);
    end
  end
  %size(mdata.mstep)
  
  % always skip memory correction for first vial
  for v=vec1
    mdata.([iso{v},'_mem'])(1:mdata.maxInjNr(1),1)=NaN;
  end

  % set ignored injections memory to NaN for data struct
  for v=vec1
    mdata.([iso{v},'_mem'])(data.excluded==1)=NaN;
  end
  
  % set memory for last averageInj injections to zero
  endidx=find(mdata.InjNr==mdata.maxInjNr);
  for i=2:length(endidx)
    rng=(endidx(i-1)+1):endidx(i);
    if ~isempty(rng)
      % safely determine range of last averageInj injections
      refrng=rng(max(1,end-averageInj+1):end);
      for v=vec1
        mdata.([iso{v},'_mem'])(refrng)=mdata.([iso{v},'_mem'])(refrng).*0;
      end
    end
  end
  
end
%fin
