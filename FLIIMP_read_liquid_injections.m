%-----------------------------------------------------------------------
% function data = FLIIMP_read_liquid_injections(handles,settings)
%
% - read Picarro L2140i and L2130i autosampler data files for liquid injections
% - 17O mode or 18O mode, intelligent recognition
%-----------------------------------------------------------------------
% P.Graf, IAC ETH
% - original routine
%-----------------------------------------------------------------------
% HS, 22.01.2016
% - adopted for UiB FARLAB
%-----------------------------------------------------------------------
% YW, 12.05.2016 
% - can read both a single file and multiple files during a period
% - Header names standardized: 'd17O','d18O','dD','dxs','e17','d17O_SD',...
%-----------------------------------------------------------------------
% HS, 22.08.2017
% - can read either 17O or 18O mode files
%-----------------------------------------------------------------------
% HS, 12.09.2017
% - takes settings as argument for FLIIMP integration
%-----------------------------------------------------------------------
% HS, 15.09.2018
% - optionally takes times in datenum format
%-----------------------------------------------------------------------
% HS, 11.06.2020
% - take care of bad files and report number of samples and injections
%-----------------------------------------------------------------------

function data = FLIIMP_read_liquid_injections(handles,settings)

global config
if isempty(config)
  config=FLIIMP_config;
end

% detect all the files within the date(s)
FLIIMP_analysers=config.devices;
if ~ismember(settings.instrument,FLIIMP_analysers)
  logf(handles,'ERROR: FLIIMP analyser has to be one of:\n');
  FARLAB_analysers
  data=[];
  return
end

% path to data files. Use %s as a placeholder to the base path and instrument name
path=sprintf(config.inputPattern,settings.inputPath,settings.instrument);
%path = sprintf('/Volumes/metdata/FARLAB/%s/IsotopeData/',settings.instrument);
%path = sprintf('/Volumes/farlab/Instruments/%s/IsotopeData/',settings.instrument);
%path = sprintf('/Volumes/FARLAB/Instruments/%s/IsotopeData/',settings.instrument);

% convert startDate/endDate to datenum format
if strcmp(class(settings.startDate),'double')==0
    % to read files for the whole day
    filterdate=settings.startDate;
    settings.startDate=datenum(settings.startDate,'yyyymmdd');
else 
    % to read a specific file
    filterdate=datestr(settings.startDate,'yyyymmdd_HHMMSS');
end
if strcmp(class(settings.endDate),'double')==0
    if ~isempty(settings.endDate)
      settings.endDate=datenum(settings.endDate,'yyyymmdd');
    else 
      settings.endDate=[];
    end
end

% fields that need to be read in as strings
strflds={'Analysis';'Port';'Identifier 1';'Identifier 2';'Time Code';'Gas Configuration';'Method';'Date';'Time';'Sample';'Tray'};
% fields that need to be present in input file, and their translated identifiers
reqflds1={'Method';'Identifier1';'Identifier2';'TimeCode';'Sample';'Line';'InjNr';'d18O';'dD';'H2O';'d18O_SD';'dD_SD';'H2O_SD';'interval';'DASTemp';'Sample';'ErrorCode';'baseline_shift';'slope_shift';'residuals';'ch4_ppm';'H2O_Sl';'d18O_Sl';'dD_Sl'};
reqflds2={'Method';'Identifier 1';'Identifier 2';'Time Code';'Sample';'Line';'Inj Nr';'d(18_16)Mean';'d(D_H)Mean';'H2O_Mean';'d(18_16)_SD';'d(D_H)_SD';'H2O_SD';'interval';'DAS Temp';'Sample';'Error Code';'baseline_shift';'slope_shift';'residuals';'ch4_ppm';'H2O_Sl';'d(18_16)_Sl';'d(D_H)_Sl'};

% find the set of files
files=[];
if ~isempty(settings.endDate)        
    for i=settings.startDate:settings.endDate
        files=[files;dir([path,settings.instrument,config.fileType,datestr(i,'yyyymmdd'),'*.csv'])];
    end
elseif ~isempty(settings.startDate)
    files=dir([path,settings.instrument,config.fileType,filterdate,'*.csv']);
    if length(files)==0
      logf(handles,'ERROR: no files found for date range at path %s',[path,settings.instrument,config.fileType,filterdate,'*.csv']);
      data=[];
      return
    end
elseif ~isempty(settings.inputFiles)
  for k=1:size(settings.inputFiles,1)
    files(k).name=settings.inputFiles(k,:);
  end
  path=''; % clear path in this case
else
  logf(handles,'ERROR: missing file date argument');
  data=[];
  return
end

if length(files)==0
  logf(handles,'ERROR: no files found at path %s',path);
  data=[];
  return
end

logf(handles,'Found %d files in time range/selection',length(files));

% read the data from the files
% - Line,  Analysis,             Time Code,           Port,  Inj Nr,  d(17_16)Mean,  d(18_16)Mean,    d(D_H)Mean,      e17_Mean,      H2O_Mean,  Ignore, Good,                            Identifier 1,                            Identifier 2,   Gas Configuration,Timestamp Mean,   d(17_16)_SD,   d(18_16)_SD,     d(D_H)_SD,        e17_SD,        H2O_SD,   d(18_16)_Sl,     d(D_H)_Sl,        H2O_Sl,baseline_shift,   slope_shift,     residuals,baseline_curvature,      interval,       ch4_ppm,  h16od_adjust,   h16od_shift,n2_flag,      DAS Temp,      Tray,  Sample,     Job,    Method,Error Code
% - 1,     P-362,   2016/04/27 15:35:12,           1-02,       1,        -4.757,        -9.011,       -57.555,         0.011,         21207,      -1,    1,                                      DI,                            Memory tests,                 H2O, 1461771588.13,         0.284,         0.286,         0.522,         0.211,       164.544,         0.001,        -0.004,         2.695,        94.819,       -12.221,         2.210,       116.784,         1.088,         0.124,        0.0003,        0.0000,    1,        39.375,         1,       2,       1,FARLAB_Test_15,       0

D = {};
for i=1:length(files)
  fidr=fopen([path,files(i).name]);
  if fidr<0
    logf(handles,'ERROR: file not found at path %s',[path,files(i).name]);
    data=[];
    return
  end
  ins=fgetl(fidr);
  % look for 17O mode in header line
  ret=strfind(ins,'17_16');
  % split header line in fields
  flds=strip(split(ins,','));
  % rewind
  fseek(fidr,0,-1); 
  Header=textscan(fidr,'%s',length(flds),'Delimiter',',');
  if isempty(ret)
    logf(handles,['Reading 18-O mode file ',files(i).name]);
    %addon=[];
    %for j=1:(length(flds)-35)
    %  addon=[addon,'%f'];
    %end
    %Di=textscan(fidr,['%f%s%s%s%f%f%f%f%f%f%s%s%s%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%s%f%s%f',addon],'Delimiter',',','EmptyValue',NaN);
  else
    logf(handles,['Reading 17-O mode file ',files(i).name]);
  end
  addon=[];
  for j=1:(length(flds))
    if ismember(flds{j},strflds)
      addon=[addon,'%s'];
    else
      addon=[addon,'%f'];
    end
  end
  Di=textscan(fidr,addon,'Delimiter',',','EmptyValue',NaN);
  % include input file name
  [fpath,fname,fext]=fileparts(files(i).name);
  maxi=length(Di)+1;
  Di{maxi}=cell(size(Di{1}));
  for j=1:length(Di{1})
    Di{maxi}(j)={[fname,fext]};
  end
  if length(Di{end})>0
    D = vertcat(D,Di);  % Concatenate arrays vertically (adding data from current file)
  end
  fclose(fidr);
  if ~exist('Header','var')
    logf(handles,'ERROR: file not found at path %s',[path,files(i).name]);
    data=[];
    return
  end
  Header=Header{1};
end
% add column for input file
Header{maxi}='Filename';

% format header names
str = {'17_16','18_16','D_H','E17',' ','(','_Mean',')Mean',')'};
newstr = {'17O','18O','D','e17','','','','',''}; 
for i = 1:length(str)
 Header = strrep(Header,str{i},newstr{i});
end

% combine
for i=1:length(Header)
  data.(Header{i}) = vertcat(D{:,i});  % Concatenate the cells vertically
end

% if TimeCode does not exist, try to create from Date and Time fields
if ~isfield(data,'TimeCode')
  if isfield(data,'Time') && isfield(data,'Date')
    for i=1:length(data.Time)
      data.TimeCode{i,1}=[data.Date{i},' ',data.Time{i}];
    end
  end
end

% test for required fields
f=fieldnames(data);
for i=1:length(reqflds1)
  if ~ismember(reqflds1{i},f)
      logf(handles,'WARNING: missing required variable %s, check your input file.',reqflds2{i});
      logf(handles,'Trying to replace by default values...');
      switch reqflds1{i}
        case 'InjNr'
          if ismember('Sample',f)            
            id=[];
            for j=1:length(data.Sample)
              if strcmp(data.Sample{j},id)==0
                id=data.Sample{j};
                injnr=1;
                data.InjNr(j,1)=injnr;
              else
                injnr=injnr+1;
                data.InjNr(j,1)=injnr;
              end
            end
          end
        case 'DASTemp'
          data.DASTemp=ones(size(data.(f{1})))*50.0;
        case 'ErrorCode'
          data.ErrorCode=zeros(size(data.(f{1})));
        case 'Method'
          for j=1:length(data.(f{1}))
            data.Method{j}='unknown';
          end
        otherwise
          data.(reqflds1{i})=zeros(size(data.(f{1})));
      end
  end
end

% convert date text to matlab time
try
  data.date=datenum(data.TimeCode,'yyyy/mm/dd HH:MM:ss');
catch me
  error('Error converting TimeCode. Please check your input file.');
end

% add unique sample number, ignoring which vial
s=1;
sample(1)=1;
for i=2:length(data.Sample)
  if ~strcmp(data.Sample{i},data.Sample{i-1})
    s=s+1;
  end
  sample(i)=s;
end
data.SampleLine=sample';

% fix line to actual count (for files that are missing lines)
for i=1:length(data.Sample)
  data.Line(i)=i;
end

% calculate deuterium excess
data.dxs = data.dD-8*data.d18O;
data.dxs_SD = sqrt(data.dD_SD.^2 + (-8.*data.d18O_SD).^2);

% calculate 17O excess for 17-O mode files
if ismember('e17','data')

  % change unit of 17O-excess: [per mil] to [per meg]
  data.e17 = data.e17.*1000;
  data.e17_SD = data.e17_SD.*1000;

  % calculate 17O excess
  data.d17O_excess = (log(data.d17O./1000+1) - 0.528.*log(data.d18O./1000+1)).*1e6;
  data.d17O_excess_SD = sqrt((data.d17O_SD./1000./(data.d17O./1000+1)).^2 + (-0.528.*data.d18O_SD./1000./(data.d18O./1000+1)).^2).*1e6;
end

% fin
