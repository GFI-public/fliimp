% [iso20000,iso20000_SD] = FARLAB_wconc_correction(iso,iso_SD,H2O,H2O_SD,device,isotype)
%--------------------------------------------------------------------------------------
% Returns the isotopic value at WConc = 20000 ppmv pm SD
% A calibration for the isotopic dependency on water concentration.
% Example: iso20000 = FARLAB_wconc_correction(data.d18O,data.H2O,'HKDS2039','18');
% 'iso': isotope dataset;
% 'H2O': water concentration dataset (from raw measurement);
% 'device' can be one of the following:
%      HIDS2254 (L2130i), HKDS2039 (L2140i freshwater), HKDS2038 (L2140i salt water)
% 'isotype' indicates the type of isotope:
%     '18','O','d18O' --> delta_18_O
%     '2','D','dD' --> delta_D
%-----------------------------------------------------------------------------
% YW
% original version
%-----------------------------------------------------------------------------
% 30.08.2017 HS
% renamed routine from Picarro_Iso_WConc20000
%-----------------------------------------------------------------------------
% 06.06.2018 HS
% included corrections for HIDS2254
%-----------------------------------------------------------------------------
% 14.09.2018 HS
% reorganised for calibration of injection file
%-----------------------------------------------------------------------------


function data = FARLAB_wconc_corr(data,device,use)

    global config
    if isempty(config)
      config=FLIIMP_config;
    end

    % check for valid device ID
    %devices = {'HIDS2254';'HKDS2039';'HKDS2038'};
    if ~ismember(device,config.devices)
        disp('ERROR: device must be one of the following:');
        for i=1:length(config.deviceNames)
          disp(config.deviceName{i});
        end
        return
    end
    
    % if no correction is available or desired, set use to false
    %use=0;

    if strcmp(device,'HKDS2039')
        % d18O:
        %     f(x) = a*x+b
        %     Coefficients (with 95% confidence bounds): % 95%~2SD = 1.8900e-05
        %     a =   0.0001561  (0.0001372, 0.000175) 
        %     b =      -28.28  (-28.64, -27.93)
        %  dD:
        %     f(x) = a*x+b
        %     Coefficients (with 95% confidence bounds): % 95%~2SD = 3.1100e-05
        %     a =  -8.791e-05  (-0.0001501, -2.57e-05)
        %     b =        -220  (-221.1, -218.8)
        % slp =
        %     d18O: 1.560781784194950e-04 pm 9.4500e-06
        %       dD: -8.790620631437319e-05 pm 3.1100e-05
        %     if ismember(isotype,str18) % d18O
        %         a = 1.560781784194950e-04;
        %         a_SD = 9.4500e-06;
        %         iso20000 = iso-a*(H2O-20000);
        %         iso20000_SD = sqrt(iso_SD.^2 + a^2*H2O_SD.^2 + (H2O-20000).^2*a_SD^2);
        %     elseif ismember(isotype,str2) % dD
        %         a = -8.790620631437319e-05;
        %         a_SD = 3.1100e-05;
        %         iso20000 = iso-a*(H2O-20000);
        %         iso20000_SD = sqrt(iso_SD.^2 + a^2*H2O_SD.^2 + (H2O-20000).^2*a_SD^2);
        %     end
        % modified 2017.05.24, calibration V2 (liquid injections with 5 water standards)
        % based on raw H2O values of 15000-24000 ppm % cross point (20000,O)
        %          f(x) = a*x+b
        %      Coefficients (with 95% confidence bounds):
        % d18O
        %        a =   9.278e-06  (8.504e-06, 1.005e-05)  % 95%~2SD = 7.7300e-07
        %        b =     -0.1848  (-0.2007, -0.1688)
        % dD
        %        a =   4.003e-05  (3.602e-05, 4.404e-05)  % 95%~2SD = 4.0100e-06
        %        b =     -0.8233  (-0.906, -0.7407)
        % correct for H218O
        %a1 = 9.2783e-06;
        %a2 = -0.1848;
        a_SD = 3.8650e-07;
        % HS; update 2021-06-25 based on Weng et al., 2020 experiments
        a1 = 1.0059e-05;
        %a2 = 1.1965e-04;
        a2 = 0;
        % correct for HDO
        %b1 = 4.003e-05;
        %b2 = -0.8233;
        b_SD = 2.0050e-06;
        % HS; update 2021-06-25 based on Weng et al., 2020 experiments
        b1 = 4.8939e-05;
        %b2 = -0.0304;
        b2 = 0;
    elseif strcmp(device,'HKDS2038')
        % modified 2017.05.24, calibration V2 (liquid injections with 5 water standards)
        %      f(x) = a*x+b
        %      Coefficients (with 95% confidence bounds):
        % d18O
        %        a =   7.849e-06  (6.896e-06, 8.802e-06) % 95%~2SD = 9.5300e-07
        %        b =     -0.1524  (-0.172, -0.1328)
        % dD
        %        a =   3.469e-05  (2.996e-05, 3.941e-05) % 95%~2SD = 4.7250e-06
        %        b =     -0.7079  (-0.8052, -0.6106)
        % correct for H218O
        %a1 = 7.8494e-06;
        %a2 = -0.1524;
        a_SD = 4.7650e-07;
        % HS; update 2021-06-25 based on Weng et al., 2020 experiments
        a1 = 6.0593e-06;
        %a2 = 0.0059;
        a2 = 0;
        % correct for HDO
        %b1 = 3.4688e-05;
        %b2 = -0.7079;
        b_SD = 2.3625e-06;
        % HS; update 2021-06-25 based on Weng et al., 2020 experiments
        b1 = 2.7264e-05;
        b2 = -0.0114;
        b2 = 0;
    elseif strcmp(device,'HIDS2254')
        % modified 2018.06.14, calibration (liquid injections with 5 water standards)
        %      f(x) = a*x+b
        %      Coefficients (with 95% confidence bounds):
        % d18O
        %      Coefficients (with 95% confidence bounds):
        %        a =  -2.341e-05  (-2.451e-05, -2.232e-05)
        %        b =      0.4603  (0.4394, 0.4811)
        % standard deviation for a and b:  5.4750e-07, 0.0104
        % dD
        %      Coefficients (with 95% confidence bounds):
        %        a =   3.424e-05  (2.38e-05, 4.468e-05)
        %        b =     -0.6763  (-0.8747, -0.478)
        % standard deviation for a and b: 5.2200e-06, 0.0992
        % correct for H218O
        %a1 = -2.341e-05;
        %a2 = 0.4603;
        a_SD = 5.4750e-07;
        % HS; update 2021-06-25 based on Weng et al., 2020 experiments
        a1 = 8.0688e-06;
        %a2 = 0.0043;
        a2 = 0;
        % correct for HDO
        %b1 = 3.424e-05;
        %b2 = -0.6763;
        b_SD = 5.2200e-06;
        % HS; update 2021-06-25 based on Weng et al., 2020 experiments
        b1 = 9.7548e-05;
        %b2 = 0.0173;
        b2 = 0;
    else  % any other device
        disp(['WARNING: no isotope ratio-mixing ratio correction implemented for device ',device]);
        disp('Analysis will continue without correction. Please make edits to routine FARLAB_wconc_corr.m.');
        
        % copy fields to allow continue processing
        data.d18O_corr=data.d18O;
        data.dD_corr=data.dD;
        data.d18O_SD_corr=data.d18O_SD;
        data.dD_SD_corr=data.dD_SD;
        data.dxs_corr = data.dD_corr - 8*data.d18O_corr;
        data.dxs_SD_corr = data.dxs_SD;
        
        if isfield(data,'d17O')
          disp('No concentration correction applied for 17O');
          data.d17O_corr = data.d17O;
          data.d17O_SD_corr = data.d17O_SD;
          data.e17_corr = data.e17;
          data.e17_SD_corr = data.e17_SD;
        end

        return % without corrections applied
    end

   % correct for H218O
   % HS, 2021-06-25: now including y-offset
   data.d18O_corr = data.d18O-(a1*(data.H2O-20000)+a2);
   data.d18O_SD_corr = sqrt(data.d18O_SD.^2 + a1^2*data.H2O_SD.^2 + (data.H2O-20000).^2*a_SD^2);
   % correct for HDO
   % HS, 2021-06-25: now including y-offset
   data.dD_corr = data.dD-(b1*(data.H2O-20000)+b2);
   data.dD_SD_corr = sqrt(data.dD_SD.^2 + b1^2*data.H2O_SD.^2 + (data.H2O-20000).^2*b_SD^2);

   % undo correction
   if use==0
      disp('WARNING: skipping mixing ratio - isotope ratio correction')
      data.d18O_corr=data.d18O;
      data.dD_corr=data.dD;
      data.d18O_SD_corr=data.d18O_SD;
      data.dD_SD_corr=data.dD_SD;
   end

   % calculate corrected dxs
   data.dxs_corr = data.dD_corr - 8*data.d18O_corr;
   % TODO: update this to full validity
   data.dxs_SD_corr = data.dxs_SD;

   % TODO: implement for 17O
   if isfield(data,'d17O')
     disp('No concentration correction applied for 17O');
     data.d17O_corr = data.d17O;
     data.d17O_SD_corr = data.d17O_SD;
     data.e17_corr = data.e17;
     data.e17_SD_corr = data.e17_SD;
   end

end
