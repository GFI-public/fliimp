% -----------------------------------------------------------------------------------
%> last changes: HS, 20.10.2017
%>
%> FLIIMP Version 1.0
%>
%> last changes: HS, 30.04.2018
%>
%> FLIIMP Version 1.0.1
%> included HIDS2245 as additional instrument. Need to implement correction for 
%> humidity dependency of HIDS2254
%
%> last changes: HS, 14.06.2018
%>
%> included option for input/output file path changes, saved in FLIIMP_settings.mat
%> included humidity dependency correction for HIDS2254
%> changed layout and window sizes
%> included help text
%> additional data display in Fig. B
%> -----------------------------------------------------------------------------------

function varargout = FLIIMP(varargin)
% FLIIMP MATLAB code for FLIIMP.fig
%      FLIIMP, by itself, creates a new FLIIMP or raises the existing
%      singleton*.
%
%      H = FLIIMP returns the handle to a new FLIIMP or the handle to
%      the existing singleton*.
%
%      FLIIMP('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FLIIMP.M with the given input arguments.
%
%      FLIIMP('Property','Value',...) creates a new FLIIMP or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before FLIIMP_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to FLIIMP_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help FLIIMP

% Last Modified by GUIDE v2.5 08-Apr-2023 12:38:47

% Begin initialization code - DO NOT EDIT

gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @FLIIMP_OpeningFcn, ...
                   'gui_OutputFcn',  @FLIIMP_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before FLIIMP is made visible.
function FLIIMP_OpeningFcn(hObject, ~, handles, varargin)
global config

% read configuration
config=FLIIMP_config;
if isempty(config)
  delete(gcf);
  return
end

% assign install directory in case FLIIMP is not run from source directory
% addpath(config.installdir) not allowed in compiler

% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to FLIIMP (see VARARGIN)
f=get(0,'Children');
for i=1:length(f)
  if ~strcmp(f(i).Name,'FLIIMP - Step 1')
    delete(f(i).Number);
  end
end

% Choose default command line output for FLIIMP
handles.output = hObject;
set(hObject,'Name','FLIIMP - Step 1');

if ~isempty(varargin)
  set(hObject,'position',varargin{1}.position{1});
  % copy settings if applicable
  if isfield(varargin{1},'settings')
    handles.settings=varargin{1}.settings;
    % apply settings for this form
    idx=find(strcmp(handles.settings.instrument,config.devices));
    set(handles.instrumentPopup,'Value',idx);
    set(handles.timeEdit1,'String',handles.settings.startDate);
    set(handles.timeEdit2,'String',handles.settings.endDate);
    set(handles.filesList,'String',handles.settings.inputFiles);
    if ~isempty(handles.settings.inputFiles)
      selectfilesButton_Callback(hObject,0,handles);
    else 
      timerangeButton_Callback(hObject,0,handles);
    end
  end
end

% Include default input and output paths
if exist(sprintf('%s/FLIIMP_settings.mat',config.homeDirectory),'file')
  load(sprintf('%s/FLIIMP_settings.mat',config.homeDirectory)); % into variable paths
  handles.inputPath=paths.inputPath;
  handles.outputPath=paths.outputPath;
  % apply previous position and size if starting up
  if isfield(paths,'positions')
    handles.position=paths.positions;    
    set(hObject,'Position',handles.position{1});
  else
    handles.position{1}=get(handles.output,'Position');
  end
else
  handles.inputPath=config.defaultInputPath;
  handles.outputPath=config.defaultOutputPath;
end
set(handles.editInputPath,'String',handles.inputPath);
set(handles.editOutputPath,'String',handles.outputPath);

% open log file
handles.settings.logfile=tempname;
handles.settings.lid=fopen(handles.settings.logfile,'w');
if handles.settings.lid<0
    logf(handles,'WARNING: Could not open log file.');
end

% default settings name
if ~isfield(handles.settings,'filename')
  handles.settings.filename='/FLIIMP_project_settings_run.mat';
end

% add instruments to popup menu
set(handles.instrumentPopup,'string',config.deviceNames);
handles.settings.instrument=config.devices{get(handles.instrumentPopup,'Value')};

% set current version number in window title
set(handles.versionText,'string',config.version);

% show the log window
[handles.logfigure, handles.logtext]=FLIIMP_form_log();
%handles.logs = {};
setappdata(0,'logs',{});

% Update handles structure
guidata(hObject, handles);

% update input path
setPathText(handles);

% UIWAIT makes FLIIMP wait for user response (see UIRESUME)
% uiwait(handles.FLIIMP_form_A);
logf(handles,'Current date: %s',datestr(now,'YYYY-mm-dd HH:MM:SS'));
logf(handles,'*****************************************************');
logf(handles,'FLIIMP - FARLAB Liquid Isotope Measurements Processor');
logf(handles,'*****************************************************');
logf(handles,sprintf('Welcome to FLIIMP %s',config.version));
logf(handles,'Please choose or load your parameter settings.');


% --- Outputs from this function are returned to the command line.
function varargout = FLIIMP_OutputFcn(hObject, ~, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on selection change in instrumentPopup.
function instrumentPopup_Callback(hObject, ~, handles)
% hObject    handle to instrumentPopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns instrumentPopup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from instrumentPopup
global config;
set(handles.instrumentPopup,'string',config.deviceNames);
handles.settings.instrument=config.devices{get(handles.instrumentPopup,'Value')};
setPathText(handles);

% --- Executes during object creation, after setting all properties.
function instrumentPopup_CreateFcn(hObject, ~, handles)
% hObject    handle to instrumentPopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function timeEdit1_Callback(hObject, ~, handles)
% hObject    handle to timeEdit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of timeEdit1 as text
%        str2double(get(hObject,'String')) returns contents of timeEdit1 as a double
setPathText(handles);

% --- Executes during object creation, after setting all properties.
function timeEdit1_CreateFcn(hObject, ~, handles)
% hObject    handle to timeEdit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function timeEdit2_Callback(hObject, ~, handles)
% hObject    handle to timeEdit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of timeEdit2 as text
%        str2double(get(hObject,'String')) returns contents of timeEdit2 as a double
setPathText(handles);

% --- Executes during object creation, after setting all properties.
function timeEdit2_CreateFcn(hObject, ~, handles)
% hObject    handle to timeEdit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in filesList.
function filesList_Callback(hObject, ~, handles)
% hObject    handle to filesList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setPathText(handles);

% --- Executes during object creation, after setting all properties.
function filesList_CreateFcn(hObject, ~, handles)
% hObject    handle to filesList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in timerangeButton.
function timerangeButton_Callback(hObject, ~, handles)
% hObject    handle to timerangeButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.deleteButton,'Visible','off');
set(handles.addButton,'Visible','off');
set(handles.filesList,'Visible','off');
set(handles.timeEdit1,'Visible','on');
set(handles.timeEdit2,'Visible','on');
set(handles.toLabel,'Visible','on');
set(handles.timerangeButton,'Value',1);
set(handles.selectfilesButton,'Value',0);


% --- Executes on button press in addButton.
function addButton_Callback(hObject, ~, handles)
% hObject    handle to addButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[newfile newpath]=uigetfile({'*.csv;*.dat',...
    'Picarro Files (*.csv, *.dat)';
    '*.*','All Files (*.*)'},...
    'Pick data files to import','MultiSelect','on');
s=get(handles.filesList,'String');
if (ischar(s)==1)
  if (strcmp('click to add a file',s)==1)
    s=[newpath,newfile];
  else 
    s={s;[newpath,newfile]};
  end
else
  v=length(s);
  s{v+1}=[newpath,newfile];
end
set(handles.filesList,'String',s);


% --- Executes on button press in deleteButton.
function deleteButton_Callback(hObject, ~, handles)
% hObject    handle to deleteButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
s=get(handles.filesList,'String');
if (ischar(s)==1)
  s='click to add a file';
else
  v=get(handles.filesList,'Value');
  s(v)=[];
  set(handles.filesList,'Value',length(s));
end
set(handles.filesList,'String',s);


% --- Executes on button press in selectfilesButton.
function selectfilesButton_Callback(hObject, ~, handles)
% hObject    handle to selectfilesButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.deleteButton,'Visible','on');
set(handles.addButton,'Visible','on');
set(handles.filesList,'Visible','on');
set(handles.timeEdit1,'Visible','off');
set(handles.timeEdit2,'Visible','off');
set(handles.toLabel,'Visible','off');
set(handles.timerangeButton,'Value',0);
set(handles.selectfilesButton,'Value',1);


% --- Executes on button press in quitButton.
function quitButton_Callback(hObject, ~, handles)
% hObject    handle to quitButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global config;

% confirm quit
%selection = questdlg('OK to quit FLIIMP?','Quit FLIIMP','Quit','Cancel','Cancel');
%if strcmp(selection,'Quit')==0
%    return;
%end
    
getSettings(hObject,handles);
handles=guidata(hObject);

% save the file path options
clear paths
paths.inputPath=handles.inputPath;
paths.outputPath=handles.outputPath;
handles.position{1}=get(handles.output,'Position');
paths.positions=handles.position;
logf(handles,['Saving path settings to ',config.homeDirectory,'/FLIIMP_settings.mat']);
save(sprintf('%s/FLIIMP_settings.mat',config.homeDirectory),'paths');
logf(handles,'FLIIMP session ended on %s',datestr(now));

%f=get(0,'Children');
%for i=1:length(f)
%  if ~strcmp(f(i).Name,'FLIIMP - Step 1')
%    delete(f(i).Number);
%  end
%end
%
%delete(handles.logfigure);
delete(gcf);


% --- Executes on button press in nextButton.
function nextButton_Callback(hObject, ~, handles)
% hObject    handle to nextButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global config

getSettings(hObject,handles);
handles=guidata(hObject);
handles.position{1}=get(handles.output,'Position');
if handles.settings.error==1
  return
end
guidata(hObject,handles);

% check if input exists
settings=handles.settings;
settings.inputPath=handles.inputPath;
settings.outputPath=handles.outputPath;
data=FLIIMP_read_liquid_injections(handles,settings);
if isempty(data)
  logf(handles,'ERROR: No data files. Please edit file location or date settings.');
  return
end

% save the file path options
clear paths
paths.inputPath=handles.inputPath;
paths.outputPath=handles.outputPath;
paths.position=handles.position;
logf(handles,'Saving path settings to $(HOME)/FLIIMP_settings.mat');
save(sprintf('%s/FLIIMP_settings.mat',config.homeDirectory),'paths');

%set(gcf,'Visible','Off');
close(gcf)
FLIIMP_form_preproc(handles);

% save current settings to global variable settings
function getSettings(hObject,handles)
global config

if isfield(handles,'settings')
  settings=handles.settings;
end
settings.error=0;
settings.instrument=config.devices{get(handles.instrumentPopup,'Value')};
if (get(handles.timerangeButton,'Value')==1)
  settings.startDate=get(handles.timeEdit1,'String');
  settings.endDate=get(handles.timeEdit2,'String');
  settings.inputFiles=[];
  if isempty(settings.startDate)
    logf(handles,'ERROR: no start date selected. Please check your settings.');
    settings.error=1;
  end
else 
  settings.startDate=[];
  settings.endDate=[];
  settings.inputFiles=get(handles.filesList,'String');
  if isempty(settings.inputFiles)
    logf(handles,'ERROR: no input files selected. Please check your settings.');
    settings.error=1;
  end
end
handles.settings=settings;
guidata(hObject,handles);

% --- Executes on button press in saveButton.
function saveButton_Callback(hObject, ~, handles)
% hObject    handle to saveButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

getSettings(hObject,handles);
handles=guidata(hObject);
settings=handles.settings;
[newfile newpath]=uiputfile({'*.mat','MATLAB data file'},'Save run settings to...',[handles.outputPath,'/',settings.filename]);
if ~isequal(newpath,0) && ~isequal(newfile,0)
  save(fullfile(newpath,newfile),'settings');
  handles.settings.filename=newfile;
  guidata(hObject,handles);
end


% --- Executes on button press in loadButton.
function loadButton_Callback(hObject, eventdata, handles)
% hObject    handle to loadButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global config;

[newfile newpath]=uigetfile({'*.mat','MATLAB data file'},'Load run settings from...',handles.outputPath);

% save log file during loading
logfile=handles.settings.logfile;
if ~isequal(newpath,0) && ~isequal(newfile,0)
  load(fullfile(newpath,newfile));
  settings.lid=handles.settings.lid;
  settings.logfile=logfile;
  settings.filename=newfile;  
  logf(handles,sprintf('loading settings from %s',settings.filename))
else 
  return
end
idx=find(strcmp(settings.instrument,config.devices));
set(handles.instrumentPopup,'Value',idx);
set(handles.timeEdit1,'String',settings.startDate);
set(handles.timeEdit2,'String',settings.endDate);
set(handles.filesList,'String',settings.inputFiles);
if ~isempty(settings.inputFiles)
  selectfilesButton_Callback(hObject,eventdata,handles);
else 
  timerangeButton_Callback(hObject,eventdata,handles);
end
handles.settings=settings;
guidata(hObject,handles);


function editOutputPath_Callback(hObject, eventdata, handles)
% hObject    handle to editOutputPath (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editOutputPath as text
%        str2double(get(hObject,'String')) returns contents of editOutputPath as a double
handles.outputPath=get(handles.editInputPath,'String');
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function editOutputPath_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editOutputPath (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function editInputPath_Callback(hObject, eventdata, handles)
% hObject    handle to editInputPath (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editInputPath as text
%        str2double(get(hObject,'String')) returns contents of editInputPath as a double
handles.outputPath=get(handles.editOutputPath,'String');
guidata(hObject,handles);
setPathText(handles);

% --- Executes during object creation, after setting all properties.
function editInputPath_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editInputPath (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in selInputPath.
function selInputPath_Callback(hObject, eventdata, handles)
% hObject    handle to selInputPath (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
newpath=uigetdir(handles.inputPath);
if newpath~=0
  handles.inputPath=newpath;
  handles.settings.inputPath=newpath;
  set(handles.editInputPath,'String',newpath);
  guidata(hObject,handles);
  setPathText(handles);
end

% --- Executes on button press in selOutputPath.
function selOutputPath_Callback(hObject, eventdata, handles)
% hObject    handle to selOutputPath (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
newpath=uigetdir(handles.outputPath);
if newpath~=0
  handles.outputPath=newpath;
  handles.settings.outputPath=newpath;
  set(handles.editOutputPath,'String',newpath);
  guidata(hObject,handles);
end

% --------------------------------------------------------------------
function FileMenu_Callback(hObject, eventdata, handles)
% hObject    handle to FileMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% --------------------------------------------------------------------
function LoadMenu_Callback(hObject, eventdata, handles)
% hObject    handle to LoadMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

loadButton_Callback(hObject, eventdata, handles);

% --------------------------------------------------------------------
function SaveMenu_Callback(hObject, eventdata, handles)
% hObject    handle to SaveMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

saveButton_Callback(hObject, eventdata, handles);

% --------------------------------------------------------------------
function ReportMenu_Callback(hObject, eventdata, handles)
% hObject    handle to ReportMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

nextButton_Callback(hObject, eventdata, handles);

% --------------------------------------------------------------------
function QuitMenu_Callback(hObject, eventdata, handles)
% hObject    handle to QuitMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

quitButton_Callback(hObject, eventdata, handles);

% --------------------------------------------------------------------
function Tools_Callback(hObject, eventdata, handles)
% hObject    handle to Tools (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function memdrift_Callback(hObject, eventdata, handles)
% hObject    handle to memdrift (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
FLIIMP_form_memdrift(handles);


% --------------------------------------------------------------------
function LTR_Callback(hObject, eventdata, handles)
% hObject    handle to LTR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
FLIIMP_form_LTP(handles);


function setPathText(handles)
% build a string that displays the current search path
global config
set(handles.pathText,'String',[handles.inputPath,'/Instruments/',handles.settings.instrument,'/IsotopeData/',handles.settings.instrument,config.fileType,'*.csv']);


% --------------------------------------------------------------------
function Log_Callback(hObject, eventdata, handles)
% hObject    handle to Log (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.logfigure,'Visible','on');


% --- Executes when user attempts to close FLIIMP_form_A.
function FLIIMP_form_A_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to FLIIMP_form_A (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
quitButton_Callback(hObject, eventdata, handles);
%close

% fin
