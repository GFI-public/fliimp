% @file FLIIMP_config.m
%--------------------------------
% Description
% @brief create and return a configuration object
%--------------------------------
% HS, Sun Apr 24 15:20:34 CEST 2022 
%--------------------------------

function config = FLIIMP_config(varargin)

  if length(varargin)>0
    configfile=[varargin{1},'/FLIIMP_config.csv'];
  else
    configfile='./FLIIMP_config.csv';
  end
  try
    fids=fopen(configfile);
    ins=fgetl(fids); % read header line
    % parse index from file name
    configs=textscan(fids,'%q%q%c%q','Delimiter',',');
    fclose(fids);
  catch
    warndlg(sprintf('Configuration file %s not found at current directory, or file could not be processed. Please check current directory location (%s) and the formatting of FLIIMP_config.csv.\n',configfile,pwd),'Configuration file warning','modal');
    config=[];
    return
  end
 
  for n=1:length(configs{1})
    if configs{3}(n)=='s'
      config.(configs{1}{n})=configs{2}{n};
    else
      config.(configs{1}{n})=eval(configs{2}{n});
    end
  end
  
  % get the home directory of the user
  if ispc==0
    config.homeDirectory=getenv('HOME');
  else
    config.homeDirectory=getenv('USERPROFILE');
  end
  
  % apply installation directory in batch mode
  if length(varargin)>0
    config.installdir=varargin{1};
  end

  %config
  return
  
end


% fin
